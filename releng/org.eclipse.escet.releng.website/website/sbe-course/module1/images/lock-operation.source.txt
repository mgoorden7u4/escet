//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

Source: https://en.wikipedia.org/wiki/Lock_(water_navigation)
Accessed: 2024-05-16
License: Creative Commons Attribution-ShareAlike License 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
