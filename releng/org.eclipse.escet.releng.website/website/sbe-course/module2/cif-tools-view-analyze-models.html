<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.6 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.6: CIF tools to view and analyze models</h1>
                    <p>
                        So far, in this module, you have learned to about various properties, such as reachability and deadlock.
                        You have also learned about interaction between automata, through synchronization and the use of shared variables.
                        And you have learned how to compute the state space of models with multiple interacting automata.
                        So far, you have manually computed the state spaces, and manually checked the various properties.
                    </p>
                    <p>
                        We will now look at some of tools in the CIF toolset that can automate this.
                        We will first look at the CIF explorer, which can automatically compute the state space of CIF models.
                        Then we will look at the CIF to yEd transformer, to visualize CIF models and state spaces.
                        Finally, we will look at the CIF event-based toolset, which has various tools to allow checking properties of CIF state spaces.
                    </p>

                    <h2>The CIF explorer</h2>
                    <p>
                        The CIF explorer can automatically compute the state space of CIF models.
                        To use this tool within the ESCET IDE, right click any CIF model file in the <em>Project Explorer</em> and then select <em>CIF miscellaneous tools</em> and <em>Explore untimed state space...</em>.
                        Since the default settings of the tool typically suffice, click <em>OK</em> to start computing the state space.
                    </p>
                    <p>
                        The CIF explorer will compute the state space.
                        Progress is shown on the <em>Console</em> tab, and once the computation is completed the number of states and transitions in the state space is printed.
                        For instance, something like the following output could be shown on the console:
                    </p>
                    <pre>
                        <code>
                            Found 1 state, 1 state to process.
                            Found 243 states, 0 states to process.
                            Number of states in state space: 243.
                            Number of transitions in state space: 945.
                        </code>
                    </pre>
                    <p>
                        Besides the console output, the CIF explorer will also create a CIF model with a single automaton that represents the state space.
                        And it will write this model to a file.
                    </p>
                    <p>
                        For instance, consider again the model with two automata, with each two locations, from the <a href="computing-state-spaces.html">previous sub-module</a>.
                        If the first automaton is named <code>aut1</code> and the second automaton is named <code>aut2</code>, the CIF model could look like this:
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            event a, b, c;

                            automaton aut1:
                                location L1:
                                    initial;
                                    edge b goto L2;

                                location L2:
                                    marked;
                                    edge a goto L1;
                            end

                            automaton aut2:
                                location R1:
                                    initial;
                                    marked;
                                    edge c goto R2;

                                location R2:
                                    edge b goto R1;
                            end
                        </code>
                    </pre>
                    <p>
                        And the CIF model with the generated generated state space could look like this:
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            event a;
                            event b;
                            event c;

                            automaton statespace:
                                alphabet b, a, c;

                                @state(aut1: "L1", aut2: "R1")
                                location loc1:
                                    initial;
                                    edge c goto loc2;

                                @state(aut1: "L1", aut2: "R2")
                                location loc2:
                                    edge b goto loc3;

                                @state(aut1: "L2", aut2: "R1")
                                location loc3:
                                    marked;
                                    edge a goto loc1;
                                    edge c goto loc4;

                                @state(aut1: "L2", aut2: "R2")
                                location loc4:
                                    edge a goto loc2;
                            end
                        </code>
                    </pre>
                    <p>
                        The state space automaton has four locations, and five edges, to represent the four states and 5 transitions of the state space.
                        Each location is annotated with <code>@state</code>, a state annotation.
                        It indicates the state that the location represents.
                        It thus indicates for each automaton its current location, and for each variable its current value.
                        In this case, the CIF model for which the state space is computed has two automata, so for both of them the current location is indicated in each state annotation.
                        For instance, location <code>loc1</code> represents the state where automaton <code>aut1</code> is in location <code>L1</code> and automaton <code>aut2</code> is in location <code>R1</code>.
                        Location <code>loc3</code> is marked, which indicates that the corresponding state where automaton <code>aut1</code> is in location <code>L2</code> and automaton <code>aut2</code> is in location <code>R1</code>, is a marked state.
                    </p>
                    <p>
                        You may have already noticed that the state space automaton has an explicitly declared alphabet.
                        CIF allows to explicitly declare the alphabet of an automaton, but if it is not explicitly specified it is implicitly defined as all the events on the edges of the automaton.
                    </p>

                    <h2>The CIF to yEd transformer</h2>
                    <p>
                        Having the CIF explorer automatically compute a state space is much more efficient then computing it manually.
                        But, looking at the generated state space as a textual CIF model is not as intuitive as the graphical representation of the state space, as we saw in previous sub-modules.
                        Luckily, the CIF toolset includes the CIF to yEd transformer tool.
                        It can be used to transform a CIF models into yEd diagram files.
                        In the ESCET IDE, simply right click any CIF model file in the <em>Project Explorer</em> and then select <em>CIF miscellaneous tools</em> and <em>Convert CIF to yEd diagram...</em>.
                        Click <em>OK</em> to accept the default settings and generate diagrams.
                        You will notice a new file is created whose name ends with <code>.model.graphml</code>, which contains the generated yEd diagram.
                    </p>
                    <p>
                        The yEd graph editor, or yEd for short, can then be used view such generated diagrams.
                        yEd is freely available and runs on Windows, Linux and macOS.
                        You can download it at <a href="https://www.yworks.com/products/yed">yEd website</a>.
                        After you have installed the tool, and assuming you let the <code>.graphml</code> file extension be coupled to yEd, simply double clicking on a <code>.model.graphml</code> file in the ESCET IDE is enough to open it in yEd.
                        By default, the model will not have any layout, so all elements of the CIF model appear on top of each other:
                    </p>
                    <img src="images/yed-no-layout.png">
                    <p>
                        yEd can automatically layout graphs.
                        Click the <em>Layout</em> menu and select <em>One-Click Layout</em> to let yEd automatically layout the diagram.
                        Alternatively, select the One-Click Layout button on the toolbar.
                        The diagram then could look something like this:
                    </p>
                    <img src="images/yed-with-layout.png">
                    <p>
                        As you can see, yEd typically does quite a good job at layouting.
                        In the diagram it is much easier to see the transitions between the various states of the state space, then it is in the textual CIF model.
                    </p>
                    <p>
                        The CIF to yEd transformer can not just generate diagrams for a state space, but for any CIF model.
                        As an example, consider the diagram for the <a href="shared-variables.html#example">production line</a> from earlier in this module:
                    </p>
                    <img src="images/yed-production-line-model.png">
                    <p>
                        Besides a 'model' diagram, a graphical representation of the CIF model, the CIF to yEd transformer also generates a 'relations' diagram.
                        These diagrams are generated in files whose names end with <code>.relations.graphml</code>.
                        Such diagram are especially useful for models with multiple interacting automata.
                        For instance, for the production system, the generated relations diagram could look like this:
                    </p>
                    <img src="images/yed-production-line-relations.png">
                    <p>
                        Various elements of the CIF model are shown as shapes labeled with their names, including the automata, locations and variables.
                        Furthermore, for each use of an event a circle is drawn and linked with an edge to the declaration of the event.
                        This allows to more quickly and visually see which automata synchronize over an event.
                        For each variable, arrows are drawn from the variable declaration to the elements of the model where the variable is used, such as the locations that have outgoing edges that use the variable in their guards or updates.
                    </p>

                    <h2>The CIF event-based toolset</h2>
                    <p>
                        The CIF event-based toolset is a collection of tools that can among others be used to analyze and manipulate CIF models.
                        These tools only work on 'simple' CIF models, and for instance do not support models with variables.
                        If you have a model with variables, you can use the CIF explorer to first compute the state space model, and use that state space model with the event-based tools.
                    </p>
                    <p>
                        For instance, a model can be checked for being <em>trim</em>.
                        This relates to properties we learned about earlier in this module.
                        A model is trim if for every automaton, every location is <em>reachable</em> from an initial location, and every location is also <em>co-reachable</em>, that is, it is possible to reach a marked location from it.
                        The term <em>co-reachable</em> is just another term for being <em>non-blocking</em>.
                        To run the check, right click a CIF model in the <em>Project Explorer</em> and choose <em>CIF synthesis tools</em>, <em>Event-based synthesis tools</em> and <em>Apply trim check...</em>.
                        Confirm the default settings by clicking <em>OK</em>.
                        A file is created whose name ends with <code>_trimcheck.txt</code>.
                        Open it to inspect the result of the check.
                        Consider for instance the following example result:
                    </p>
                    <pre>
                        <code>
                            Trim check FAILS in file "example.cif".

                            Trim check FAILS for automaton "aut1".
                            - All locations are reachable.
                            - The following location is not coreachable:
                              location "aut1.l2".
                        </code>
                    </pre>
                    <p>
                        It indicates that the model failed the check and is thus not trim.
                        All locations of automaton <code>aut1</code> are reachable, but its location <code>l2</code> is not co-reachable.
                        Location <code>l2</code> is thus blocking.
                        If the model being checked is not trim, and error message is also shown on the <em>Console</em> tab.
                    </p>
                    <p>
                        Previously in this module, we discussed <a href="non-deterministic-automata.html">non-deterministic automata</a>.
                        These are not supported by most of the tools in the event-based toolset.
                        To transform a model with a non-deterministic automaton to a deterministic one, you can use the NFA to DFA automaton conversion.
                        To execute the conversion, right click a CIF model in the <em>Project Explorer</em> and choose <em>CIF synthesis tools</em>, <em>Event-based synthesis tools</em> and <em>Apply NFA to DFA automaton conversion...</em>.
                        Confirm the default settings by clicking <em>OK</em>.
                        A file is created whose name ends with <code>_dfa.cif</code>.
                        Open it to inspect the result of the conversion.
                    </p>
                    <p>
                        The event-based toolset contains various other tools.
                        For instance, it has a tool to check whether the languages of two models are the same, and a tool to convert a non-trim model into a trim one.
                    </p>

                    <h2>Quiz</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: `
                                    Model the synchronizing automata from the model used in the <a href="synchronizing-automata.html#quiz">quiz of the synchronizing automata sub-module</a> in CIF.
                                    Check whether the automata have only reachable locations and are non-blocking.
                                    Which of the following statements is true?`,
                                answers: [
                                    "The top automaton has only reachable locations and is non-blocking, the bottom automaton has at least one location that can't be reached and is blocking.",
                                    "Both automata have only locations that are reachable and are non-blocking.",
                                    "Both automata have only locations that are reachable. The top automaton is blocking, while the bottom one is non-blocking."
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    Compute the state space of that same model and check whether the reachability and blocking behavior is the same.
                                    Does it have the same behavior?`,
                                answers: [
                                    "Yes, it has the same behavior.",
                                    "No, it does not have the same behavior."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    Turn around the edge of event <code>b</code> in the bottom automaton, such that the edge no longer goes from location <code>1</code> to location <code>2</code>, but instead goes from location <code>2</code> to location <code>1</code>.
                                    What is the effect of doing so?`,
                                answers: [
                                    "Location <code>1</code> of the bottom automaton has now become blocking.",
                                    "Location <code>2</code> of the bottom automaton has now become blocking.",
                                    "Location <code>2</code> of the bottom automaton is now not reachable anymore."
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    Make location <code>3</code> of the top automaton the marked location instead of location <code>2</code>.
                                    What changes then?`,
                                answers: [
                                    "The top automaton then has non-reachable locations.",
                                    "The top automaton becomes non-blocking.",
                                    "Location <code>1</code> of the top automaton then becomes blocking."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'multiple-choice',
                                question: `
                                    Which of the statements is true about the following CIF model?
                                    <pre class="highlightjs highlight">
                                        <code class="language-cif hljs" data-lang="cif">
                                            event e, f, g, h;

                                            automaton a:
                                                location:
                                                    initial;
                                                    marked;
                                                    edge e, f, g;
                                            end

                                            automaton b:
                                                alphabet f, g, h;

                                                location:
                                                    initial;
                                                    marked;
                                                    edge g, h;
                                            end
                                        </code>
                                    </pre>`,
                                answers: [
                                    'Transitions for event <code>e</code> are possible.',
                                    'Transitions for event <code>f</code> are possible.',
                                    'Transitions for event <code>g</code> are possible.',
                                    'Transitions for event <code>h</code> are possible.'
                                ],
                                correctAnswer: '1, 3, 4'
                            },
                            {
                                type: 'multiple-choice',
                                question: "Which of the following events are in the alphabet of automaton <code>a</code> of the CIF model from the previous question?",
                                answers: [
                                    "Event <code>e</code>.",
                                    "Event <code>f</code>.",
                                    "Event <code>g</code>.",
                                    "Event <code>h</code>."
                                ],
                                correctAnswer: '1, 2, 3'
                            }
                        ]
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="computing-state-spaces.html">Go back to Module 2.5</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="constants-and-algebraic-variables.html">Proceed to Module 2.7</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
