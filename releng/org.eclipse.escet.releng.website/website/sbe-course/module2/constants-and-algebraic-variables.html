<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.7 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.7: Constants and algebraic variables</h1>
                    <p>
                        So far, in our models, we have only used discrete variables.
                        Discrete variables hold a value, which can be used in for instance guards, and can be updated by assignments on edges.
                        We will now introduce constants, that hold a fixed value, and algebraic variables, that hold a value that is computed from other values, such as those of constants and discrete variables.
                        We will introduce these new concepts through an example.
                    </p>

                    <h2>Example</h2>
                    <p>
                        Consider the following model of a bridge, with a bridge deck, a bridge light and a car:
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            automaton bridge_deck:
                                disc int angle = 0;
                                event open_further, close_further;

                                const int step = 10;

                                alg bool is_open   = angle &gt;= 90;
                                alg bool is_closed = angle &lt;= 0;

                                location:
                                    initial;
                                    edge open_further  when angle &lt; 90 do angle := angle + step;
                                    edge close_further when angle &gt;  0 do angle := angle - step;
                            end

                            automaton bridge_light:
                                event turn_on, turn_off;

                                alg bool is_on = On;
                                alg bool is_off = Off;

                                location On:
                                    initial;
                                    edge turn_off goto Off;

                                location Off:
                                    edge turn_on goto On;
                            end

                            alg bool car_may_drive     =     bridge_deck.is_closed and bridge_light.is_off;
                            alg bool car_may_not_drive = not bridge_deck.is_closed or  bridge_light.is_on;
                        </code>
                    </pre>
                    <p>
                        The <code>bridge_deck</code> automaton models the bridge deck.
                        The bridge deck can open or close.
                        If the deck is closed, and thus horizontal, cars can drive over it.
                        As it opens, its angle compared to the road increases from 0 to 90, and cars can not drive over it.
                        Since the automaton has only a single location, its name is left out.
                        There are two edges, one to open the deck a bit further, and one to close it a bit further.
                        Each occurrence of one of these events opens or closes the bridge deck a bit, changing its angle by a number of degrees.
                        Since opening and closing the deck is done in increments of ten degrees, a constant <code>step</code> is defined to hold this value.
                    </p>
                    <p>
                        Constants are declared using the <code>const</code> keyword.
                        Like variables, constants have a name, a type and a value.
                        In this case, a constant named <code>step</code> is defined of type <code>int</code> and with value <code>10</code>.
                        Like variables, constants can also have a different type, such <code>bool</code> or <code>real</code>.
                        Constants hold a fixed value that can not be changed by assignments.
                        Still, constants can be useful.
                        By giving a value a name, the name can be used instead of the value.
                        This can make models more readable, as you don't have to guess what the value represents.
                        Furthermore, if the constant is used in multiple places, then changing the value of the constant has an affect in all places where it is used.
                        This makes it easier to change values that are used multiple times in the model, and thus to keep the model consistent after such changes.
                    </p>
                    <p>
                        The bridge deck is open at 90 degrees and closed at 0 degrees.
                        The bridge deck automaton has two algebraic variables <code>is_open</code> and <code>is_closed</code>, that model sensors that indicate whether the bridge deck is open or closed, respectively.
                        An algebraic variable are declared using the <code>alg</code> keyword and have a name, a type and a defining expression.
                        The expression represents a computation that produces the value of the algebraic variable, and this computation may make use of constants and other variables.
                        For instance, algebraic variable <code>is_open</code> has a <code>bool</code> type.
                        Its value is thus either <code>true</code> or <code>false</code>.
                        Its defining expression is <code>angle &gt;= 90</code>, which indicates that if the bridge deck's angle is at least 90 degrees, then the bridge deck is open.
                        The <code>angle</code> refers to the discrete variable named <code>angle</code>.
                        The value of the algebraic variable <code>is_open</code> thus depends on the value of the discrete variable <code>angle</code>.
                        If <code>angle</code> is at least 90, then <code>is_open</code> is <code>true</code>, and otherwise, if <code>angle</code> is less than 90, then <code>is_open</code> is <code>false</code>.
                        The algebraic variable can't be assigned a new value.
                        Instead, its value is at all times determined by its defining expression.
                        But, assigning a different value to discrete variable <code>angle</code> may lead to <code>is_open</code> getting a different value as well.
                        That is, if <code>angle</code> is <code>80</code> and the <code>open_further</code> edge is taken, then <code>angle</code> becomes <code>90</code> and the value of <code>is_open</code> automatically changes from <code>false</code> to <code>true</code>.
                        Similarly, the value of algebraic variable <code>is_closed</code> also changes based on the value of discrete variable <code>angle</code>, and is only true if the <code>angle</code> is at most zero.
                    </p>
                    <p>
                        If the values of the sensors would instead be modeled as discrete variables, they would need to be updated on every edge where the value of variable <code>angle</code> is updated.
                        That is, if the angle is increased, <code>is_open</code> would need to be assigned value <code>false</code> if the <code>angle</code> was <code>0</code> before the edge is taken, and <code>is_closed</code> would need to be assigned value <code>true</code> if the <code>angle</code> becomes <code>90</code> after the edge is taken.
                        And similarly for the edge where the angle is decreased.
                        A benefit of algebraic variables is that they don't have to be updated on every edge.
                        Instead, they are defined once and automatically change value as needed.
                        This benefit increases when there are more edges where the variable would have otherwise been needed to be updated.
                        And when a new edge is added to the automaton that also updates the <code>angle</code>, the algebraic variable does not need to be changed.
                        If the variables would instead be modeled as discrete variables, the new edge would also need to update them.
                        If this would be forgotten, the variables would no longer hold the right value.
                        Algebraic variables can thus reduce modeling effort and improve consistency.
                    </p>
                    <p>
                        The <code>bridge_light</code> automaton models the bridge light.
                        When the light is on, cars may not drive over the bridge.
                        Cars may only drive over the bridge when the light is off.
                        This automaton also has two algebraic variables, <code>is_on</code> and <code>is_off</code>, that indicate whether the light is on or off, respectively.
                        The <code>is_on</code> is <code>true</code> when the automaton is in its <code>On</code> location, and is <code>false</code> otherwise.
                        Similarly, <code>is_off</code> is defined by the automaton being in its <code>Off</code> location.
                    </p>
                    <p>
                        The car is modeled by two algebraic variables that model the whether it may or may not drive over the bridge.
                        The car may only drive over the bridge if the bridge deck is closed, and the bridge light is off.
                        Both conditions must hold.
                        This is the case when the algebraic variable <code>is_closed</code> of the <code>bridge_deck</code> automaton and the algebraic variable <code>is_off</code> of the <code>bridge_light</code> automaton are both <code>true</code>.
                        Hence, <code>car_may_drive</code> is defined by expression <code>bridge_deck.is_closed and bridge_light.is_off</code>.
                        In all other situations, thus when the bridge deck is not closed, or the bridge light is off, or both, the car may not drive.
                        Hence, <code>car_may_not_drive</code> is defined by expression <code>bridge_deck.is_open or bridge_light.is_on</code>.
                        Similar to discrete variables, algebraic variables can be read globally, and if defined inside an automaton, and read outside of it, their name must be prefixed with the name of the defining automaton and a period.
                        Note that instead of the boolean algebraic variable <code>bridge_light.is_on</code>, also the <code>On</code> location of the <code>bridge_light</code> could have been used directly.
                        That is, the defining expression of <code>car_may_not_drive</code> could instead have also been defined by expression <code>bridge_deck.is_open or bridge_light.On</code>.
                    </p>
                    <p>
                        In general, algebraic variables can be used to give names to expressions (computations), similar to how constants can be used to give names to fixed values.
                        This brings similar benefits, in terms of readability and making it easier to change the model consistently.
                        Boolean algebraic variables can often be used instead of locations, or vice versa.
                        For instance, the above model could also be modeled using only locations and no algebraic variables.
                        However, using algebraic variables can often make it easier to read and work with the model.
                    </p>
                    <p>
                        Besides that, algebraic variables offer a means of abstraction.
                        The car can refer to the algebraic variables like <code>is_closed</code>.
                        It does not have to know how this algebraic variable is defined.
                        It does not have to that the bridge deck has an <code>angle</code>, nor at which exact angle the it is closed.
                        As the bridge deck automaton is changed, and even the defining expression of <code>is_closed</code>, this does not matter to the car.
                        It can still use the algebraic variable as before, assuming its name and type have not changed.
                        Algebraic variables thus offer a form of abstraction, making it easier to adapt the part of the model that it abstracts.
                    </p>

                    <h2>Quiz 1</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: `
                                    The above example is also part of the course files (folder <code>module2</code>, file <code>constants-and-algebraic-variables.cif</code>).
                                    Simulate it and check with the state visualizer whether the model, and especially the algebraic variables, show the desired behavior.
                                    What do you think?`,
                                answers: [
                                    "Yes, the model has the desired behavior.",
                                    "No, the model does not have the desired behavior."
                                ],
                                correctAnswer: '1'
                            }
                        ]
                    </div>

                    <h2>Exercise 1</h2>
                    <p>
                        Check the above model from the above quiz for unreachable and blocking states.
                        Does the result of the trim check make sense?
                        If not, why not, and how can you fix it?
                    </p>
                    <div class="accordion" id="exercise1-accordion">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#exercise1-solution" aria-expanded="false" aria-controls="exercise1-solution">
                                    Show solution
                                </button>
                            </h2>
                            <div id="exercise1-solution" class="accordion-collapse collapse" data-bs-parent="#exercise1-accordion">
                                <div class="accordion-body">
                                    <p>
                                        The trim check indicates that all locations are blocking.
                                        This is to be expected, since none of the locations are marked.
                                        Therefore, no location can reach a marked location.
                                        To resolve this for this example, the initial locations can be marked.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2>Quiz 2</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: "How can the value of an algebraic variable be changed?",
                                answers: [
                                    "By assigning it a new value in an update.",
                                    "The value changes automatically according to the expression of the variable.",
                                    "By assigning it a new value in an expression."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "Can an algebraic variable be read by another algebraic variable in another automaton?",
                                answers: [
                                    "Yes, that is possible because of the 'global read' concept.",
                                    "No, that is not possible because of the 'local write' concept.",
                                    "It depends on whether it is a boolean, integer or real-valued algebraic variable."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: "Is <code>bridge_deck.is_open or bridge_light.is_on</code> a good alternative defining expression for algebraic variable <code>car_may_not_drive</code>?",
                                answers: [
                                    "Yes, because when the bridge is not closed, it is open.",
                                    "No, because when the bridge is not closed, it may not be fully open."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "Is <code>not car_may_drive</code> a good alternative defining expression for algebraic variable <code>car_may_not_drive</code>?",
                                answers: [
                                    "Yes, it is completely equivalent.",
                                    "No, it is not always equivalent."
                                ],
                                correctAnswer: '1'
                            }
                        ]
                    </div>

                    <h2>Exercise 2</h2>
                    <p>
                        Model the bridge deck and bridge light example without algebraic variables.
                        When you are done, check with the state visualizer whether the behavior is the same as before.
                    </p>
                    <div class="accordion" id="exercise2-accordion">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#exercise2-solution" aria-expanded="false" aria-controls="exercise2-solution">
                                    Show solution
                                </button>
                            </h2>
                            <div id="exercise2-solution" class="accordion-collapse collapse" data-bs-parent="#exercise2-accordion">
                                <div class="accordion-body">
                                    <p>
                                        The example model can be modeled without algebraic variables, making use of more locations, as follows:
                                    </p>
                                    <pre class="highlightjs highlight">
                                        <code class="language-cif hljs" data-lang="cif">
                                            automaton bridge_deck:
                                                disc int angle = 0;
                                                event open_further, close_further;

                                                const int step = 10;

                                                location Closed:
                                                    initial;
                                                    edge open_further do angle := angle + step goto InBetween;

                                                location InBetween:
                                                    edge open_further when angle &lt; 90 - step do angle := angle + step;
                                                    edge open_further when angle = 90 - step do angle := angle + step goto Opened;
                                                    edge close_further when angle &gt; 0 + step do angle := angle - step;
                                                    edge close_further when angle = 0 + step do angle := angle - step goto Closed;

                                                location Opened:
                                                    edge close_further do angle := angle - step goto InBetween;
                                            end

                                            automaton bridge_light:
                                                event turn_on, turn_off;

                                                location On:
                                                    initial;
                                                    edge turn_off goto Off;

                                                location Off:
                                                    edge turn_on goto On;
                                            end

                                            automaton car:
                                                event may_drive_now, may_not_drive_anymore;

                                                location MayNotDrive:
                                                    initial;
                                                    edge may_drive_now when bridge_deck.Closed and bridge_light.Off goto MayDrive;

                                                location MayDrive:
                                                    edge may_not_drive_anymore when not bridge_deck.Closed or bridge_light.On goto MayNotDrive;
                                            end
                                        </code>
                                    </pre>
                                    <p>
                                        This is just one possible answer.
                                        Hopefully, you see that the version of the model with algebraic variables is simpler.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="cif-tools-view-analyze-models.html">Go back to Module 2.6</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="exercises.html">Proceed to Module 2.8</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
