<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.3 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.3: Synchronizing automata</h1>
                    <p>
                        Larger systems consist of sub-systems and sub-sub-systems, with various components.
                        In Module 1, you already learned to model each component separately as an automaton.
                        This way, you model several smaller automata, rather than a much larger single automaton.
                    </p>
                    <p>
                        For instance, consider a system consisting of 20 independent conveyer belts.
                        Let say, to keep it simple, that each belt has only two states, as it can be on or off.
                        Since the state of the system is the combined state of each of its parts, the system would then have 2<sup>20</sup> = 1,048,576 states.
                        Clearly, modeling each state separately is not feasible.
                        Using a collection of automata, which is sometimes also called a network of automata, is then more convenient.
                        Then each conveyer belt can be modeled with just two locations, for a total of 40 locations in total.
                    </p>
                    <p>
                        So far, if you used multiple automata, each automaton had its own events, and thus operated independently of the other automata.
                        In this sub-module you learn about synchronizing automata, that use each others events and thereby interact with each other.
                        Synchronization is a key concept of the CIF modeling language.
                    </p>

                    <h2>Synchronization on events</h2>
                    <p>
                        The power of events is that they synchronize.
                        That is, when multiple automata have an event in their alphabet, a transition for that event can only occur if these automata can all participate at the same time.
                        If only one of them does not have an enabled edge for the event, a transition is not possible for the event.
                        Thus if one of them does not have an edge for the event in its current location, or it does have an edge but its guard condition is not satisfied, then none of the other automata can take an edge for the event either.
                        If all automata that should participate do have an enabled edge for the event, a transition is possible for the event, and all automata take an edge at the same time.
                        They thus synchronize, either all together taking a step, or not at all.
                        If an automaton can take an edge for an event, it is said to <em>enable</em> the event.
                        Only if all automata in the system that have the event in their alphabet enable the event, then they can together take a transition for the event.
                        If an automaton can not take any edge for an event, and this event is in its alphabet, then it is said to <em>block</em> or <em>disable</em> the event.
                        If only just one automaton blocks an event, no transition for that event can occur in the entire system.
                        An automata that does not have a certain event in its alphabet, does not participate in transitions for that event, and also does not block it.
                    </p>
                    <p>
                        As an example, consider a driver of a car that can break down, and a mechanic that can repair the car.
                        We can model their interactions using the following two automata, where the top automaton models the driver and the bottom automaton the mechanic:
                    </p>
                    <img src="images/synchronization-example-driver.png">
                    <img src="images/synchronization-example-mechanic.png">
                    <p>
                        The driver is initially driving the car (location <code>Driving</code>).
                        Once the car breaks down (event <code>breakdown</code>), the driver prepares to call the mechanic (location <code>PreparingToCall</code>).
                        After calling the mechanic (event <code>call_mechanic</code>), the driver waits for the repairs to be completed (location <code>Waiting</code>).
                        Once the repairs have been completed (event <code>repair_finished</code>), and the car is again in working order, the driver can continue driving it (location <code>Driving</code>).
                        If the car breaks down again, the driver will again call the mechanic, and so on.
                    </p>
                    <p>
                        The mechanic is initially not working (location <code>NotWorking</code>).
                        Once a call comes in from the driver (event <code>call_mechanic</code>), the mechanic prepares to start repairs on the car (location <code>PreparingToRepair</code>).
                        After the preparations, the mechanic starts repairing the car (event <code>start_repair</code>), which takes some time (location <code>Repairing</code>).
                        Once the repairs have been completed (event <code>repair_finished</code>), the mechanic is done and is no longer working on the car (location <code>NotWorking</code>).
                        Should another call come in, the mechanic will again repair the car, and so on.
                    </p>
                    <p>
                        The driver's automaton uses three events: <code>breakdown</code>, <code>call_mechanic</code> and <code>repair_finished</code>.
                        The mechanic's automaton also uses three events: <code>call_mechanic</code>, <code>start_repair</code> and <code>repair_finished</code>.
                        The <code>call_mechanic</code> and <code>repair_finished</code> events are used by both automata (they <em>share</em> the events), while the <code>breakdown</code> event is used only by the driver, and the <code>start_repair</code> event only by the mechanic.
                    </p>
                    <p>
                        Now consider the combined behavior of the driver and the mechanic, taking into account their interactions.
                        Initially, the driver is driving (location <code>Driving</code>), and the mechanic is not working (location <code>NotWorking</code>).
                        The mechanic is waiting for a call, but it won't come in at this time, since the driver will only call in its <code>PreparingToCall</code> location.
                        Since the driver has no edge for the <code>call_mechanic</code> event in its current location (<code>Driving</code>), the event is not enabled.
                        The mechanic thus continues to wait for the call.
                        The driver will keep driving until the car breaks down (event <code>breakdown</code>).
                        Since only the driver automaton uses this event, no other automaton needs to synchronize.
                        Once the car breaks down, the driver automaton will go its <code>PreparingToCall</code> location.
                        Then the driver will call the mechanic (event <code>call_mechanic</code>).
                        This involves an interaction between the driver and the mechanic.
                        After this event, the driver will go to its <code>Waiting</code> location, and the mechanic will go its <code>PreparingToRepair</code> location.
                        The driver has to wait for the repair to have finished (event <code>repair_finished</code>), which can not occur yet, as the mechanic has not started the repair, let alone finished it.
                        That is, the driver has an outgoing edge for the <code>repair_finished</code> event in its current location, but the mechanic does not.
                        The mechanic can however start the repair (event <code>start_repair</code>) and go its <code>Repairing</code> location.
                        Once the mechanic then finishes the repair (event <code>repair_finished</code>), the driver can go back to <code>Driving</code> and the mechanic is again <code>NotWorking</code>.
                        This is again a synchronization, so both automata go to their next locations simultaneously.
                    </p>

                    <h2>Event placement</h2>
                    <p>
                        When multiple automata use an event, and synchronize on it, the event is only declared once.
                        This begs the question where the declaration of the event should be placed in a CIF model.
                    </p>
                    <p>
                        In many cases it is clear in which automaton the event should be declared.
                        For instance, the event for the car breaking down belongs logically with the driver of the car, as that automaton is the only automaton that uses the event.
                        And the mechanic being finished with repairing the care belongs logically with the mechanic, as the mechanic performs the action.
                        The driver synchronizes on the event of the mechanic being done, and reacts to it, but does not perform the action.
                    </p>
                    <p>
                        There are however events for which it is more difficult to determine in which automaton to declare them.
                        For instance, the event to call the mechanic involves both the driver and the mechanic.
                        One could argue that the driver initiates the action, and thus the event should be declared in the driver's automaton.
                        But, one could also argue that the mechanic is being called, and thus the event should be declared in the mechanic's automaton.
                        In cases where it is less clear in which automaton to declare the event, there is another option: declare the event outside the automata.
                    </p>
                    <p>
                        In general, it is best to place events as close as possible to where they are used (initiated, executed, and so on).
                        As an example, consider the following CIF model of the driver and mechanic, with some events declared in the automata, and some outside the automata.
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            event call_mechanic;

                            automaton Driver:
                                event breakdown;

                                location Driving:
                                    initial;
                                    marked;
                                    edge breakdown goto PreparingToCall;

                                location PreparingToCall:
                                    edge call_mechanic goto Waiting;

                                location Waiting:
                                    edge Mechanic.repair_finished goto Driving;
                            end

                            automaton Mechanic:
                                event start_repair, repair_finished;

                                location NotWorking:
                                    initial;
                                    marked;
                                    edge call_mechanic goto PreparingToRepair;

                                location PreparingToRepair:
                                    edge start_repair goto Repairing;

                                location Repairing:
                                    edge repair_finished goto NotWorking;
                            end
                        </code>
                    </pre>
                    <p>
                        Events declared in the same automaton as where they are used (on edges), can be referred to only by their name.
                        Similarly, events declared outside the automata can also be referred to directly by their name.
                        Events declared in other automata, on the other hand, have are to prefixed with the name of that automaton and a period, such as <code>Mechanic.repair_finished</code> in the example above.
                    </p>

                    <h2 id="quiz">Quiz</h2>
                    <p>
                        Consider the following two automata for the first five questions.
                    </p>
                    <img src="images/synchronization-quiz-model1.png">
                    <img src="images/synchronization-quiz-model2.png">
                    <div class="quiz">
                        [
                            {
                                type: 'multiple-choice',
                                question: "What are the shared events of the two automata?",
                                answers: [
                                    "Event <code>a</code>.",
                                    "Event <code>b</code>.",
                                    "Event <code>c</code>.",
                                    "Event <code>d</code>.",
                                    "Event <code>e</code>.",
                                    "Event <code>f</code>."
                                ],
                                correctAnswer: '2, 3'
                            },
                            {
                                type: 'multiple-choice',
                                question: "For which events can transitions happen when the top automaton is in location <code>2</code> and the bottom one is in location <code>1</code>?",
                                answers: [
                                    "Event <code>a</code>.",
                                    "Event <code>b</code>.",
                                    "Event <code>c</code>.",
                                    "Event <code>d</code>.",
                                    "Event <code>e</code>.",
                                    "Event <code>f</code>."
                                ],
                                correctAnswer: '2, 4, 6'
                            },
                            {
                                type: 'multiple-choice',
                                question: "For which events can transitions happen when both automata are in location <code>1</code>?",
                                answers: [
                                    "Event <code>a</code>.",
                                    "Event <code>b</code>.",
                                    "Event <code>c</code>.",
                                    "Event <code>d</code>.",
                                    "Event <code>e</code>.",
                                    "Event <code>f</code>."
                                ],
                                correctAnswer: '3, 5'
                            },
                            {
                                type: 'single-choice',
                                question: "When considering only the top automaton, and assuming it is the only automaton in a model, does it then have deadlock?",
                                answers: [
                                    "Yes, from location <code>3</code> the automaton cannot go back to locations <code>1</code> and <code>2</code>.",
                                    "Yes, in location <code>3</code> there is is no behavior possible.",
                                    "No, there is no situation in which no further behavior is possible."
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'single-choice',
                                question: "When considering only the top automaton, and assuming it is the only automaton in a model, is it blocking?",
                                answers: [
                                    "Yes, once in location <code>3</code> the automaton cannot go to a marked location.",
                                    "No, all its locations are reachable from its initial location."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    What would happen with the behavior of the driver and the mechanic, if the driver would have location <code>Waiting</code> as its initial location instead of location <code>Driving</code>?
                                    Select the best answer.`,
                                answers: [
                                    "This does not matter, as the change only affects the initial state, and the two automata are still deadlock-free and non-blocking.",
                                    "The individual automata still do not have deadlock, but the entire system now does. No further behavior is possible right from the start.",
                                    "The automata then do not correctly model the behavior of the interactions between the driver and the mechanic."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    Let's say you have to model a button that can be pushed and released, and a motor that can be turned on and off.
                                    Pushing the button turns on the motor, and pushing it again turns off the motor.
                                    Where would you place the events of the button being pushed and released?
                                    Select the best answer.`,
                                answers: [
                                    "Either in the button automaton, the motor automaton, or outside the automata. It does not matter.",
                                    "In the automaton of the button, since it is the button that is pushed and released.",
                                    "In the automaton of the motor, because pushing and releasing the button directly affects the motor.",
                                    "Outside the automata, since the button and motor interact and it is therefore difficult to decide a specific automaton to declare the events."
                                ],
                                correctAnswer: '2'
                            }
                        ]
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="non-deterministic-automata.html">Go back to Module 2.2</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="shared-variables.html">Proceed to Module 2.4</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
