//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.targets;

import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newRealType;
import static org.eclipse.escet.common.java.Lists.last;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Maps.map;
import static org.eclipse.escet.common.java.Sets.set;
import static org.eclipse.escet.common.java.Strings.fmt;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.escet.cif.common.CifTypeUtils;
import org.eclipse.escet.cif.metamodel.cif.ComplexComponent;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.declarations.Constant;
import org.eclipse.escet.cif.metamodel.cif.declarations.ContVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumDecl;
import org.eclipse.escet.cif.metamodel.cif.declarations.InputVariable;
import org.eclipse.escet.cif.metamodel.cif.types.BoolType;
import org.eclipse.escet.cif.metamodel.cif.types.CifType;
import org.eclipse.escet.cif.metamodel.cif.types.EnumType;
import org.eclipse.escet.cif.metamodel.cif.types.IntType;
import org.eclipse.escet.cif.metamodel.cif.types.RealType;
import org.eclipse.escet.cif.plcgen.PlcGenSettings;
import org.eclipse.escet.cif.plcgen.conversion.ModelTextGenerator;
import org.eclipse.escet.cif.plcgen.conversion.expressions.ExprGenerator;
import org.eclipse.escet.cif.plcgen.generators.CifEventTransition;
import org.eclipse.escet.cif.plcgen.generators.CifProcessor;
import org.eclipse.escet.cif.plcgen.generators.CifProcessor.CifProcessorResults;
import org.eclipse.escet.cif.plcgen.generators.ContinuousVariablesGenerator;
import org.eclipse.escet.cif.plcgen.generators.DefaultContinuousVariablesGenerator;
import org.eclipse.escet.cif.plcgen.generators.DefaultNameGenerator;
import org.eclipse.escet.cif.plcgen.generators.DefaultTransitionGenerator;
import org.eclipse.escet.cif.plcgen.generators.DefaultTypeGenerator;
import org.eclipse.escet.cif.plcgen.generators.DefaultVariableStorage;
import org.eclipse.escet.cif.plcgen.generators.InputOutputGenerator;
import org.eclipse.escet.cif.plcgen.generators.NameGenerator;
import org.eclipse.escet.cif.plcgen.generators.PlcCodeStorage;
import org.eclipse.escet.cif.plcgen.generators.PlcVariablePurpose;
import org.eclipse.escet.cif.plcgen.generators.TransitionGenerator;
import org.eclipse.escet.cif.plcgen.generators.TypeGenerator;
import org.eclipse.escet.cif.plcgen.generators.VariableStorage;
import org.eclipse.escet.cif.plcgen.generators.io.IoAddress;
import org.eclipse.escet.cif.plcgen.generators.io.IoDirection;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcBasicVariable;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcDataVariable;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcPou;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcPouType;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcProject;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcExpression;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcFuncAppl;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcNamedValue;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcVarExpression;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.ExprAssociativity;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.ExprBinding;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.PlcFuncNotation;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.PlcFuncTypeExtension;
import org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation;
import org.eclipse.escet.cif.plcgen.model.functions.PlcPouDescription;
import org.eclipse.escet.cif.plcgen.model.statements.PlcAssignmentStatement;
import org.eclipse.escet.cif.plcgen.model.statements.PlcCommentLine;
import org.eclipse.escet.cif.plcgen.model.statements.PlcReturnStatement;
import org.eclipse.escet.cif.plcgen.model.statements.PlcStatement;
import org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType;
import org.eclipse.escet.cif.plcgen.model.types.PlcType;
import org.eclipse.escet.cif.plcgen.options.ConvertEnums;
import org.eclipse.escet.cif.plcgen.options.ConvertEnumsOption;
import org.eclipse.escet.cif.plcgen.options.EventTransitionForm;
import org.eclipse.escet.cif.plcgen.options.PlcNumberBits;
import org.eclipse.escet.cif.plcgen.writers.Writer;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.PathPair;
import org.eclipse.escet.common.java.exceptions.InvalidInputException;
import org.eclipse.escet.common.java.output.WarnOutput;

/** Base class for generating a {@link PlcProject}. */
public abstract class PlcBaseTarget extends PlcTarget {
    /** Size of an integer value in a CIF specification. */
    public static final int CIF_INTEGER_SIZE = 32;

    /** Size of a real value in a CIF specification. */
    public static final int CIF_REAL_SIZE = 64;

    /** PLC target type for code generation. */
    public final PlcTargetType targetType;

    /** Name to use to call the {@code TON} function within the instance variable of the block function. */
    protected final String tonFuncBlockCallName;

    /** User-defined integer type size to use by the PLC. */
    private PlcNumberBits intTypeSize;

    /** User-defined real type size to use by the PLC. */
    private PlcNumberBits realTypeSize;

    /** How to convert enumerations when the {@link ConvertEnumsOption} is set to {@link ConvertEnums#AUTO}. */
    private final ConvertEnums autoEnumConversion;

    /** How to convert enumerations. */
    private ConvertEnums selectedEnumConversion;

    /** The chosen form of the code for an event transition. */
    private EventTransitionForm transitionForm;

    /** Paths to write the generated code. Depending on the target can be either a file or a directory path. */
    private PathPair outputPaths;

    /** Callback to send warnings to the user. */
    private WarnOutput warnOutput;

    /** Conversion of PLC models to text for the target. */
    private final ModelTextGenerator modelTextGenerator = new ModelTextGenerator(this);

    /** Extracts information from the CIF input file, to be used during PLC code generation. */
    protected CifProcessor cifProcessor;

    /** Generates PLC code for performing CIF event transitions. */
    protected TransitionGenerator transitionGenerator;

    /** Code generator for handling continuous behavior. */
    private ContinuousVariablesGenerator continuousVariablesGenerator;

    /** Generator that creates input and output PLC code. */
    protected InputOutputGenerator ioGenerator;

    /** Handles storage and retrieval of globally used variables in the PLC program. */
    protected VariableStorage varStorage;

    /** Stores and writes generated PLC code. */
    protected PlcCodeStorage codeStorage;

    /** Names disallowed in the PLC language or by the target, in lowercase. */
    protected final Set<String> disallowedNames;

    /**
     * Constructor of the {@link PlcBaseTarget} class.
     *
     * @param targetType PLC target type for code generation.
     * @param autoEnumConversion How to convert enumerations when the user selects {@link ConvertEnums#AUTO}. This
     *     should not be {@link ConvertEnums#AUTO}.
     */
    public PlcBaseTarget(PlcTargetType targetType, ConvertEnums autoEnumConversion) {
        this(targetType, autoEnumConversion, "");
    }

    /**
     * Constructor of the {@link PlcBaseTarget} class.
     *
     * @param targetType PLC target type for code generation.
     * @param autoEnumConversion How to convert enumerations when the user selects {@link ConvertEnums#AUTO}. This
     *     should not be {@link ConvertEnums#AUTO}.
     * @param tonFuncBlockCallName Name to use to call the {@code TON} function within the instance variable of the
     *     block function.
     */
    public PlcBaseTarget(PlcTargetType targetType, ConvertEnums autoEnumConversion, String tonFuncBlockCallName) {
        this.targetType = targetType;
        this.autoEnumConversion = autoEnumConversion;
        this.tonFuncBlockCallName = tonFuncBlockCallName;
        this.disallowedNames = Collections.unmodifiableSet(getDisallowedNames());

        // Selecting "auto" by the user should result in a concrete preference of the target.
        Assert.check(autoEnumConversion != ConvertEnums.AUTO);
    }

    /**
     * Returns the names disallowed in the PLC language. Specific targets may override this method to additionally
     * return the names disallowed by the target.
     *
     * @return The names disallowed in the PLC language or by the target, in lowercase.
     */
    protected Set<String> getDisallowedNames() {
        Set<String> names = set();

        // Keywords. Note that "en" and "eno" special parameter names have been left out.
        names.addAll(List.of("action", "array", "at", "by", "case", "configuration", "constant", "do", "else", "elsif",
                "end_action", "end_case", "end_configuration", "end_for", "end_function", "end_function_block",
                "end_if", "end_program", "end_repeat", "end_resource", "end_retain", "end_step", "end_struct",
                "end_transition", "end_type", "end_var", "end_while", "exit", "false", "f_edge", "for", "from",
                "function", "function_block", "if", "initial_step", "of", "on", "program", "read_only", "read_write",
                "r_edge", "repeat", "resource", "retain", "return", "step", "struct", "task", "then", "to",
                "transition", "true", "type", "until", "var", "var_access", "var_config", "var_external", "var_global",
                "var_in_out", "var_input", "var_output", "var_temp", "while", "with"));
        // Functions.
        names.addAll(List.of("abs", "acos", "add", "and", "asin", "atan", "cos", "div", "eq", "exp", "expt", "ge", "gt",
                "le", "ln", "log", "lt", "max", "min", "mod", "mul", "ne", "not", "or", "sel", "sin", "sqrt", "sub",
                "tan", "xor"));

        // Function block names.
        names.addAll(List.of(
                "rs", "sr", // Set/reset.
                "ton", "tof", "tp", // Timers.
                "f_trig", "r_trig", // Edge detection.
                "ctu", "ctu_dint", "ctu_lint", "ctu_udint", "ctu_ulint", // Up counters.
                "ctd", "ctd_dint", "ctd_lint", "ctd_udint", "ctd_ulint", // Down counters.
                "ctud", "ctud_dint", "ctud_lint", "ctud_udint", "ctud_ulint" // Up-down counters.
        ));

        // Type keywords.
        List<String> typeKeywords = List.of("bool", "sint", "int", "dint", "lint", "usint", "uint", "ulint", "udint",
                "real", "lreal", "time", "date", "time_of_day", "tod", "date_and_time", "dt", "string", "byte", "word",
                "dword", "lword", "wstring");
        names.addAll(typeKeywords);

        // Generic type keywords.
        names.addAll(List.of("any", "any_derived", "any_elementary", "any_magnitude", "any_num", "any_real", "any_int",
                "any_bit", "any_string", "any_date"));

        // Casts functions (X_TO_Y).
        for (int i = 0; i < typeKeywords.size(); i++) {
            for (int j = 0; j < typeKeywords.size(); j++) {
                if (i == j) {
                    continue;
                }
                names.add(typeKeywords.get(i) + "_to_" + typeKeywords.get(j));
            }
        }

        // Return disallowed names.
        return names;
    }

    /**
     * Initialize the target.
     *
     * @param settings Configuration to use.
     */
    public void setup(PlcGenSettings settings) {
        intTypeSize = settings.intTypeSize;
        realTypeSize = settings.realTypeSize;
        outputPaths = settings.outputPaths;
        warnOutput = settings.warnOutput;
        selectedEnumConversion = (settings.enumConversion == ConvertEnums.AUTO) ? autoEnumConversion
                : settings.enumConversion;
        transitionForm = settings.transitionForm;

        // Warn the user about getting a possibly too small integer type size.
        if (settings.intTypeSize.getTypeSize(CIF_INTEGER_SIZE) < CIF_INTEGER_SIZE) {
            warnOutput.line(
                    "Configured integer type size is less than the CIF integer type size. Some values in the program "
                            + "may be truncated.");
        } else if (getMaxIntegerTypeSize() < CIF_INTEGER_SIZE) {
            warnOutput.line("Maximum integer type size supported by the PLC is less than the CIF integer type size. "
                    + "Some values in the program may be truncated.");
        }

        // Warn the user about getting a possibly too small real type size.
        if (settings.realTypeSize.getTypeSize(CIF_REAL_SIZE) < CIF_REAL_SIZE) {
            warnOutput.line("Configured real type size is less than the CIF real type size. Some values in the program "
                    + "may be truncated.");
        } else if (getMaxRealTypeSize() < CIF_REAL_SIZE) {
            warnOutput.line("Maximum real type size supported by the PLC is less than the CIF real type size. Some "
                    + "values in the program may be truncated.");
        }
    }

    /**
     * Generate and write the PLC code.
     *
     * @param settings Configuration to use.
     * @param inputSpec Input CIF specification.
     */
    public void generate(PlcGenSettings settings, Specification inputSpec) {
        setup(settings);

        NameGenerator nameGenerator = new DefaultNameGenerator(this, settings);
        TypeGenerator typeGenerator = new DefaultTypeGenerator(this, nameGenerator);

        codeStorage = new PlcCodeStorage(this, nameGenerator, typeGenerator, settings);
        varStorage = new DefaultVariableStorage(this, nameGenerator, typeGenerator);
        cifProcessor = new CifProcessor(this, inputSpec, settings);
        transitionGenerator = new DefaultTransitionGenerator(this, nameGenerator, typeGenerator);
        ioGenerator = new InputOutputGenerator(this, nameGenerator, typeGenerator, settings);
        continuousVariablesGenerator = new DefaultContinuousVariablesGenerator(this, nameGenerator);

        // Preparation.
        //
        // Extend the set of reserved names in the name generator, to avoid conflicts.
        nameGenerator.addDisallowedNames(ioGenerator.getCustomIoNames());

        // Processing and code generation.
        //
        // Check and normalize the CIF specification, and extract relevant information from it.
        CifProcessorResults processorResults = cifProcessor.process();
        if (settings.termination.isRequested()) {
            return;
        }

        // Distribute results of the CIF processor.
        for (DiscVariable discVar: processorResults.discVariables()) {
            varStorage.addStateVariable(discVar, discVar.getType());
        }
        for (InputVariable inpVar: processorResults.inputVariables()) {
            varStorage.addStateVariable(inpVar, inpVar.getType());
        }
        for (EnumDecl enumDecl: processorResults.enumDecls()) {
            typeGenerator.convertEnumDecl(enumDecl);
        }
        for (ContVariable contVar: processorResults.contVariables()) {
            varStorage.addStateVariable(contVar, newRealType());
            continuousVariablesGenerator.addVariable(contVar);
        }
        for (Constant constant: processorResults.constants()) {
            varStorage.addConstant(constant);
        }
        codeStorage.addComponentDatas(processorResults.componentDatas());
        if (settings.termination.isRequested()) {
            return;
        }

        // Add code and data to variable storage for the previously supplied continuous variables.
        continuousVariablesGenerator.process();
        if (settings.termination.isRequested()) {
            return;
        }

        // Generate input and output code.
        ioGenerator.process(processorResults.cifObjectFinder());
        if (settings.termination.isRequested()) {
            return;
        }

        // Make the globally used variables ready for use in the PLC code.
        varStorage.process();
        if (settings.termination.isRequested()) {
            return;
        }

        // Generate the event transitions code.
        List<CifEventTransition> allCifEventTransitions = processorResults.cifEventTransitions();
        EventTransitionsCode eventTransCode = generateTransCode(nameGenerator, typeGenerator, allCifEventTransitions);
        if (settings.termination.isRequested()) {
            return;
        }

        // Prepare the PLC program for getting saved to the file system.
        codeStorage.finishPlcProgram(eventTransCode);
        if (settings.termination.isRequested()) {
            return;
        }

        // And write it.
        codeStorage.writeOutput();
    }

    /**
     * Construct the transitions code and support POUs.
     *
     * @param nameGenerator Generator for creating clash-free names in the generated code.
     * @param typeGenerator Generator for converting CIF types to PLC types.
     * @param allCifEventTransitions Event transitions to convert to code.
     * @return The generated transitions code and support POUs.
     */
    private EventTransitionsCode generateTransCode(NameGenerator nameGenerator, TypeGenerator typeGenerator,
            List<CifEventTransition> allCifEventTransitions)
    {
        // Setup the transition generator.
        transitionGenerator.setup(allCifEventTransitions);

        // Split event transitions between controllable and uncontrollable events.
        List<CifEventTransition> unconSeq = allCifEventTransitions.stream()
                .filter(cet -> !cet.event.getControllable()).toList();
        List<CifEventTransition> conSeq = allCifEventTransitions.stream()
                .filter(cet -> cet.event.getControllable()).toList();

        // Generate the transitions code.
        PlcBasicVariable mainProgressVar = codeStorage.getIsProgressVariable();
        switch (transitionForm) {
            case CODE_IN_FUNCTION: {
                List<PlcPou> pous = listc(unconSeq.size() + conSeq.size());
                List<PlcStatement> unconCallSequence = convertEventTransitions(nameGenerator, typeGenerator, unconSeq,
                        mainProgressVar, pous);
                List<PlcStatement> conCallSequence = convertEventTransitions(nameGenerator, typeGenerator, conSeq,
                        mainProgressVar, pous);
                return new EventTransitionsCode(unconCallSequence, conCallSequence, pous);
            }
            case CODE_IN_MAIN: {
                ExprGenerator exprGen = codeStorage.getExprGenerator();
                List<List<PlcStatement>> transCode = transitionGenerator.generate(List.of(unconSeq, conSeq),
                        exprGen, mainProgressVar);
                return new EventTransitionsCode(transCode.get(0), transCode.get(1), List.of());
            }
            case FUNCTIONS_FOR_SCOPE: {
                String unconPrefix = "tryUncon_"; // POU function name prefix for uncontrollable event transitions.
                String conPrefix = "tryCon_"; // POU function name prefix for controllable event transitions.

                // Split events by scope and controllability. We preserve adherence to the execution scheme prescribed
                // by the controller properties checker, by using an order-preserving map, and considering the
                // transitions in the given order. Note that the execution scheme's event sorting already puts events
                // together by scope, so this is also preserved.
                Map<ComplexComponent, ScopedEvents> splittedEvents = map();
                for (CifEventTransition evtTrans: allCifEventTransitions) {
                    ComplexComponent comp = (ComplexComponent)evtTrans.event.eContainer();
                    ScopedEvents scopedEvts = splittedEvents.computeIfAbsent(comp,
                            grp -> new ScopedEvents(comp, list(), list(),
                                    nameGenerator.generateGlobalNames(Set.of(unconPrefix, conPrefix), comp)));
                    if (evtTrans.event.getControllable()) {
                        scopedEvts.controllables.add(evtTrans);
                    } else {
                        Assert.check(!evtTrans.event.getControllable());
                        scopedEvts.uncontrollables.add(evtTrans);
                    }
                }

                // Setup storage for the results.
                List<PlcPou> pous = listc(splittedEvents.size() * 2);
                List<PlcStatement> unconCallSequence = list();
                List<PlcStatement> conCallSequence = list();

                // Convert uncontrollable events in each scope.
                for (ScopedEvents scopedEvents: splittedEvents.values()) {
                    if (!scopedEvents.uncontrollables.isEmpty()) {
                        // Generate the POU and call it with: isProgress := tryUncon_<GROUP_NAME>(isProgress);
                        String pouName = unconPrefix + scopedEvents.pouFuncName;
                        PlcPou pou = makeTransitionsPou(nameGenerator, typeGenerator, scopedEvents.uncontrollables,
                                pouName, mainProgressVar.varName);
                        pous.add(pou);
                        unconCallSequence.add(createProgressPouCall(pou, mainProgressVar));
                    }
                }

                // Convert controllable events in each scope.
                for (ScopedEvents scopedEvents: splittedEvents.values()) {
                    if (!scopedEvents.controllables.isEmpty()) {
                        // Generate the POU and call it with: isProgress := tryCon_<GROUP_NAME>(isProgress);
                        String pouName = conPrefix + scopedEvents.pouFuncName;
                        PlcPou pou = makeTransitionsPou(nameGenerator, typeGenerator, scopedEvents.controllables,
                                pouName, mainProgressVar.varName);
                        pous.add(pou);
                        conCallSequence.add(createProgressPouCall(pou, mainProgressVar));
                    }
                }
                return new EventTransitionsCode(unconCallSequence, conCallSequence, pous);
            }
            default:
                throw new AssertionError("Unexpected event transition form \"" + transitionForm + "\".");
        }
    }

    /**
     * Storage for controllable and uncontrollable events in a single CIF scope.
     *
     * @param scope CIF scope containing the events.
     * @param controllables Controllable events in the scope.
     * @param uncontrollables Uncontrollable events in the scope.
     * @param pouFuncName Name of the POU function without prefix.
     */
    private static record ScopedEvents(ComplexComponent scope,
            List<CifEventTransition> controllables, List<CifEventTransition> uncontrollables, String pouFuncName)
    {
    }

    /**
     * Convert the provided event transitions by creating a POU for each event transition, generating the event
     * transition code in the POU, adding the POU to the output, and adding a function call statement to the POU in the
     * returned event code.
     *
     * @param nameGenerator Generator for creating clash-free names in the generated code.
     * @param typeGenerator Generator for converting CIF types to PLC types.
     * @param eventTransitions Event transitions to convert.
     * @param mainProgressVar The progress variable to update from the generated event code inside the POU.
     * @param pous Storage for the generated POUs, in order of creating and calling the POUs.
     * @return The generated sequence of POU call statements for the main program that tries each of the given event
     *     transitions once.
     */
    private List<PlcStatement> convertEventTransitions(NameGenerator nameGenerator, TypeGenerator typeGenerator,
            List<CifEventTransition> eventTransitions, PlcBasicVariable mainProgressVar, List<PlcPou> pous)
    {
        List<PlcStatement> stats = listc(eventTransitions.size());
        for (CifEventTransition eventTrans: eventTransitions) {
            // Construct the POU with one input variable.
            String funcName = nameGenerator.generateGlobalNames(Set.of("tryEvent_"), eventTrans.event);
            PlcPou pou = makeTransitionsPou(nameGenerator, typeGenerator, List.of(eventTrans), "tryEvent_" + funcName,
                    mainProgressVar.varName);
            pous.add(pou);

            // Call the POU: isProgress := tryEvent_<EVENT_NAME>(isProgress);
            stats.add(createProgressPouCall(pou, mainProgressVar));
        }
        return stats;
    }

    /**
     * Constructs a call to the given POU with the given variable, and assigns the return value from the call again to
     * the variable.
     *
     * @param pou The POU that is called.
     * @param progressVar Progress variable that is given to the POU call as parameter, and is updated.
     * @return The statement performing the POU call and updating the variable.
     */
    private PlcStatement createProgressPouCall(PlcPou pou, PlcBasicVariable progressVar) {
        PlcPouDescription funcDesc = new PlcPouDescription(pou);
        PlcNamedValue argument = new PlcNamedValue(progressVar.varName, new PlcVarExpression(progressVar));
        PlcExpression rhs = new PlcFuncAppl(funcDesc, List.of(argument));
        PlcStatement assignment = new PlcAssignmentStatement(progressVar, rhs);
        return assignment;
    }

    /**
     * Construct a POU that tries to perform the provided CIF event transitions in the order as given. It takes a single
     * boolean input parameter named {@code paramName}. The generated POU tests whether each of the given event
     * transitions can be performed and if so, actually perform them. If at least one transition is performed in the
     * function, {@code TRUE} is returned. If none of the transitions was performed, the value of the given parameter is
     * returned.
     *
     * @param nameGenerator Generator for creating clash-free names in the generated code.
     * @param typeGenerator Generator for converting CIF types to PLC types.
     * @param cifEventSeq Sequence of transitions to translate.
     * @param pouName Name of the POU to generate.
     * @param paramName The name of the input parameter of the generated POU.
     * @return The constructed POU.
     */
    private PlcPou makeTransitionsPou(NameGenerator nameGenerator, TypeGenerator typeGenerator,
            List<CifEventTransition> cifEventSeq, String pouName, String paramName)
    {
        // Construct a POU for the generated code and add the POU parameter.
        PlcPou pou = new PlcPou(pouName, PlcPouType.FUNCTION, PlcElementaryType.BOOL_TYPE);
        PlcDataVariable funcIsProgressParam = new PlcDataVariable(paramName, PlcElementaryType.BOOL_TYPE);
        pou.inputVars.add(funcIsProgressParam);

        // Initialize function POU code.
        List<PlcStatement> stats = list();

        // Copy the input variable to a local variable.
        ExprGenerator pouExprGen = new ExprGenerator(this, nameGenerator, typeGenerator,
                varStorage.getCifDataProvider());
        PlcDataVariable funcIsProgressVar = pouExprGen.makeLocalVariable("funcIsProgress", PlcElementaryType.BOOL_TYPE);
        pou.tempVars.add(funcIsProgressVar);
        PlcExpression copyRhs = new PlcVarExpression(funcIsProgressParam);
        PlcStatement copyAssignment = new PlcAssignmentStatement(funcIsProgressVar, copyRhs);
        stats.add(copyAssignment);

        // Construct the PLC code of the transition.
        List<List<CifEventTransition>> eventSeqs = List.of(cifEventSeq);
        stats.addAll(transitionGenerator.generate(eventSeqs, pouExprGen, funcIsProgressVar).get(0));
        pou.tempVars.addAll(pouExprGen.getCreatedTempVariables());
        // Don't bother cleaning up the expression generator of the POU as it gets discarded.

        // POU must return a boolean value.
        stats.add(new PlcCommentLine(null));
        stats.add(new PlcCommentLine("Return event execution progress."));
        stats.add(new PlcReturnStatement(new PlcVarExpression(funcIsProgressVar)));

        // Convert statements to text, store inside the POU, and return the created POU.
        modelTextGenerator.toText(stats, pou.body, pou.name, true);
        return pou;
    }

    /**
     * Storage for generated event transitions code and supporting event functions.
     *
     * @param unconTransCode Code to execute in main program scope to (try to) perform each uncontrollable event once.
     * @param conTransCode Code to execute in main program scope to (try to) perform each controllable event once.
     * @param eventFunctions Support functions to perform event transitions. May be empty.
     */
    public static record EventTransitionsCode(List<PlcStatement> unconTransCode, List<PlcStatement> conTransCode,
            List<PlcPou> eventFunctions)
    {
    }

    /**
     * Get the writer for writing the generated PLC code to the file system.
     *
     * @return The requested PLC code writer.
     */
    protected abstract Writer getPlcCodeWriter();

    @Override
    public PlcTargetType getTargetType() {
        return targetType;
    }

    @Override
    public ModelTextGenerator getModelTextGenerator() {
        Assert.notNull(modelTextGenerator);
        return modelTextGenerator;
    }

    @Override
    public CifProcessor getCifProcessor() {
        Assert.notNull(cifProcessor);
        return cifProcessor;
    }

    @Override
    public TransitionGenerator getTransitionGenerator() {
        Assert.notNull(transitionGenerator);
        return transitionGenerator;
    }

    @Override
    public ContinuousVariablesGenerator getContinuousVariablesGenerator() {
        Assert.notNull(continuousVariablesGenerator);
        return continuousVariablesGenerator;
    }

    @Override
    public VariableStorage getVarStorage() {
        Assert.notNull(varStorage);
        return varStorage;
    }

    @Override
    public PlcCodeStorage getCodeStorage() {
        Assert.notNull(codeStorage);
        return codeStorage;
    }

    @Override
    public boolean isAllowedName(String name) {
        return !disallowedNames.contains(name);
    }

    @Override
    public ConvertEnums getActualEnumerationsConversion() {
        return selectedEnumConversion;
    }

    @Override
    public String getUsageVariableText(PlcVariablePurpose purpose, String varName) {
        return varName;
    }

    @Override
    public String getTonFuncBlockCallName() {
        return tonFuncBlockCallName;
    }

    /**
     * Common code that decides allowance for a small subset of constants.
     *
     * <p>
     * Allowed constants are values of boolean, integer, real and enumeration type.
     * </p>
     *
     * @param constant Constant to consider.
     * @return Whether the give constant is part of the subset.
     */
    protected static boolean commonSupportedConstants(Constant constant) {
        CifType tp = CifTypeUtils.unwrapType(constant.getType());
        return tp instanceof BoolType || tp instanceof IntType || tp instanceof RealType || tp instanceof EnumType;
    }

    @Override
    public EnumSet<PlcFuncNotation> getSupportedFuncNotations(PlcFuncOperation funcOper, int numArgs) {
        if (funcOper == PlcFuncOperation.AND_SHORT_CIRCUIT_OP || funcOper == PlcFuncOperation.OR_SHORT_CIRCUIT_OP) {
            // By default, disable short-circuit AND / OR functions, since the PLC standard doesn't have them.
            return PlcFuncNotation.UNSUPPORTED;
        } else {
            return PlcFuncNotation.ALL;
        }
    }

    @Override
    public int getExprPriority(ExprBinding exprBinding) {
        return switch (exprBinding) {
            case UNARY_NEGATE, UNARY_NOT -> 1;
            case BINARY_DIV, BINARY_MOD, BINARY_MUL -> 3;
            case BINARY_ADD, BINARY_SUB -> 4;
            case BINARY_GREATER_EQUAL, BINARY_GREATER_THAN, BINARY_LESS_EQUAL, BINARY_LESS_THAN -> 5;
            case BINARY_EQUAL, BINARY_UNEQUAL -> 6;
            case BINARY_AND -> 7;
            case BINARY_XOR -> 8;
            case BINARY_OR -> 9;
            case NO_PRIORITY -> Integer.MAX_VALUE;
        };
    }

    @Override
    public ExprAssociativity getExprAssociativity(ExprBinding exprBinding) {
        return switch (exprBinding) {
            case UNARY_NEGATE, UNARY_NOT -> ExprAssociativity.RIGHT;
            case BINARY_DIV, BINARY_MOD, BINARY_MUL, BINARY_ADD, BINARY_SUB -> ExprAssociativity.LEFT;
            case BINARY_GREATER_EQUAL, BINARY_GREATER_THAN -> ExprAssociativity.ALWAYS;
            case BINARY_LESS_EQUAL, BINARY_LESS_THAN -> ExprAssociativity.ALWAYS;
            case BINARY_EQUAL, BINARY_UNEQUAL -> ExprAssociativity.ALWAYS;
            case BINARY_AND, BINARY_XOR, BINARY_OR -> ExprAssociativity.LEFT;
            case NO_PRIORITY -> ExprAssociativity.NONE;
        };
    }

    @Override
    public PlcFuncTypeExtension getTypeExtension(PlcFuncOperation funcOper) {
        return PlcFuncTypeExtension.NEVER;
    }

    @Override
    public int getMaxIntegerTypeSize() {
        return last(getSupportedIntegerTypes()).bitSize;
    }

    @Override
    public PlcElementaryType getStdIntegerType() {
        int generatorBestIntSize = Math.min(CIF_INTEGER_SIZE, getMaxIntegerTypeSize());
        int userSpecifiedIntSize = intTypeSize.getTypeSize(generatorBestIntSize);
        return PlcElementaryType.getTypeByRequiredBits(userSpecifiedIntSize, PlcElementaryType.INTEGER_TYPES_ALL);
    }

    @Override
    public int getMaxRealTypeSize() {
        return last(getSupportedRealTypes()).bitSize;
    }

    @Override
    public PlcElementaryType getStdRealType() {
        int generatorBestRealSize = Math.min(CIF_REAL_SIZE, getMaxRealTypeSize());
        int userSpecifiedRealSize = realTypeSize.getTypeSize(generatorBestRealSize);
        return PlcElementaryType.getTypeByRequiredBits(userSpecifiedRealSize, PlcElementaryType.REAL_TYPES_ALL);
    }

    @Override
    public void verifyIoTableEntry(IoAddress parsedAddress, PlcType plcTableType, IoDirection directionFromCif,
            String ioName, String tableLinePositionText)
    {
        // Get the maximum supported width for the type.
        int maxAvailableBits;
        String typeText;
        if (PlcElementaryType.isIntType(plcTableType)) {
            maxAvailableBits = getMaxIntegerTypeSize();
            typeText = "integer";
        } else if (PlcElementaryType.isRealType(plcTableType)) {
            maxAvailableBits = getMaxRealTypeSize();
            typeText = "real";
        } else if (plcTableType.equals(PlcElementaryType.BOOL_TYPE)) {
            maxAvailableBits = 1;
            typeText = "boolean";
        } else {
            throw new AssertionError("Unexpected PLC type \"" + plcTableType + "\" found.");
        }

        // Check that the address size is within supported limits. If not, give a warning.
        if (parsedAddress.size() > maxAvailableBits) {
            warnOutput.line(
                    "Size of I/O address \"%s\" (of %d bits) exceeds the size of the largest supported %s type "
                            + "(of %d bits).",
                    parsedAddress.getAddress(), parsedAddress.size(), typeText, maxAvailableBits);
        }

        // Check a supplied I/O variable name for being acceptable to the target.
        if (ioName != null && !checkIoVariableName(ioName)) {
            String msg = fmt("I/O variable name \"%s\" %s is not a valid name for the selected target.",
                    ioName, tableLinePositionText);
            throw new InvalidInputException(msg);
        }
    }

    @Override
    public boolean checkIoVariableName(String name) {
        // The generic implementation checks the name for being a regular ASCII identifier with a few limitations on
        // underscore character usage (not at start or end, and no consecutive underscore characters).
        Assert.notNull(name);

        Pattern p = Pattern.compile("[A-Za-z][A-Za-z0-9_]*");
        Matcher m = p.matcher(name);
        if (!m.matches()) {
            return false;
        }

        // Limit underscore characters to single underscores within the name.
        return !name.startsWith("_") && !name.endsWith("_") && !name.contains("__");
    }

    @Override
    public void writeOutput(PlcProject project) {
        Writer writer = getPlcCodeWriter();
        writer.write(project, outputPaths);
    }
}
