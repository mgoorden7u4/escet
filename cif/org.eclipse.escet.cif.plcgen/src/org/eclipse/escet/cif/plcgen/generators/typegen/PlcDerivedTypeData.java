//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.generators.typegen;

import java.util.Set;

import org.eclipse.escet.cif.plcgen.model.types.PlcDerivedType;

/** Storage class for a derived type and its direct child derived type dependencies. */
public class PlcDerivedTypeData {
    /** The stored derived type. */
    public final PlcDerivedType derivedType;

    /** Derived types directly needed by {@link #derivedType}. */
    public final Set<PlcDerivedType> childDeps;

    /**
     * Constructor of the {@link PlcDerivedTypeData} class.
     *
     * @param derivedType The stored derived type.
     * @param childDeps Derived types directly needed by {@code derivedType}.
     */
    public PlcDerivedTypeData(PlcDerivedType derivedType, Set<PlcDerivedType> childDeps) {
        this.derivedType = derivedType;
        this.childDeps = childDeps;
    }
}
