/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[1..5]; int[-2..10]; int[-2..11])". */
            class CifTuple_T3III {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /** The 3rd field. */
                _field2;

                /**
                 * Constructor for the {@link CifTuple_T3III} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 * @param _field2 The 3rd field.
                 */
                constructor(_field0, _field1, _field2) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                    this._field2 = _field2;
                }

                copy() {
                    return new CifTuple_T3III(this._field0, this._field1, this._field2);
                }

                toString() {
                    var rslt = '(';
                    rslt += databased_supervisorUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += databased_supervisorUtils.valueToStr(this._field1);
                    rslt += ', ';
                    rslt += databased_supervisorUtils.valueToStr(this._field2);
                    rslt += ')';
                    return rslt;
                }
            }

/** databased_supervisor code generated from a CIF specification. */
class databased_supervisor_class {
    /** databased_supervisorEnum declaration. It contains the single merged enum from the CIF model. */
    databased_supervisorEnum = Object.freeze({
        /** Literal "Idle". */
        _Idle: Symbol("Idle"),

        /** Literal "Off". */
        _Off: Symbol("Off"),

        /** Literal "On". */
        _On: Symbol("On"),

        /** Literal "Pushed". */
        _Pushed: Symbol("Pushed"),

        /** Literal "Released". */
        _Released: Symbol("Released"),

        /** Literal "Running". */
        _Running: Symbol("Running"),

        /** Literal "StartTimer". */
        _StartTimer: Symbol("StartTimer"),

        /** Literal "TurnLampOff". */
        _TurnLampOff: Symbol("TurnLampOff"),

        /** Literal "TurnLampOn". */
        _TurnLampOn: Symbol("TurnLampOn"),

        /** Literal "WaitForButtonPush". */
        _WaitForButtonPush: Symbol("WaitForButtonPush"),

        /** Literal "WaitForTimeout". */
        _WaitForTimeout: Symbol("WaitForTimeout")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "Button.u_pushed",
        "Button.u_released",
        "Lamp.c_off",
        "Lamp.c_on",
        "Timer.c_start",
        "Timer.u_timeout"
    ];

    /** Constant "bdd_nodes". */
    bdd_nodes_;

    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "Button". */
    Button_;

    /** Discrete variable "Cycle". */
    Cycle_;

    /** Discrete variable "Lamp". */
    Lamp_;

    /** Discrete variable "Timer". */
    Timer_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws databased_supervisorException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) databased_supervisor.log('Initial state: ' + databased_supervisor.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute SVG input edges as long as they are possible.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "Button.u_pushed".
            edgeExecuted |= this.execEdge0();

            // Event "Button.u_released".
            edgeExecuted |= this.execEdge1();

            // Event "Timer.u_timeout".
            edgeExecuted |= this.execEdge2();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "Lamp.c_off".
            edgeExecuted |= this.execEdge3();

            // Event "Lamp.c_on".
            edgeExecuted |= this.execEdge4();

            // Event "Timer.c_start".
            edgeExecuted |= this.execEdge5();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws databased_supervisorException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws databased_supervisorException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (databased_supervisor.first) {
                    databased_supervisor.first = false;
                    databased_supervisor.startMilli = now;
                    databased_supervisor.targetMilli = databased_supervisor.startMilli;
                    preMilli = databased_supervisor.startMilli;
                }

                // Handle pausing/playing.
                if (!databased_supervisor.playing) {
                    databased_supervisor.timePaused = now;
                    return;
                }

                if (databased_supervisor.timePaused) {
                    databased_supervisor.startMilli += (now - databased_supervisor.timePaused);
                    databased_supervisor.targetMilli += (now - databased_supervisor.timePaused);
                    databased_supervisor.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = databased_supervisor.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - databased_supervisor.startMilli;

                // Execute once.
                databased_supervisor.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (databased_supervisor.doInfoExec) {
                    databased_supervisor.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    databased_supervisor.targetMilli += cycleMilli;
                    remainderMilli = databased_supervisor.targetMilli - postMilli;
                }

                // Execute again.
                databased_supervisor.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "Button.u_pushed".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge0() {
        var guard = ((databased_supervisor.Button_) == (databased_supervisor.databased_supervisorEnum._Released)) && ((((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForButtonPush)) || ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOn))) || (((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._StartTimer)) || (((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForTimeout)) || ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOff)))));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(0, true);
        if (this.doInfoEvent) this.infoEvent(0, true);

        databased_supervisor.Button_ = databased_supervisor.databased_supervisorEnum._Pushed;
        if ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForButtonPush)) {
            databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._TurnLampOn;
        } else if ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOn)) {
            databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._TurnLampOn;
        } else if ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._StartTimer)) {
            databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._StartTimer;
        } else if ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForTimeout)) {
            databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._WaitForTimeout;
        } else if ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOff)) {
            databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._TurnLampOff;
        }

        if (this.doInfoEvent) this.infoEvent(0, false);
        if (this.doInfoPrintOutput) this.printOutput(0, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 1 and event "Button.u_released".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge1() {
        var guard = (databased_supervisor.Button_) == (databased_supervisor.databased_supervisorEnum._Pushed);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(1, true);
        if (this.doInfoEvent) this.infoEvent(1, true);

        databased_supervisor.Button_ = databased_supervisor.databased_supervisorEnum._Released;

        if (this.doInfoEvent) this.infoEvent(1, false);
        if (this.doInfoPrintOutput) this.printOutput(1, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 2 and event "Timer.u_timeout".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge2() {
        var guard = ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForTimeout)) && ((databased_supervisor.Timer_) == (databased_supervisor.databased_supervisorEnum._Running));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(5, true);
        if (this.doInfoEvent) this.infoEvent(5, true);

        databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._TurnLampOff;
        databased_supervisor.Timer_ = databased_supervisor.databased_supervisorEnum._Idle;

        if (this.doInfoEvent) this.infoEvent(5, false);
        if (this.doInfoPrintOutput) this.printOutput(5, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 3 and event "Lamp.c_off".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge3() {
        var guard = (((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOff)) && ((databased_supervisor.Lamp_) == (databased_supervisor.databased_supervisorEnum._On))) && (databased_supervisor.bdd_eval_(5, databased_supervisor.bdd_values_()));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(2, true);
        if (this.doInfoEvent) this.infoEvent(2, true);

        databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._WaitForButtonPush;
        databased_supervisor.Lamp_ = databased_supervisor.databased_supervisorEnum._Off;

        if (this.doInfoEvent) this.infoEvent(2, false);
        if (this.doInfoPrintOutput) this.printOutput(2, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 4 and event "Lamp.c_on".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge4() {
        var guard = (((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOn)) && ((databased_supervisor.Lamp_) == (databased_supervisor.databased_supervisorEnum._Off))) && (databased_supervisor.bdd_eval_(0, databased_supervisor.bdd_values_()));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(3, true);
        if (this.doInfoEvent) this.infoEvent(3, true);

        databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._StartTimer;
        databased_supervisor.Lamp_ = databased_supervisor.databased_supervisorEnum._On;

        if (this.doInfoEvent) this.infoEvent(3, false);
        if (this.doInfoPrintOutput) this.printOutput(3, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 5 and event "Timer.c_start".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge5() {
        var guard = ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._StartTimer)) && ((databased_supervisor.bdd_eval_(9, databased_supervisor.bdd_values_())) && ((databased_supervisor.Timer_) == (databased_supervisor.databased_supervisorEnum._Idle)));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(4, true);
        if (this.doInfoEvent) this.infoEvent(4, true);

        databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._WaitForTimeout;
        databased_supervisor.Timer_ = databased_supervisor.databased_supervisorEnum._Running;

        if (this.doInfoEvent) this.infoEvent(4, false);
        if (this.doInfoPrintOutput) this.printOutput(4, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /** Initializes the state. */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;
            databased_supervisor.bdd_nodes_ = [new CifTuple_T3III(1, -(2), 1), new CifTuple_T3III(2, 2, -(2)), new CifTuple_T3III(3, 3, -(2)), new CifTuple_T3III(4, 4, -(2)), new CifTuple_T3III(5, -(1), -(2)), new CifTuple_T3III(1, 6, -(2)), new CifTuple_T3III(2, 7, -(2)), new CifTuple_T3III(3, -(2), 8), new CifTuple_T3III(4, -(2), 4), new CifTuple_T3III(1, 10, -(2)), new CifTuple_T3III(2, -(2), 11), new CifTuple_T3III(3, 8, -(2))];
        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;

        // CIF model state variables.
        databased_supervisor.Button_ = databased_supervisor.databased_supervisorEnum._Released;
        databased_supervisor.Cycle_ = databased_supervisor.databased_supervisorEnum._WaitForButtonPush;
        databased_supervisor.Lamp_ = databased_supervisor.databased_supervisorEnum._Off;
        databased_supervisor.Timer_ = databased_supervisor.databased_supervisorEnum._Idle;
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) databased_supervisor.log(databased_supervisorUtils.fmt('Transition: event %s', databased_supervisor.getEventName(idx)));
        } else {
            if (this.doStateOutput) databased_supervisor.log('State: ' + databased_supervisor.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = databased_supervisorUtils.fmt('time=%s', databased_supervisor.time);
        state += databased_supervisorUtils.fmt(', Button=%s', databased_supervisorUtils.valueToStr(databased_supervisor.Button_));
        state += databased_supervisorUtils.fmt(', Cycle=%s', databased_supervisorUtils.valueToStr(databased_supervisor.Cycle_));
        state += databased_supervisorUtils.fmt(', Lamp=%s', databased_supervisorUtils.valueToStr(databased_supervisor.Lamp_));
        state += databased_supervisorUtils.fmt(', Timer=%s', databased_supervisorUtils.valueToStr(databased_supervisor.Timer_));
        return state;
    }


    /**
     * Evaluates algebraic variable "bdd_value0".
     *
     * @return The evaluation result.
     */
    bdd_value0_() {
        return (databased_supervisor.Button_) == (databased_supervisor.databased_supervisorEnum._Pushed);
    }

    /**
     * Evaluates algebraic variable "bdd_value1".
     *
     * @return The evaluation result.
     */
    bdd_value1_() {
        return ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOn)) || ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForTimeout));
    }

    /**
     * Evaluates algebraic variable "bdd_value2".
     *
     * @return The evaluation result.
     */
    bdd_value2_() {
        return ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._StartTimer)) || ((databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._WaitForTimeout));
    }

    /**
     * Evaluates algebraic variable "bdd_value3".
     *
     * @return The evaluation result.
     */
    bdd_value3_() {
        return (databased_supervisor.Cycle_) == (databased_supervisor.databased_supervisorEnum._TurnLampOff);
    }

    /**
     * Evaluates algebraic variable "bdd_value4".
     *
     * @return The evaluation result.
     */
    bdd_value4_() {
        return (databased_supervisor.Lamp_) == (databased_supervisor.databased_supervisorEnum._On);
    }

    /**
     * Evaluates algebraic variable "bdd_value5".
     *
     * @return The evaluation result.
     */
    bdd_value5_() {
        return (databased_supervisor.Timer_) == (databased_supervisor.databased_supervisorEnum._Running);
    }

    /**
     * Evaluates algebraic variable "bdd_values".
     *
     * @return The evaluation result.
     */
    bdd_values_() {
        return [databased_supervisor.bdd_value0_(), databased_supervisor.bdd_value1_(), databased_supervisor.bdd_value2_(), databased_supervisor.bdd_value3_(), databased_supervisor.bdd_value4_(), databased_supervisor.bdd_value5_()];
    }


    /**
     * Function "bdd_eval".
     *
     * @param bdd_eval_idx_ Function parameter "bdd_eval.idx".
     * @param bdd_eval_values_ Function parameter "bdd_eval.values".
     * @return The return value of the function.
     */
    bdd_eval_(bdd_eval_idx_, bdd_eval_values_) {
        // Variable "bdd_eval.node".
        var bdd_eval_node_ = new CifTuple_T3III(0, 0, 0);

        // Variable "bdd_eval.val".
        var bdd_eval_val_ = false;

        // Execute statements in the function body.
        while ((bdd_eval_idx_) >= (0)) {
            bdd_eval_node_ = databased_supervisorUtils.projectList(databased_supervisor.bdd_nodes_, bdd_eval_idx_);

            bdd_eval_val_ = databased_supervisorUtils.projectList(bdd_eval_values_, (bdd_eval_node_)._field0);

            bdd_eval_idx_ = (bdd_eval_val_) ? (bdd_eval_node_)._field2 : ((bdd_eval_node_)._field1);
        }

        return databased_supervisorUtils.equalObjs(bdd_eval_idx_, -(1));
        throw new Error('No return statement at end of function.');
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link databased_supervisorUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            databased_supervisor.log(text);
        } else if (target == ':stderr') {
            databased_supervisor.error(text);
        } else {
            var path = databased_supervisorUtils.normalizePrintTarget(target);
            databased_supervisor.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
