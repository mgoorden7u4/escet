/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[1..1]; int[2..2])". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += types_and_literalsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(int[3..3]; int[4..4]; string)". */
            class CifTuple_T3IIS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /** The 3rd field. */
                _field2;

                /**
                 * Constructor for the {@link CifTuple_T3IIS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 * @param _field2 The 3rd field.
                 */
                constructor(_field0, _field1, _field2) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                    this._field2 = _field2;
                }

                copy() {
                    return new CifTuple_T3IIS(this._field0, this._field1, this._field2);
                }

                toString() {
                    var rslt = '(';
                    rslt += types_and_literalsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field1);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field2);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(tuple(int[5..5]; int[6..6]); string)". */
            class CifTuple_T2T2IIS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2T2IIS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2T2IIS(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += types_and_literalsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(real; string)". */
            class CifTuple_T2RS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2RS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2RS(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += types_and_literalsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(list[1] int[1..1]; list[2] tuple(real; string))". */
            class CifTuple_T2LILT2RS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2LILT2RS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2LILT2RS(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += types_and_literalsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += types_and_literalsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** types_and_literals code generated from a CIF specification. */
class types_and_literals_class {
    /** types_and_literalsEnum declaration. It contains the single merged enum from the CIF model. */
    types_and_literalsEnum = Object.freeze({
        /** Literal "BLUE". */
        _BLUE: Symbol("BLUE"),

        /** Literal "RED". */
        _RED: Symbol("RED"),

        /** Literal "WHITE". */
        _WHITE: Symbol("WHITE")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [

    ];

    /** Constant "c1". */
    c1_;

    /** Constant "c2". */
    c2_;

    /** Constant "c3". */
    c3_;

    /** Constant "c4". */
    c4_;

    /** Constant "c5". */
    c5_;

    /** Constant "c6". */
    c6_;

    /** Constant "c7". */
    c7_;

    /** Constant "c8". */
    c8_;

    /** Constant "c9". */
    c9_;

    /** Constant "c10". */
    c10_;

    /** Constant "c11". */
    c11_;

    /** Constant "c12". */
    c12_;

    /** Constant "c13". */
    c13_;

    /** Constant "c14". */
    c14_;

    /** Constant "c15". */
    c15_;

    /** Constant "c16". */
    c16_;

    /** Constant "c17". */
    c17_;

    /** Constant "c18". */
    c18_;

    /** Constant "c19". */
    c19_;

    /** Constant "c20". */
    c20_;

    /** Constant "c21". */
    c21_;

    /** Constant "c22". */
    c22_;

    /** Constant "c23". */
    c23_;

    /** Constant "c24". */
    c24_;

    /** Constant "c25". */
    c25_;

    /** Constant "c26". */
    c26_;

    /** Constant "c27". */
    c27_;

    /** Constant "c28". */
    c28_;

    /** Constant "c29". */
    c29_;

    /** Constant "c31". */
    c31_;

    /** Constant "c30". */
    c30_;

    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;




    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws types_and_literalsException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) types_and_literals.log('Initial state: ' + types_and_literals.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute SVG input edges as long as they are possible.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws types_and_literalsException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws types_and_literalsException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (types_and_literals.first) {
                    types_and_literals.first = false;
                    types_and_literals.startMilli = now;
                    types_and_literals.targetMilli = types_and_literals.startMilli;
                    preMilli = types_and_literals.startMilli;
                }

                // Handle pausing/playing.
                if (!types_and_literals.playing) {
                    types_and_literals.timePaused = now;
                    return;
                }

                if (types_and_literals.timePaused) {
                    types_and_literals.startMilli += (now - types_and_literals.timePaused);
                    types_and_literals.targetMilli += (now - types_and_literals.timePaused);
                    types_and_literals.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = types_and_literals.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - types_and_literals.startMilli;

                // Execute once.
                types_and_literals.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (types_and_literals.doInfoExec) {
                    types_and_literals.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    types_and_literals.targetMilli += cycleMilli;
                    remainderMilli = types_and_literals.targetMilli - postMilli;
                }

                // Execute again.
                types_and_literals.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }


    /** Initializes the state. */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;
            types_and_literals.c1_ = true;
            types_and_literals.c2_ = false;
            types_and_literals.c3_ = [true, false, true];
            types_and_literals.c4_ = 123;
            types_and_literals.c5_ = -(456);
            types_and_literals.c6_ = [-(3), 2, 0];
            types_and_literals.c7_ = [false, true, false];
            types_and_literals.c8_ = [0, 1, 1];
            types_and_literals.c9_ = types_and_literals.c8_;
            types_and_literals.c10_ = types_and_literals.types_and_literalsEnum._RED;
            types_and_literals.c11_ = types_and_literals.types_and_literalsEnum._WHITE;
            types_and_literals.c12_ = types_and_literals.c11_;
            types_and_literals.c13_ = 1.23;
            types_and_literals.c14_ = 1.0E308;
            types_and_literals.c15_ = [0.0, types_and_literalsUtils.negateReal(0.0), 0.0, 0.0, 0.01, 1.234E12, 1.0E12, 1.0E12, 1.0E-12, 1.0E-8];
            types_and_literals.c16_ = "";
            types_and_literals.c17_ = "abc";
            types_and_literals.c18_ = "d e f";
            types_and_literals.c19_ = "a\nb\tc\\d\"e";
            types_and_literals.c20_ = ["a", "a", "bcd"];
            types_and_literals.c21_ = [];
            types_and_literals.c22_ = [1];
            types_and_literals.c23_ = [true, false];
            types_and_literals.c24_ = types_and_literals.c23_;
            types_and_literals.c25_ = [[1.0, 2.0], [2.0, 3.0], [3.0, 4.0]];
            types_and_literals.c26_ = new CifTuple_T2II(1, 2);
            types_and_literals.c27_ = types_and_literals.c26_;
            types_and_literals.c28_ = new CifTuple_T3IIS(3, 4, "abc");
            types_and_literals.c29_ = new CifTuple_T2T2IIS(new CifTuple_T2II(5, 6), "def");
            types_and_literals.c31_ = [new CifTuple_T2LILT2RS([1], [new CifTuple_T2RS(2.0, "a"), new CifTuple_T2RS(3.0, "b")])];
            types_and_literals.c30_ = types_and_literals.c31_;
        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;

        // CIF model state variables.
        // No state variables, except variable 'time'.
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) types_and_literals.log(types_and_literalsUtils.fmt('Transition: event %s', types_and_literals.getEventName(idx)));
        } else {
            if (this.doStateOutput) types_and_literals.log('State: ' + types_and_literals.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = types_and_literalsUtils.fmt('time=%s', types_and_literals.time);

        return state;
    }





    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link types_and_literalsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            types_and_literals.log(text);
        } else if (target == ':stderr') {
            types_and_literals.error(text);
        } else {
            var path = types_and_literalsUtils.normalizePrintTarget(target);
            types_and_literals.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
