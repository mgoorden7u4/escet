/** Tuples. */


/** ctrl_chk_exec_scheme code generated from a CIF specification. */
class ctrl_chk_exec_scheme_class {
    /** ctrl_chk_exec_schemeEnum declaration. It contains the single merged enum from the CIF model. */
    ctrl_chk_exec_schemeEnum = Object.freeze({
        /** Literal "__some_dummy_enum_literal". */
        ___some_dummy_enum_literal: Symbol("__some_dummy_enum_literal")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "chan",
        "edge_order.e",
        "event_order_abs1.a",
        "event_order_abs2.a",
        "event_order_abs3.a",
        "event_order_casing.aAa",
        "event_order_casing.aaA",
        "event_order_casing.aaa",
        "event_order_escaping.tim",
        "event_order_escaping.time",
        "event_order_escaping.timer",
        "event_order_prefix.x",
        "event_order_prefix.x2",
        "event_order_prefix.x2b",
        "event_order_sort.a",
        "event_order_sort.b",
        "event_order_sort.c",
        "one_trans_per_evt1.a",
        "one_trans_per_evt1.b",
        "one_trans_per_evt2.e",
        "one_trans_per_evt3.a",
        "one_trans_per_evt3.b",
        "one_trans_per_evt3.c",
        "one_trans_per_evt3.d",
        "unctrl_vs_ctrl.c1",
        "unctrl_vs_ctrl.c2",
        "unctrl_vs_ctrl.u1",
        "unctrl_vs_ctrl.u2"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "chan_rcv1.x". */
    chan_rcv1_x_;

    /** Discrete variable "chan_rcv2.x". */
    chan_rcv2_x_;

    /** Discrete variable "chan_sync1.v". */
    chan_sync1_v_;

    /** Discrete variable "edge_order.v". */
    edge_order_v_;

    /** Discrete variable "event_order_abs2.v". */
    event_order_abs2_v_;

    /** Discrete variable "event_order_casing.v". */
    event_order_casing_v_;

    /** Discrete variable "event_order_escaping.v". */
    event_order_escaping_v_;

    /** Discrete variable "event_order_prefix.v". */
    event_order_prefix_v_;

    /** Discrete variable "event_order_sort.v". */
    event_order_sort_v_;

    /** Discrete variable "one_trans_per_evt1.v". */
    one_trans_per_evt1_v_;

    /** Discrete variable "one_trans_per_evt2a.v". */
    one_trans_per_evt2a_v_;

    /** Discrete variable "one_trans_per_evt2a.w". */
    one_trans_per_evt2a_w_;

    /** Discrete variable "one_trans_per_evt2b.w". */
    one_trans_per_evt2b_w_;

    /** Discrete variable "one_trans_per_evt3.v". */
    one_trans_per_evt3_v_;

    /** Discrete variable "one_trans_per_evt3.x". */
    one_trans_per_evt3_x_;

    /** Discrete variable "one_trans_per_evt3.w". */
    one_trans_per_evt3_w_;

    /** Discrete variable "unctrl_vs_ctrl.v". */
    unctrl_vs_ctrl_v_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws ctrl_chk_exec_schemeException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) ctrl_chk_exec_scheme.log('Initial state: ' + ctrl_chk_exec_scheme.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute SVG input edges as long as they are possible.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "chan".
            edgeExecuted |= this.execEdge0();

            // Event "edge_order.e".
            edgeExecuted |= this.execEdge1();

            // Event "one_trans_per_evt1.a".
            edgeExecuted |= this.execEdge2();

            // Event "one_trans_per_evt1.b".
            edgeExecuted |= this.execEdge3();

            // Event "one_trans_per_evt2.e".
            edgeExecuted |= this.execEdge4();

            // Event "one_trans_per_evt3.a".
            edgeExecuted |= this.execEdge5();

            // Event "one_trans_per_evt3.b".
            edgeExecuted |= this.execEdge6();

            // Event "one_trans_per_evt3.c".
            edgeExecuted |= this.execEdge7();

            // Event "one_trans_per_evt3.d".
            edgeExecuted |= this.execEdge8();

            // Event "unctrl_vs_ctrl.u1".
            edgeExecuted |= this.execEdge9();

            // Event "unctrl_vs_ctrl.u2".
            edgeExecuted |= this.execEdge10();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "event_order_abs1.a".
            edgeExecuted |= this.execEdge11();

            // Event "event_order_abs2.a".
            edgeExecuted |= this.execEdge12();

            // Event "event_order_abs3.a".
            edgeExecuted |= this.execEdge13();

            // Event "event_order_casing.aAa".
            edgeExecuted |= this.execEdge14();

            // Event "event_order_casing.aaA".
            edgeExecuted |= this.execEdge15();

            // Event "event_order_casing.aaa".
            edgeExecuted |= this.execEdge16();

            // Event "event_order_escaping.tim".
            edgeExecuted |= this.execEdge17();

            // Event "event_order_escaping.time".
            edgeExecuted |= this.execEdge18();

            // Event "event_order_escaping.timer".
            edgeExecuted |= this.execEdge19();

            // Event "event_order_prefix.x".
            edgeExecuted |= this.execEdge20();

            // Event "event_order_prefix.x2".
            edgeExecuted |= this.execEdge21();

            // Event "event_order_prefix.x2b".
            edgeExecuted |= this.execEdge22();

            // Event "event_order_sort.a".
            edgeExecuted |= this.execEdge23();

            // Event "event_order_sort.b".
            edgeExecuted |= this.execEdge24();

            // Event "event_order_sort.c".
            edgeExecuted |= this.execEdge25();

            // Event "unctrl_vs_ctrl.c1".
            edgeExecuted |= this.execEdge26();

            // Event "unctrl_vs_ctrl.c2".
            edgeExecuted |= this.execEdge27();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws ctrl_chk_exec_schemeException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws ctrl_chk_exec_schemeException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (ctrl_chk_exec_scheme.first) {
                    ctrl_chk_exec_scheme.first = false;
                    ctrl_chk_exec_scheme.startMilli = now;
                    ctrl_chk_exec_scheme.targetMilli = ctrl_chk_exec_scheme.startMilli;
                    preMilli = ctrl_chk_exec_scheme.startMilli;
                }

                // Handle pausing/playing.
                if (!ctrl_chk_exec_scheme.playing) {
                    ctrl_chk_exec_scheme.timePaused = now;
                    return;
                }

                if (ctrl_chk_exec_scheme.timePaused) {
                    ctrl_chk_exec_scheme.startMilli += (now - ctrl_chk_exec_scheme.timePaused);
                    ctrl_chk_exec_scheme.targetMilli += (now - ctrl_chk_exec_scheme.timePaused);
                    ctrl_chk_exec_scheme.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = ctrl_chk_exec_scheme.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - ctrl_chk_exec_scheme.startMilli;

                // Execute once.
                ctrl_chk_exec_scheme.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (ctrl_chk_exec_scheme.doInfoExec) {
                    ctrl_chk_exec_scheme.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    ctrl_chk_exec_scheme.targetMilli += cycleMilli;
                    remainderMilli = ctrl_chk_exec_scheme.targetMilli - postMilli;
                }

                // Execute again.
                ctrl_chk_exec_scheme.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "chan".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge0() {
        var guard = ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) || (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) && (((((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) || ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1)))) || (((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) || (((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) || ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1)))))) || ((((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) || (((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) || ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1))))) || (((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) || (((ctrl_chk_exec_scheme.chan_rcv1_x_) > (2)) || ((ctrl_chk_exec_scheme.chan_rcv2_x_) > (2))))));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(0, true);
        if (this.doInfoEvent) this.infoEvent(0, true);

        if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (1);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (2);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (4);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (8);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (16);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 1)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (32);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (64);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 1))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (128);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if ((ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 2)) && (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 2))) {
            var rhs1 = ((ctrl_chk_exec_scheme.chan_sync1_v_) + (ctrl_chk_exec_scheme.chan_sync1_v_)) + (256);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("chan_sync1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.chan_sync1_v_ = rhs1;
        } else if (((ctrl_chk_exec_scheme.chan_rcv1_x_) > (2)) || ((ctrl_chk_exec_scheme.chan_rcv2_x_) > (2))) {
            ctrl_chk_exec_scheme.chan_sync1_v_ = 999;
        }
        if (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv1_x_, 0)) {
            ctrl_chk_exec_scheme.chan_rcv1_x_ = 1;
        } else if (ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.chan_rcv2_x_, 0)) {
            ctrl_chk_exec_scheme.chan_rcv2_x_ = 1;
        }

        if (this.doInfoEvent) this.infoEvent(0, false);
        if (this.doInfoPrintOutput) this.printOutput(0, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 1 and event "edge_order.e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge1() {
        var guard = ((ctrl_chk_exec_scheme.edge_order_v_) <= (0)) || (((ctrl_chk_exec_scheme.edge_order_v_) <= (1)) || ((ctrl_chk_exec_scheme.edge_order_v_) <= (4)));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(1, true);
        if (this.doInfoEvent) this.infoEvent(1, true);

        if ((ctrl_chk_exec_scheme.edge_order_v_) <= (0)) {
            var rhs1 = ((ctrl_chk_exec_scheme.edge_order_v_) + (ctrl_chk_exec_scheme.edge_order_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("edge_order.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.edge_order_v_ = rhs1;
        } else if ((ctrl_chk_exec_scheme.edge_order_v_) <= (1)) {
            var rhs1 = ((ctrl_chk_exec_scheme.edge_order_v_) + (ctrl_chk_exec_scheme.edge_order_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("edge_order.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.edge_order_v_ = rhs1;
        } else if ((ctrl_chk_exec_scheme.edge_order_v_) <= (4)) {
            var rhs1 = ((ctrl_chk_exec_scheme.edge_order_v_) + (ctrl_chk_exec_scheme.edge_order_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("edge_order.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.edge_order_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(1, false);
        if (this.doInfoPrintOutput) this.printOutput(1, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 2 and event "one_trans_per_evt1.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge2() {
        var guard = ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) <= (0)) || ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) <= (4));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(17, true);
        if (this.doInfoEvent) this.infoEvent(17, true);

        if ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) <= (0)) {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) + (ctrl_chk_exec_scheme.one_trans_per_evt1_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt1_v_ = rhs1;
        } else if ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) <= (4)) {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) + (ctrl_chk_exec_scheme.one_trans_per_evt1_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt1_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(17, false);
        if (this.doInfoPrintOutput) this.printOutput(17, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 3 and event "one_trans_per_evt1.b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge3() {
        var guard = (ctrl_chk_exec_scheme.one_trans_per_evt1_v_) <= (12);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(18, true);
        if (this.doInfoEvent) this.infoEvent(18, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt1_v_) + (ctrl_chk_exec_scheme.one_trans_per_evt1_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt1.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt1_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(18, false);
        if (this.doInfoPrintOutput) this.printOutput(18, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 4 and event "one_trans_per_evt2.e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge4() {
        var guard = (((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) <= (1)) || ((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) <= (3))) && ((ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) % (2), 0)) || (ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) % (2), 1)));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(19, true);
        if (this.doInfoEvent) this.infoEvent(19, true);

        if (ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) % (2), 0)) {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt2b_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt2b_w_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2b.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt2b_w_ = rhs1;
        } else if (ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) % (2), 1)) {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt2b_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt2b_w_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2b.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt2b_w_ = rhs1;
        }
        if ((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) <= (1)) {
            {
                var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) + (1);
                if ((rhs1) > 5) {
                    ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2a.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
                }
                ctrl_chk_exec_scheme.one_trans_per_evt2a_v_ = rhs1;
            }
            {
                var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt2a_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt2a_w_)) + (1);
                if ((rhs1) > 50) {
                    ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2a.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
                }
                ctrl_chk_exec_scheme.one_trans_per_evt2a_w_ = rhs1;
            }
        } else if ((ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) <= (3)) {
            {
                var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt2a_v_) + (1);
                if ((rhs1) > 5) {
                    ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2a.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
                }
                ctrl_chk_exec_scheme.one_trans_per_evt2a_v_ = rhs1;
            }
            {
                var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt2a_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt2a_w_)) + (2);
                if ((rhs1) > 50) {
                    ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt2a.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
                }
                ctrl_chk_exec_scheme.one_trans_per_evt2a_w_ = rhs1;
            }
        }

        if (this.doInfoEvent) this.infoEvent(19, false);
        if (this.doInfoPrintOutput) this.printOutput(19, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 5 and event "one_trans_per_evt3.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge5() {
        var guard = ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.one_trans_per_evt3_v_, 1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(20, true);
        if (this.doInfoEvent) this.infoEvent(20, true);

        {
            var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt3_v_) + (1);
            if ((rhs1) > 5) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_v_ = rhs1;
        }
        {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt3_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt3_w_)) + (1);
            if ((rhs1) > 40) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..40]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_w_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(20, false);
        if (this.doInfoPrintOutput) this.printOutput(20, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 6 and event "one_trans_per_evt3.b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge6() {
        var guard = ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.one_trans_per_evt3_v_, 2);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(21, true);
        if (this.doInfoEvent) this.infoEvent(21, true);

        {
            var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt3_v_) + (1);
            if ((rhs1) > 5) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_v_ = rhs1;
        }
        {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt3_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt3_w_)) + (2);
            if ((rhs1) > 40) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..40]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_w_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(21, false);
        if (this.doInfoPrintOutput) this.printOutput(21, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 7 and event "one_trans_per_evt3.c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge7() {
        var guard = ctrl_chk_exec_schemeUtils.equalObjs(ctrl_chk_exec_scheme.one_trans_per_evt3_v_, 0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(22, true);
        if (this.doInfoEvent) this.infoEvent(22, true);

        {
            var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt3_v_) + (1);
            if ((rhs1) > 5) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_v_ = rhs1;
        }
        {
            var rhs1 = ((ctrl_chk_exec_scheme.one_trans_per_evt3_w_) + (ctrl_chk_exec_scheme.one_trans_per_evt3_w_)) + (4);
            if ((rhs1) > 40) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.w", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..40]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_w_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(22, false);
        if (this.doInfoPrintOutput) this.printOutput(22, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 8 and event "one_trans_per_evt3.d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge8() {
        var guard = (ctrl_chk_exec_scheme.one_trans_per_evt3_v_) < (3);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(23, true);
        if (this.doInfoEvent) this.infoEvent(23, true);

        {
            var rhs1 = (ctrl_chk_exec_scheme.one_trans_per_evt3_x_) + (1);
            if ((rhs1) > 5) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("one_trans_per_evt3.x", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..5]");
            }
            ctrl_chk_exec_scheme.one_trans_per_evt3_x_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(23, false);
        if (this.doInfoPrintOutput) this.printOutput(23, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 9 and event "unctrl_vs_ctrl.u1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge9() {
        var guard = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) < (500)) && (ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) % (2), 0));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(26, true);
        if (this.doInfoEvent) this.infoEvent(26, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) + (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_)) + (1);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("unctrl_vs_ctrl.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(26, false);
        if (this.doInfoPrintOutput) this.printOutput(26, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 10 and event "unctrl_vs_ctrl.u2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge10() {
        var guard = (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) < (10);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(27, true);
        if (this.doInfoEvent) this.infoEvent(27, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) + (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_)) + (2);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("unctrl_vs_ctrl.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(27, false);
        if (this.doInfoPrintOutput) this.printOutput(27, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 11 and event "event_order_abs1.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge11() {
        var guard = (ctrl_chk_exec_scheme.event_order_abs2_v_) <= (0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(2, true);
        if (this.doInfoEvent) this.infoEvent(2, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_abs2_v_) + (ctrl_chk_exec_scheme.event_order_abs2_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_abs2.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_abs2_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(2, false);
        if (this.doInfoPrintOutput) this.printOutput(2, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 12 and event "event_order_abs2.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge12() {
        var guard = (ctrl_chk_exec_scheme.event_order_abs2_v_) <= (1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(3, true);
        if (this.doInfoEvent) this.infoEvent(3, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_abs2_v_) + (ctrl_chk_exec_scheme.event_order_abs2_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_abs2.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_abs2_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(3, false);
        if (this.doInfoPrintOutput) this.printOutput(3, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 13 and event "event_order_abs3.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge13() {
        var guard = (ctrl_chk_exec_scheme.event_order_abs2_v_) <= (4);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(4, true);
        if (this.doInfoEvent) this.infoEvent(4, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_abs2_v_) + (ctrl_chk_exec_scheme.event_order_abs2_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_abs2.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_abs2_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(4, false);
        if (this.doInfoPrintOutput) this.printOutput(4, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 14 and event "event_order_casing.aAa".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge14() {
        var guard = (ctrl_chk_exec_scheme.event_order_casing_v_) <= (0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(5, true);
        if (this.doInfoEvent) this.infoEvent(5, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_casing_v_) + (ctrl_chk_exec_scheme.event_order_casing_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_casing.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_casing_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(5, false);
        if (this.doInfoPrintOutput) this.printOutput(5, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 15 and event "event_order_casing.aaA".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge15() {
        var guard = (ctrl_chk_exec_scheme.event_order_casing_v_) <= (1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(6, true);
        if (this.doInfoEvent) this.infoEvent(6, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_casing_v_) + (ctrl_chk_exec_scheme.event_order_casing_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_casing.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_casing_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(6, false);
        if (this.doInfoPrintOutput) this.printOutput(6, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 16 and event "event_order_casing.aaa".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge16() {
        var guard = (ctrl_chk_exec_scheme.event_order_casing_v_) <= (4);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(7, true);
        if (this.doInfoEvent) this.infoEvent(7, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_casing_v_) + (ctrl_chk_exec_scheme.event_order_casing_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_casing.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_casing_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(7, false);
        if (this.doInfoPrintOutput) this.printOutput(7, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 17 and event "event_order_escaping.tim".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge17() {
        var guard = (ctrl_chk_exec_scheme.event_order_escaping_v_) <= (0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(8, true);
        if (this.doInfoEvent) this.infoEvent(8, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_escaping_v_) + (ctrl_chk_exec_scheme.event_order_escaping_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_escaping.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_escaping_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(8, false);
        if (this.doInfoPrintOutput) this.printOutput(8, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 18 and event "event_order_escaping.time".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge18() {
        var guard = (ctrl_chk_exec_scheme.event_order_escaping_v_) <= (1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(9, true);
        if (this.doInfoEvent) this.infoEvent(9, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_escaping_v_) + (ctrl_chk_exec_scheme.event_order_escaping_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_escaping.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_escaping_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(9, false);
        if (this.doInfoPrintOutput) this.printOutput(9, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 19 and event "event_order_escaping.timer".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge19() {
        var guard = (ctrl_chk_exec_scheme.event_order_escaping_v_) <= (4);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(10, true);
        if (this.doInfoEvent) this.infoEvent(10, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_escaping_v_) + (ctrl_chk_exec_scheme.event_order_escaping_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_escaping.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_escaping_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(10, false);
        if (this.doInfoPrintOutput) this.printOutput(10, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 20 and event "event_order_prefix.x".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge20() {
        var guard = (ctrl_chk_exec_scheme.event_order_prefix_v_) <= (0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(11, true);
        if (this.doInfoEvent) this.infoEvent(11, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_prefix_v_) + (ctrl_chk_exec_scheme.event_order_prefix_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_prefix.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_prefix_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(11, false);
        if (this.doInfoPrintOutput) this.printOutput(11, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 21 and event "event_order_prefix.x2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge21() {
        var guard = (ctrl_chk_exec_scheme.event_order_prefix_v_) <= (1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(12, true);
        if (this.doInfoEvent) this.infoEvent(12, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_prefix_v_) + (ctrl_chk_exec_scheme.event_order_prefix_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_prefix.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_prefix_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(12, false);
        if (this.doInfoPrintOutput) this.printOutput(12, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 22 and event "event_order_prefix.x2b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge22() {
        var guard = (ctrl_chk_exec_scheme.event_order_prefix_v_) <= (4);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(13, true);
        if (this.doInfoEvent) this.infoEvent(13, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_prefix_v_) + (ctrl_chk_exec_scheme.event_order_prefix_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_prefix.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_prefix_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(13, false);
        if (this.doInfoPrintOutput) this.printOutput(13, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 23 and event "event_order_sort.a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge23() {
        var guard = (ctrl_chk_exec_scheme.event_order_sort_v_) <= (0);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(14, true);
        if (this.doInfoEvent) this.infoEvent(14, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_sort_v_) + (ctrl_chk_exec_scheme.event_order_sort_v_)) + (1);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_sort.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_sort_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(14, false);
        if (this.doInfoPrintOutput) this.printOutput(14, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 24 and event "event_order_sort.b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge24() {
        var guard = (ctrl_chk_exec_scheme.event_order_sort_v_) <= (1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(15, true);
        if (this.doInfoEvent) this.infoEvent(15, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_sort_v_) + (ctrl_chk_exec_scheme.event_order_sort_v_)) + (2);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_sort.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_sort_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(15, false);
        if (this.doInfoPrintOutput) this.printOutput(15, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 25 and event "event_order_sort.c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge25() {
        var guard = (ctrl_chk_exec_scheme.event_order_sort_v_) <= (4);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(16, true);
        if (this.doInfoEvent) this.infoEvent(16, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.event_order_sort_v_) + (ctrl_chk_exec_scheme.event_order_sort_v_)) + (4);
            if ((rhs1) > 50) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("event_order_sort.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..50]");
            }
            ctrl_chk_exec_scheme.event_order_sort_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(16, false);
        if (this.doInfoPrintOutput) this.printOutput(16, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 26 and event "unctrl_vs_ctrl.c1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge26() {
        var guard = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) < (500)) && (ctrl_chk_exec_schemeUtils.equalObjs((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) % (2), 1));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(24, true);
        if (this.doInfoEvent) this.infoEvent(24, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) + (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_)) + (4);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("unctrl_vs_ctrl.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(24, false);
        if (this.doInfoPrintOutput) this.printOutput(24, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 27 and event "unctrl_vs_ctrl.c2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge27() {
        var guard = (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) < (100);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(25, true);
        if (this.doInfoEvent) this.infoEvent(25, true);

        {
            var rhs1 = ((ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_) + (ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_)) + (8);
            if ((rhs1) > 2000) {
                ctrl_chk_exec_schemeUtils.rangeErrInt("unctrl_vs_ctrl.v", ctrl_chk_exec_schemeUtils.valueToStr(rhs1), "int[0..2,000]");
            }
            ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(25, false);
        if (this.doInfoPrintOutput) this.printOutput(25, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /** Initializes the state. */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;

        // CIF model state variables.
        ctrl_chk_exec_scheme.chan_rcv1_x_ = 0;
        ctrl_chk_exec_scheme.chan_rcv2_x_ = 0;
        ctrl_chk_exec_scheme.chan_sync1_v_ = 0;
        ctrl_chk_exec_scheme.edge_order_v_ = 0;
        ctrl_chk_exec_scheme.event_order_abs2_v_ = 0;
        ctrl_chk_exec_scheme.event_order_casing_v_ = 0;
        ctrl_chk_exec_scheme.event_order_escaping_v_ = 0;
        ctrl_chk_exec_scheme.event_order_prefix_v_ = 0;
        ctrl_chk_exec_scheme.event_order_sort_v_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt1_v_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt2a_v_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt2a_w_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt2b_w_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt3_v_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt3_x_ = 0;
        ctrl_chk_exec_scheme.one_trans_per_evt3_w_ = 0;
        ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_ = 0;
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) ctrl_chk_exec_scheme.log(ctrl_chk_exec_schemeUtils.fmt('Transition: event %s', ctrl_chk_exec_scheme.getEventName(idx)));
        } else {
            if (this.doStateOutput) ctrl_chk_exec_scheme.log('State: ' + ctrl_chk_exec_scheme.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = ctrl_chk_exec_schemeUtils.fmt('time=%s', ctrl_chk_exec_scheme.time);
        state += ctrl_chk_exec_schemeUtils.fmt(', chan_rcv1.x=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.chan_rcv1_x_));
        state += ctrl_chk_exec_schemeUtils.fmt(', chan_rcv2.x=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.chan_rcv2_x_));
        state += ctrl_chk_exec_schemeUtils.fmt(', chan_sync1.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.chan_sync1_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', edge_order.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.edge_order_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', event_order_abs2.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.event_order_abs2_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', event_order_casing.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.event_order_casing_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', event_order_escaping.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.event_order_escaping_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', event_order_prefix.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.event_order_prefix_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', event_order_sort.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.event_order_sort_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt1.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt1_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt2a.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt2a_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt2a.w=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt2a_w_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt2b.w=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt2b_w_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt3.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt3_v_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt3.w=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt3_w_));
        state += ctrl_chk_exec_schemeUtils.fmt(', one_trans_per_evt3.x=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.one_trans_per_evt3_x_));
        state += ctrl_chk_exec_schemeUtils.fmt(', unctrl_vs_ctrl.v=%s', ctrl_chk_exec_schemeUtils.valueToStr(ctrl_chk_exec_scheme.unctrl_vs_ctrl_v_));
        return state;
    }





    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link ctrl_chk_exec_schemeUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            ctrl_chk_exec_scheme.log(text);
        } else if (target == ':stderr') {
            ctrl_chk_exec_scheme.error(text);
        } else {
            var path = ctrl_chk_exec_schemeUtils.normalizePrintTarget(target);
            ctrl_chk_exec_scheme.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
