/* CIF to C translation of ctrl_chk_exec_scheme.cif
 * Generated file, DO NOT EDIT
 */

#include <stdio.h>
#include <stdlib.h>
#include "ctrl_chk_exec_scheme_engine.h"

#ifndef MAX_NUM_ITERS
#define MAX_NUM_ITERS 1000
#endif

/* What to do if a range error is found in an assignment? */
#ifdef KEEP_RUNNING
static inline void RangeErrorDetected(void) { /* Do nothing, error is already reported. */ }
#else
static inline void RangeErrorDetected(void) { exit(1); }
#endif

/* Type support code. */
int EnumTypePrint(ctrl_chk_exec_schemeEnum value, char *dest, int start, int end) {
    int last = end - 1;
    const char *lit_name = enum_names[value];
    while (start < last && *lit_name) {
        dest[start++] = *lit_name;
        lit_name++;
    }
    dest[start] = '\0';
    return start;
}


/** Event names. */
const char *ctrl_chk_exec_scheme_event_names[] = {
    "initial-step",               /**< Initial step. */
    "delay-step",                 /**< Delay step. */
    "chan",                       /**< Event "chan". */
    "edge_order.e",               /**< Event "edge_order.e". */
    "event_order_abs1.a",         /**< Event "event_order_abs1.a". */
    "event_order_abs2.a",         /**< Event "event_order_abs2.a". */
    "event_order_abs3.a",         /**< Event "event_order_abs3.a". */
    "event_order_casing.aAa",     /**< Event "event_order_casing.aAa". */
    "event_order_casing.aaA",     /**< Event "event_order_casing.aaA". */
    "event_order_casing.aaa",     /**< Event "event_order_casing.aaa". */
    "event_order_escaping.tim",   /**< Event "event_order_escaping.tim". */
    "event_order_escaping.time",  /**< Event "event_order_escaping.time". */
    "event_order_escaping.timer", /**< Event "event_order_escaping.timer". */
    "event_order_prefix.x",       /**< Event "event_order_prefix.x". */
    "event_order_prefix.x2",      /**< Event "event_order_prefix.x2". */
    "event_order_prefix.x2b",     /**< Event "event_order_prefix.x2b". */
    "event_order_sort.a",         /**< Event "event_order_sort.a". */
    "event_order_sort.b",         /**< Event "event_order_sort.b". */
    "event_order_sort.c",         /**< Event "event_order_sort.c". */
    "one_trans_per_evt1.a",       /**< Event "one_trans_per_evt1.a". */
    "one_trans_per_evt1.b",       /**< Event "one_trans_per_evt1.b". */
    "one_trans_per_evt2.e",       /**< Event "one_trans_per_evt2.e". */
    "one_trans_per_evt3.a",       /**< Event "one_trans_per_evt3.a". */
    "one_trans_per_evt3.b",       /**< Event "one_trans_per_evt3.b". */
    "one_trans_per_evt3.c",       /**< Event "one_trans_per_evt3.c". */
    "one_trans_per_evt3.d",       /**< Event "one_trans_per_evt3.d". */
    "unctrl_vs_ctrl.c1",          /**< Event "unctrl_vs_ctrl.c1". */
    "unctrl_vs_ctrl.c2",          /**< Event "unctrl_vs_ctrl.c2". */
    "unctrl_vs_ctrl.u1",          /**< Event "unctrl_vs_ctrl.u1". */
    "unctrl_vs_ctrl.u2",          /**< Event "unctrl_vs_ctrl.u2". */
};

/** Enumeration names. */
const char *enum_names[] = {
    /** Literal "__some_dummy_enum_literal". */
    "__some_dummy_enum_literal",
};

/* Constants. */


/* Functions. */


/* Input variables. */


/* State variables. */

/** Discrete variable "int[0..5] chan_rcv1.x". */
IntType chan_rcv1_x_;

/** Discrete variable "int[0..5] chan_rcv2.x". */
IntType chan_rcv2_x_;

/** Discrete variable "int[0..2,000] chan_sync1.v". */
IntType chan_sync1_v_;

/** Discrete variable "int[0..50] edge_order.v". */
IntType edge_order_v_;

/** Discrete variable "int[0..50] event_order_abs2.v". */
IntType event_order_abs2_v_;

/** Discrete variable "int[0..50] event_order_casing.v". */
IntType event_order_casing_v_;

/** Discrete variable "int[0..50] event_order_escaping.v". */
IntType event_order_escaping_v_;

/** Discrete variable "int[0..50] event_order_prefix.v". */
IntType event_order_prefix_v_;

/** Discrete variable "int[0..50] event_order_sort.v". */
IntType event_order_sort_v_;

/** Discrete variable "int[0..50] one_trans_per_evt1.v". */
IntType one_trans_per_evt1_v_;

/** Discrete variable "int[0..5] one_trans_per_evt2a.v". */
IntType one_trans_per_evt2a_v_;

/** Discrete variable "int[0..50] one_trans_per_evt2a.w". */
IntType one_trans_per_evt2a_w_;

/** Discrete variable "int[0..50] one_trans_per_evt2b.w". */
IntType one_trans_per_evt2b_w_;

/** Discrete variable "int[0..5] one_trans_per_evt3.v". */
IntType one_trans_per_evt3_v_;

/** Discrete variable "int[0..5] one_trans_per_evt3.x". */
IntType one_trans_per_evt3_x_;

/** Discrete variable "int[0..40] one_trans_per_evt3.w". */
IntType one_trans_per_evt3_w_;

/** Discrete variable "int[0..2,000] unctrl_vs_ctrl.v". */
IntType unctrl_vs_ctrl_v_;

RealType model_time; /**< Current model time. */

/** Initialize constants. */
static void InitConstants(void) {

}

/** Print function. */
#if PRINT_OUTPUT
static void PrintOutput(ctrl_chk_exec_scheme_Event_ event, BoolType pre) {
}
#endif

/* Edge execution code. */

/**
 * Execute code for edge with index 0 and event "chan".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge0(void) {
    BoolType guard = (((chan_rcv1_x_) == (0)) || ((chan_rcv2_x_) == (0))) && ((((((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (0))) || (((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (1)))) || ((((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (2))) || ((((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (0))) || (((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (1)))))) || (((((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (2))) || ((((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (0))) || (((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (1))))) || ((((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (2))) || (((chan_rcv1_x_) > (2)) || ((chan_rcv2_x_) > (2))))));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(chan_, TRUE);
    #endif

    if (((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (0))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (1))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (0)) && ((chan_rcv2_x_) == (2))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (0))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (8);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (1))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (16);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (1)) && ((chan_rcv2_x_) == (2))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (32);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (0))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (64);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (1))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (128);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) == (2)) && ((chan_rcv2_x_) == (2))) {
        IntType rhs2 = ((chan_sync1_v_) + (chan_sync1_v_)) + (256);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "chan_sync1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        chan_sync1_v_ = rhs2;
    } else if (((chan_rcv1_x_) > (2)) || ((chan_rcv2_x_) > (2))) {
        chan_sync1_v_ = 999;
    }
    if ((chan_rcv1_x_) == (0)) {
        chan_rcv1_x_ = 1;
    } else if ((chan_rcv2_x_) == (0)) {
        chan_rcv2_x_ = 1;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(chan_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 1 and event "edge_order.e".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge1(void) {
    BoolType guard = ((edge_order_v_) <= (0)) || (((edge_order_v_) <= (1)) || ((edge_order_v_) <= (4)));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(edge_order_e_, TRUE);
    #endif

    if ((edge_order_v_) <= (0)) {
        IntType rhs2 = ((edge_order_v_) + (edge_order_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "edge_order.v" "\n");
            RangeErrorDetected();
        }
        #endif
        edge_order_v_ = rhs2;
    } else if ((edge_order_v_) <= (1)) {
        IntType rhs2 = ((edge_order_v_) + (edge_order_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "edge_order.v" "\n");
            RangeErrorDetected();
        }
        #endif
        edge_order_v_ = rhs2;
    } else if ((edge_order_v_) <= (4)) {
        IntType rhs2 = ((edge_order_v_) + (edge_order_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "edge_order.v" "\n");
            RangeErrorDetected();
        }
        #endif
        edge_order_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(edge_order_e_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 2 and event "one_trans_per_evt1.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge2(void) {
    BoolType guard = ((one_trans_per_evt1_v_) <= (0)) || ((one_trans_per_evt1_v_) <= (4));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt1_a_, TRUE);
    #endif

    if ((one_trans_per_evt1_v_) <= (0)) {
        IntType rhs2 = ((one_trans_per_evt1_v_) + (one_trans_per_evt1_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt1_v_ = rhs2;
    } else if ((one_trans_per_evt1_v_) <= (4)) {
        IntType rhs2 = ((one_trans_per_evt1_v_) + (one_trans_per_evt1_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt1_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt1_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 3 and event "one_trans_per_evt1.b".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge3(void) {
    BoolType guard = (one_trans_per_evt1_v_) <= (12);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt1_b_, TRUE);
    #endif

    {
        IntType rhs2 = ((one_trans_per_evt1_v_) + (one_trans_per_evt1_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt1.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt1_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt1_b_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 4 and event "one_trans_per_evt2.e".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge4(void) {
    BoolType guard = (((one_trans_per_evt2a_v_) <= (1)) || ((one_trans_per_evt2a_v_) <= (3))) && ((((one_trans_per_evt2a_v_) % (2)) == (0)) || (((one_trans_per_evt2a_v_) % (2)) == (1)));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt2_e_, TRUE);
    #endif

    if (((one_trans_per_evt2a_v_) % (2)) == (0)) {
        IntType rhs2 = ((one_trans_per_evt2b_w_) + (one_trans_per_evt2b_w_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt2b.w" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt2b_w_ = rhs2;
    } else if (((one_trans_per_evt2a_v_) % (2)) == (1)) {
        IntType rhs2 = ((one_trans_per_evt2b_w_) + (one_trans_per_evt2b_w_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt2b.w" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt2b_w_ = rhs2;
    }
    if ((one_trans_per_evt2a_v_) <= (1)) {
        {
            IntType rhs2 = (one_trans_per_evt2a_v_) + (1);
            #if CHECK_RANGES
            if ((rhs2) > 5) {
                fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
                fprintf(stderr, "            at " "one_trans_per_evt2a.v" "\n");
                RangeErrorDetected();
            }
            #endif
            one_trans_per_evt2a_v_ = rhs2;
        }
        {
            IntType rhs2 = ((one_trans_per_evt2a_w_) + (one_trans_per_evt2a_w_)) + (1);
            #if CHECK_RANGES
            if ((rhs2) > 50) {
                fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
                fprintf(stderr, "            at " "one_trans_per_evt2a.w" "\n");
                RangeErrorDetected();
            }
            #endif
            one_trans_per_evt2a_w_ = rhs2;
        }
    } else if ((one_trans_per_evt2a_v_) <= (3)) {
        {
            IntType rhs2 = (one_trans_per_evt2a_v_) + (1);
            #if CHECK_RANGES
            if ((rhs2) > 5) {
                fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
                fprintf(stderr, "            at " "one_trans_per_evt2a.v" "\n");
                RangeErrorDetected();
            }
            #endif
            one_trans_per_evt2a_v_ = rhs2;
        }
        {
            IntType rhs2 = ((one_trans_per_evt2a_w_) + (one_trans_per_evt2a_w_)) + (2);
            #if CHECK_RANGES
            if ((rhs2) > 50) {
                fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
                fprintf(stderr, "            at " "one_trans_per_evt2a.w" "\n");
                RangeErrorDetected();
            }
            #endif
            one_trans_per_evt2a_w_ = rhs2;
        }
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt2_e_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 5 and event "one_trans_per_evt3.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge5(void) {
    BoolType guard = (one_trans_per_evt3_v_) == (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_a_, TRUE);
    #endif

    {
        IntType rhs2 = (one_trans_per_evt3_v_) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 5) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_v_ = rhs2;
    }
    {
        IntType rhs2 = ((one_trans_per_evt3_w_) + (one_trans_per_evt3_w_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 40) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..40]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.w" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_w_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 6 and event "one_trans_per_evt3.b".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge6(void) {
    BoolType guard = (one_trans_per_evt3_v_) == (2);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_b_, TRUE);
    #endif

    {
        IntType rhs2 = (one_trans_per_evt3_v_) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 5) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_v_ = rhs2;
    }
    {
        IntType rhs2 = ((one_trans_per_evt3_w_) + (one_trans_per_evt3_w_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 40) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..40]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.w" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_w_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_b_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 7 and event "one_trans_per_evt3.c".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge7(void) {
    BoolType guard = (one_trans_per_evt3_v_) == (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_c_, TRUE);
    #endif

    {
        IntType rhs2 = (one_trans_per_evt3_v_) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 5) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.v" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_v_ = rhs2;
    }
    {
        IntType rhs2 = ((one_trans_per_evt3_w_) + (one_trans_per_evt3_w_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 40) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..40]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.w" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_w_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_c_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 8 and event "one_trans_per_evt3.d".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge8(void) {
    BoolType guard = (one_trans_per_evt3_v_) < (3);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_d_, TRUE);
    #endif

    {
        IntType rhs2 = (one_trans_per_evt3_x_) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 5) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..5]\"\n", rhs2);
            fprintf(stderr, "            at " "one_trans_per_evt3.x" "\n");
            RangeErrorDetected();
        }
        #endif
        one_trans_per_evt3_x_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(one_trans_per_evt3_d_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 9 and event "unctrl_vs_ctrl.u1".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge9(void) {
    BoolType guard = ((unctrl_vs_ctrl_v_) < (500)) && (((unctrl_vs_ctrl_v_) % (2)) == (0));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_u1_, TRUE);
    #endif

    {
        IntType rhs2 = ((unctrl_vs_ctrl_v_) + (unctrl_vs_ctrl_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "unctrl_vs_ctrl.v" "\n");
            RangeErrorDetected();
        }
        #endif
        unctrl_vs_ctrl_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_u1_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 10 and event "unctrl_vs_ctrl.u2".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge10(void) {
    BoolType guard = (unctrl_vs_ctrl_v_) < (10);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_u2_, TRUE);
    #endif

    {
        IntType rhs2 = ((unctrl_vs_ctrl_v_) + (unctrl_vs_ctrl_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "unctrl_vs_ctrl.v" "\n");
            RangeErrorDetected();
        }
        #endif
        unctrl_vs_ctrl_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_u2_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 11 and event "event_order_abs1.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge11(void) {
    BoolType guard = (event_order_abs2_v_) <= (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs1_a_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_abs2_v_) + (event_order_abs2_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_abs2.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_abs2_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs1_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 12 and event "event_order_abs2.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge12(void) {
    BoolType guard = (event_order_abs2_v_) <= (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs2_a_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_abs2_v_) + (event_order_abs2_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_abs2.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_abs2_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs2_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 13 and event "event_order_abs3.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge13(void) {
    BoolType guard = (event_order_abs2_v_) <= (4);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs3_a_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_abs2_v_) + (event_order_abs2_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_abs2.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_abs2_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_abs3_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 14 and event "event_order_casing.aAa".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge14(void) {
    BoolType guard = (event_order_casing_v_) <= (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aAa_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_casing_v_) + (event_order_casing_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_casing.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_casing_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aAa_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 15 and event "event_order_casing.aaA".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge15(void) {
    BoolType guard = (event_order_casing_v_) <= (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aaA_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_casing_v_) + (event_order_casing_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_casing.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_casing_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aaA_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 16 and event "event_order_casing.aaa".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge16(void) {
    BoolType guard = (event_order_casing_v_) <= (4);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aaa_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_casing_v_) + (event_order_casing_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_casing.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_casing_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_casing_aaa_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 17 and event "event_order_escaping.tim".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge17(void) {
    BoolType guard = (event_order_escaping_v_) <= (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_tim_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_escaping_v_) + (event_order_escaping_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_escaping.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_escaping_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_tim_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 18 and event "event_order_escaping.time".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge18(void) {
    BoolType guard = (event_order_escaping_v_) <= (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_time_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_escaping_v_) + (event_order_escaping_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_escaping.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_escaping_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_time_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 19 and event "event_order_escaping.timer".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge19(void) {
    BoolType guard = (event_order_escaping_v_) <= (4);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_timer_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_escaping_v_) + (event_order_escaping_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_escaping.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_escaping_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_escaping_timer_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 20 and event "event_order_prefix.x".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge20(void) {
    BoolType guard = (event_order_prefix_v_) <= (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_prefix_v_) + (event_order_prefix_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_prefix.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_prefix_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 21 and event "event_order_prefix.x2".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge21(void) {
    BoolType guard = (event_order_prefix_v_) <= (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x2_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_prefix_v_) + (event_order_prefix_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_prefix.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_prefix_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x2_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 22 and event "event_order_prefix.x2b".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge22(void) {
    BoolType guard = (event_order_prefix_v_) <= (4);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x2b_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_prefix_v_) + (event_order_prefix_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_prefix.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_prefix_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_prefix_x2b_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 23 and event "event_order_sort.a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge23(void) {
    BoolType guard = (event_order_sort_v_) <= (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_a_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_sort_v_) + (event_order_sort_v_)) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_sort.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_sort_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 24 and event "event_order_sort.b".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge24(void) {
    BoolType guard = (event_order_sort_v_) <= (1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_b_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_sort_v_) + (event_order_sort_v_)) + (2);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_sort.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_sort_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_b_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 25 and event "event_order_sort.c".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge25(void) {
    BoolType guard = (event_order_sort_v_) <= (4);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_c_, TRUE);
    #endif

    {
        IntType rhs2 = ((event_order_sort_v_) + (event_order_sort_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 50) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..50]\"\n", rhs2);
            fprintf(stderr, "            at " "event_order_sort.v" "\n");
            RangeErrorDetected();
        }
        #endif
        event_order_sort_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(event_order_sort_c_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 26 and event "unctrl_vs_ctrl.c1".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge26(void) {
    BoolType guard = ((unctrl_vs_ctrl_v_) < (500)) && (((unctrl_vs_ctrl_v_) % (2)) == (1));
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_c1_, TRUE);
    #endif

    {
        IntType rhs2 = ((unctrl_vs_ctrl_v_) + (unctrl_vs_ctrl_v_)) + (4);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "unctrl_vs_ctrl.v" "\n");
            RangeErrorDetected();
        }
        #endif
        unctrl_vs_ctrl_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_c1_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 27 and event "unctrl_vs_ctrl.c2".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge27(void) {
    BoolType guard = (unctrl_vs_ctrl_v_) < (100);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_c2_, TRUE);
    #endif

    {
        IntType rhs2 = ((unctrl_vs_ctrl_v_) + (unctrl_vs_ctrl_v_)) + (8);
        #if CHECK_RANGES
        if ((rhs2) > 2000) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..2,000]\"\n", rhs2);
            fprintf(stderr, "            at " "unctrl_vs_ctrl.v" "\n");
            RangeErrorDetected();
        }
        #endif
        unctrl_vs_ctrl_v_ = rhs2;
    }

    #if EVENT_OUTPUT
        ctrl_chk_exec_scheme_InfoEvent(unctrl_vs_ctrl_c2_, FALSE);
    #endif
    return TRUE;
}

/**
 * Normalize and check the new value of a continuous variable after an update.
 * @param new_value Unnormalized new value of the continuous variable.
 * @param var_name Name of the continuous variable in the CIF model.
 * @return The normalized new value of the continuous variable.
 */
static inline RealType UpdateContValue(RealType new_value, const char *var_name) {
    if (isfinite(new_value)) {
        return (new_value == -0.0) ? 0.0 : new_value;
    }

    const char *err_type;
    if (isnan(new_value)) {
        err_type = "NaN";
    } else if (new_value > 0) {
        err_type = "+inf";
    } else {
        err_type = "-inf";
    }
    fprintf(stderr, "Continuous variable \"%s\" has become %s.\n", var_name, err_type);

#ifdef KEEP_RUNNING
    return 0.0;
#else
    exit(1);
#endif
}

/** Repeatedly perform discrete event steps, until no progress can be made any more. */
static void PerformEdges(void) {
    /* Uncontrollables. */
    int count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d uncontrollable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;

        edgeExecuted |= execEdge0(); /* (Try to) perform edge with index 0 and event "chan". */
        edgeExecuted |= execEdge1(); /* (Try to) perform edge with index 1 and event "edge_order.e". */
        edgeExecuted |= execEdge2(); /* (Try to) perform edge with index 2 and event "one_trans_per_evt1.a". */
        edgeExecuted |= execEdge3(); /* (Try to) perform edge with index 3 and event "one_trans_per_evt1.b". */
        edgeExecuted |= execEdge4(); /* (Try to) perform edge with index 4 and event "one_trans_per_evt2.e". */
        edgeExecuted |= execEdge5(); /* (Try to) perform edge with index 5 and event "one_trans_per_evt3.a". */
        edgeExecuted |= execEdge6(); /* (Try to) perform edge with index 6 and event "one_trans_per_evt3.b". */
        edgeExecuted |= execEdge7(); /* (Try to) perform edge with index 7 and event "one_trans_per_evt3.c". */
        edgeExecuted |= execEdge8(); /* (Try to) perform edge with index 8 and event "one_trans_per_evt3.d". */
        edgeExecuted |= execEdge9(); /* (Try to) perform edge with index 9 and event "unctrl_vs_ctrl.u1". */
        edgeExecuted |= execEdge10(); /* (Try to) perform edge with index 10 and event "unctrl_vs_ctrl.u2". */

        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }

    /* Controllables. */
    count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d controllable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;

        edgeExecuted |= execEdge11(); /* (Try to) perform edge with index 11 and event "event_order_abs1.a". */
        edgeExecuted |= execEdge12(); /* (Try to) perform edge with index 12 and event "event_order_abs2.a". */
        edgeExecuted |= execEdge13(); /* (Try to) perform edge with index 13 and event "event_order_abs3.a". */
        edgeExecuted |= execEdge14(); /* (Try to) perform edge with index 14 and event "event_order_casing.aAa". */
        edgeExecuted |= execEdge15(); /* (Try to) perform edge with index 15 and event "event_order_casing.aaA". */
        edgeExecuted |= execEdge16(); /* (Try to) perform edge with index 16 and event "event_order_casing.aaa". */
        edgeExecuted |= execEdge17(); /* (Try to) perform edge with index 17 and event "event_order_escaping.tim". */
        edgeExecuted |= execEdge18(); /* (Try to) perform edge with index 18 and event "event_order_escaping.time". */
        edgeExecuted |= execEdge19(); /* (Try to) perform edge with index 19 and event "event_order_escaping.timer". */
        edgeExecuted |= execEdge20(); /* (Try to) perform edge with index 20 and event "event_order_prefix.x". */
        edgeExecuted |= execEdge21(); /* (Try to) perform edge with index 21 and event "event_order_prefix.x2". */
        edgeExecuted |= execEdge22(); /* (Try to) perform edge with index 22 and event "event_order_prefix.x2b". */
        edgeExecuted |= execEdge23(); /* (Try to) perform edge with index 23 and event "event_order_sort.a". */
        edgeExecuted |= execEdge24(); /* (Try to) perform edge with index 24 and event "event_order_sort.b". */
        edgeExecuted |= execEdge25(); /* (Try to) perform edge with index 25 and event "event_order_sort.c". */
        edgeExecuted |= execEdge26(); /* (Try to) perform edge with index 26 and event "unctrl_vs_ctrl.c1". */
        edgeExecuted |= execEdge27(); /* (Try to) perform edge with index 27 and event "unctrl_vs_ctrl.c2". */

        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }
}

/** First model call, initializing, and performing discrete events before the first time step. */
void ctrl_chk_exec_scheme_EngineFirstStep(void) {
    InitConstants();

    model_time = 0.0;

    chan_rcv1_x_ = 0;
    chan_rcv2_x_ = 0;
    chan_sync1_v_ = 0;
    edge_order_v_ = 0;
    event_order_abs2_v_ = 0;
    event_order_casing_v_ = 0;
    event_order_escaping_v_ = 0;
    event_order_prefix_v_ = 0;
    event_order_sort_v_ = 0;
    one_trans_per_evt1_v_ = 0;
    one_trans_per_evt2a_v_ = 0;
    one_trans_per_evt2a_w_ = 0;
    one_trans_per_evt2b_w_ = 0;
    one_trans_per_evt3_v_ = 0;
    one_trans_per_evt3_x_ = 0;
    one_trans_per_evt3_w_ = 0;
    unctrl_vs_ctrl_v_ = 0;

    #if PRINT_OUTPUT
        /* pre-initial and post-initial prints. */
        PrintOutput(EVT_INITIAL_, TRUE);
        PrintOutput(EVT_INITIAL_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

/**
 * Engine takes a time step of length \a delta.
 * @param delta Length of the time step.
 */
void ctrl_chk_exec_scheme_EngineTimeStep(double delta) {


    /* Update continuous variables. */
    if (delta > 0.0) {

        model_time += delta;
    }

    #if PRINT_OUTPUT
        /* post-timestep print. */
        PrintOutput(EVT_DELAY_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

