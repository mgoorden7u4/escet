/* Additional code to allow compilation and test of the generated code.
 *
 * This file is generated, DO NOT EDIT
 */

#include <stdio.h>
#include "ctrl_chk_exec_scheme_engine.h"

/* Assign values to the input variables. */
void ctrl_chk_exec_scheme_AssignInputVariables(void) {

}

void ctrl_chk_exec_scheme_InfoEvent(ctrl_chk_exec_scheme_Event_ event, BoolType pre) {
    const char *prePostText = pre ? "pre" : "post";
    printf("Executing %s-event \"%s\"\n", prePostText, ctrl_chk_exec_scheme_event_names[event]);
}

void ctrl_chk_exec_scheme_PrintOutput(const char *text, const char *fname) {
    printf("Print @ %s: \"%s\"\n", fname, text);
}

int main(void) {
    ctrl_chk_exec_scheme_EngineFirstStep();

    ctrl_chk_exec_scheme_EngineTimeStep(1.0);
    return 0;
}

