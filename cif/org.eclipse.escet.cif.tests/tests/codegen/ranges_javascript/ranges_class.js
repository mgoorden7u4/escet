/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[0..7] a; int[0..7] b)". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += rangesUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += rangesUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** ranges code generated from a CIF specification. */
class ranges_class {
    /** rangesEnum declaration. It contains the single merged enum from the CIF model. */
    rangesEnum = Object.freeze({
        /** Literal "__some_dummy_enum_literal". */
        ___some_dummy_enum_literal: Symbol("__some_dummy_enum_literal")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "e11",
        "e12",
        "e13",
        "e14",
        "e15",
        "e16",
        "e17",
        "e18",
        "e21",
        "e22",
        "e23",
        "e24",
        "e25",
        "e26",
        "e27",
        "e28",
        "e31",
        "e32",
        "e33",
        "e34",
        "e35",
        "e36",
        "e37",
        "e38",
        "e41"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "aut1.v1". */
    aut1_v1_;

    /** Discrete variable "aut1.v2". */
    aut1_v2_;

    /** Discrete variable "aut1.w1". */
    aut1_w1_;

    /** Discrete variable "aut1.x1". */
    aut1_x1_;

    /** Discrete variable "aut1.x2". */
    aut1_x2_;

    /** Discrete variable "aut1.x3". */
    aut1_x3_;

    /** Discrete variable "aut1.x4". */
    aut1_x4_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws rangesException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) ranges.log('Initial state: ' + ranges.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute SVG input edges as long as they are possible.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "e11".
            edgeExecuted |= this.execEdge0();

            // Event "e12".
            edgeExecuted |= this.execEdge1();

            // Event "e13".
            edgeExecuted |= this.execEdge2();

            // Event "e14".
            edgeExecuted |= this.execEdge3();

            // Event "e15".
            edgeExecuted |= this.execEdge4();

            // Event "e16".
            edgeExecuted |= this.execEdge5();

            // Event "e17".
            edgeExecuted |= this.execEdge6();

            // Event "e18".
            edgeExecuted |= this.execEdge7();

            // Event "e21".
            edgeExecuted |= this.execEdge8();

            // Event "e22".
            edgeExecuted |= this.execEdge9();

            // Event "e23".
            edgeExecuted |= this.execEdge10();

            // Event "e24".
            edgeExecuted |= this.execEdge11();

            // Event "e25".
            edgeExecuted |= this.execEdge12();

            // Event "e26".
            edgeExecuted |= this.execEdge13();

            // Event "e27".
            edgeExecuted |= this.execEdge14();

            // Event "e28".
            edgeExecuted |= this.execEdge15();

            // Event "e31".
            edgeExecuted |= this.execEdge16();

            // Event "e32".
            edgeExecuted |= this.execEdge17();

            // Event "e33".
            edgeExecuted |= this.execEdge18();

            // Event "e34".
            edgeExecuted |= this.execEdge19();

            // Event "e35".
            edgeExecuted |= this.execEdge20();

            // Event "e36".
            edgeExecuted |= this.execEdge21();

            // Event "e37".
            edgeExecuted |= this.execEdge22();

            // Event "e38".
            edgeExecuted |= this.execEdge23();

            // Event "e41".
            edgeExecuted |= this.execEdge24();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws rangesException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws rangesException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (ranges.first) {
                    ranges.first = false;
                    ranges.startMilli = now;
                    ranges.targetMilli = ranges.startMilli;
                    preMilli = ranges.startMilli;
                }

                // Handle pausing/playing.
                if (!ranges.playing) {
                    ranges.timePaused = now;
                    return;
                }

                if (ranges.timePaused) {
                    ranges.startMilli += (now - ranges.timePaused);
                    ranges.targetMilli += (now - ranges.timePaused);
                    ranges.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = ranges.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - ranges.startMilli;

                // Execute once.
                ranges.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (ranges.doInfoExec) {
                    ranges.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    ranges.targetMilli += cycleMilli;
                    remainderMilli = ranges.targetMilli - postMilli;
                }

                // Execute again.
                ranges.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "e11".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge0() {

        if (this.doInfoPrintOutput) this.printOutput(0, true);
        if (this.doInfoEvent) this.infoEvent(0, true);

        ranges.aut1_v1_ = ranges.aut1_v1_;

        if (this.doInfoEvent) this.infoEvent(0, false);
        if (this.doInfoPrintOutput) this.printOutput(0, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 1 and event "e12".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge1() {

        if (this.doInfoPrintOutput) this.printOutput(1, true);
        if (this.doInfoEvent) this.infoEvent(1, true);

        ranges.aut1_v1_ = ranges.aut1_v2_;

        if (this.doInfoEvent) this.infoEvent(1, false);
        if (this.doInfoPrintOutput) this.printOutput(1, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 2 and event "e13".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge2() {

        if (this.doInfoPrintOutput) this.printOutput(2, true);
        if (this.doInfoEvent) this.infoEvent(2, true);

        {
            var rhs1 = ranges.aut1_w1_;
            for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                var rng_elem0 = rhs1[rng_index0];
                if (((rng_elem0)._field0) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][a]", rangesUtils.valueToStr((rng_elem0)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
                if (((rng_elem0)._field1) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][b]", rangesUtils.valueToStr((rng_elem0)._field1), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
            }
            ranges.aut1_v1_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(2, false);
        if (this.doInfoPrintOutput) this.printOutput(2, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 3 and event "e14".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge3() {

        if (this.doInfoPrintOutput) this.printOutput(3, true);
        if (this.doInfoEvent) this.infoEvent(3, true);

        ranges.aut1_v1_ = [new CifTuple_T2II(1, 2), new CifTuple_T2II(2, 3), new CifTuple_T2II(3, 4)];

        if (this.doInfoEvent) this.infoEvent(3, false);
        if (this.doInfoPrintOutput) this.printOutput(3, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 4 and event "e15".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge4() {

        if (this.doInfoPrintOutput) this.printOutput(4, true);
        if (this.doInfoEvent) this.infoEvent(4, true);

        {
            var rhs1 = [new CifTuple_T2II(1, 2), new CifTuple_T2II(2, 3), new CifTuple_T2II(9, 4)];
            for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                var rng_elem0 = rhs1[rng_index0];
                if (((rng_elem0)._field0) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][a]", rangesUtils.valueToStr((rng_elem0)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
            }
            ranges.aut1_v1_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(4, false);
        if (this.doInfoPrintOutput) this.printOutput(4, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 5 and event "e16".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge5() {

        if (this.doInfoPrintOutput) this.printOutput(5, true);
        if (this.doInfoEvent) this.infoEvent(5, true);

        {
            var rhs1 = [new CifTuple_T2II(1, 2), new CifTuple_T2II(2, 3), new CifTuple_T2II(3, 9)];
            for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                var rng_elem0 = rhs1[rng_index0];
                if (((rng_elem0)._field1) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][b]", rangesUtils.valueToStr((rng_elem0)._field1), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
            }
            ranges.aut1_v1_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(5, false);
        if (this.doInfoPrintOutput) this.printOutput(5, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 6 and event "e17".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge6() {

        if (this.doInfoPrintOutput) this.printOutput(6, true);
        if (this.doInfoEvent) this.infoEvent(6, true);

        {
            var rhs1 = [new CifTuple_T2II(1, 2), new CifTuple_T2II(2, 3), new CifTuple_T2II(-(1), 4)];
            for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                var rng_elem0 = rhs1[rng_index0];
                if (((rng_elem0)._field0) < 0) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][a]", rangesUtils.valueToStr((rng_elem0)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
            }
            ranges.aut1_v1_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(6, false);
        if (this.doInfoPrintOutput) this.printOutput(6, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 7 and event "e18".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge7() {

        if (this.doInfoPrintOutput) this.printOutput(7, true);
        if (this.doInfoEvent) this.infoEvent(7, true);

        {
            var rhs1 = [new CifTuple_T2II(1, 2), new CifTuple_T2II(-(3), 3), new CifTuple_T2II(9, 9)];
            for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                var rng_elem0 = rhs1[rng_index0];
                if (((rng_elem0)._field0) < 0 || ((rng_elem0)._field0) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][a]", rangesUtils.valueToStr((rng_elem0)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
                if (((rng_elem0)._field1) > 7) {
                    rangesUtils.rangeErrInt("aut1.v1" + "[" + (rng_index0).toString() + "][b]", rangesUtils.valueToStr((rng_elem0)._field1), "list[3] tuple(int[0..7] a; int[0..7] b)");
                }
            }
            ranges.aut1_v1_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(7, false);
        if (this.doInfoPrintOutput) this.printOutput(7, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 8 and event "e21".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge8() {

        if (this.doInfoPrintOutput) this.printOutput(8, true);
        if (this.doInfoEvent) this.infoEvent(8, true);

        {
            var rhs1 = rangesUtils.projectList(ranges.aut1_v1_, 2);
            var index2 = 2;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(8, false);
        if (this.doInfoPrintOutput) this.printOutput(8, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 9 and event "e22".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge9() {

        if (this.doInfoPrintOutput) this.printOutput(9, true);
        if (this.doInfoEvent) this.infoEvent(9, true);

        {
            var rhs1 = rangesUtils.projectList(ranges.aut1_v2_, 2);
            var index2 = 2;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(9, false);
        if (this.doInfoPrintOutput) this.printOutput(9, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 10 and event "e23".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge10() {

        if (this.doInfoPrintOutput) this.printOutput(10, true);
        if (this.doInfoEvent) this.infoEvent(10, true);

        {
            var rhs1 = rangesUtils.projectList(ranges.aut1_w1_, 2);
            var index2 = 2;
            if (((rhs1)._field0) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr((rhs1)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            if (((rhs1)._field1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][b]", rangesUtils.valueToStr((rhs1)._field1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(10, false);
        if (this.doInfoPrintOutput) this.printOutput(10, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 11 and event "e24".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge11() {

        if (this.doInfoPrintOutput) this.printOutput(11, true);
        if (this.doInfoEvent) this.infoEvent(11, true);

        {
            var rhs1 = new CifTuple_T2II(ranges.aut1_x1_, 4);
            var index2 = 2;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(11, false);
        if (this.doInfoPrintOutput) this.printOutput(11, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 12 and event "e25".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge12() {

        if (this.doInfoPrintOutput) this.printOutput(12, true);
        if (this.doInfoEvent) this.infoEvent(12, true);

        {
            var rhs1 = new CifTuple_T2II(ranges.aut1_x2_, 4);
            var index2 = 2;
            if (((rhs1)._field0) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr((rhs1)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(12, false);
        if (this.doInfoPrintOutput) this.printOutput(12, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 13 and event "e26".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge13() {

        if (this.doInfoPrintOutput) this.printOutput(13, true);
        if (this.doInfoEvent) this.infoEvent(13, true);

        {
            var rhs1 = new CifTuple_T2II(4, ranges.aut1_x2_);
            var index2 = 2;
            if (((rhs1)._field1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][b]", rangesUtils.valueToStr((rhs1)._field1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(13, false);
        if (this.doInfoPrintOutput) this.printOutput(13, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 14 and event "e27".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge14() {

        if (this.doInfoPrintOutput) this.printOutput(14, true);
        if (this.doInfoEvent) this.infoEvent(14, true);

        {
            var rhs1 = new CifTuple_T2II(ranges.aut1_x3_, 4);
            var index2 = 2;
            if (((rhs1)._field0) < 0) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr((rhs1)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(14, false);
        if (this.doInfoPrintOutput) this.printOutput(14, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 15 and event "e28".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge15() {

        if (this.doInfoPrintOutput) this.printOutput(15, true);
        if (this.doInfoEvent) this.infoEvent(15, true);

        {
            var rhs1 = new CifTuple_T2II(ranges.aut1_x4_, 4);
            var index2 = 2;
            if (((rhs1)._field0) < 0 || ((rhs1)._field0) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr((rhs1)._field0), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(15, false);
        if (this.doInfoPrintOutput) this.printOutput(15, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 16 and event "e31".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge16() {

        if (this.doInfoPrintOutput) this.printOutput(16, true);
        if (this.doInfoEvent) this.infoEvent(16, true);

        {
            var rhs1 = (rangesUtils.projectList(ranges.aut1_v1_, 2))._field0;
            var index2 = 2;
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(16, false);
        if (this.doInfoPrintOutput) this.printOutput(16, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 17 and event "e32".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge17() {

        if (this.doInfoPrintOutput) this.printOutput(17, true);
        if (this.doInfoEvent) this.infoEvent(17, true);

        {
            var rhs1 = (rangesUtils.projectList(ranges.aut1_v2_, 2))._field0;
            var index2 = 2;
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(17, false);
        if (this.doInfoPrintOutput) this.printOutput(17, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 18 and event "e33".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge18() {

        if (this.doInfoPrintOutput) this.printOutput(18, true);
        if (this.doInfoEvent) this.infoEvent(18, true);

        {
            var rhs1 = (rangesUtils.projectList(ranges.aut1_w1_, 2))._field0;
            var index2 = 2;
            if ((rhs1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr(rhs1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(18, false);
        if (this.doInfoPrintOutput) this.printOutput(18, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 19 and event "e34".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge19() {

        if (this.doInfoPrintOutput) this.printOutput(19, true);
        if (this.doInfoEvent) this.infoEvent(19, true);

        {
            var rhs1 = ranges.aut1_x1_;
            var index2 = 2;
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(19, false);
        if (this.doInfoPrintOutput) this.printOutput(19, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 20 and event "e35".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge20() {

        if (this.doInfoPrintOutput) this.printOutput(20, true);
        if (this.doInfoEvent) this.infoEvent(20, true);

        {
            var rhs1 = ranges.aut1_x2_;
            var index2 = 2;
            if ((rhs1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr(rhs1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(20, false);
        if (this.doInfoPrintOutput) this.printOutput(20, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 21 and event "e36".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge21() {

        if (this.doInfoPrintOutput) this.printOutput(21, true);
        if (this.doInfoEvent) this.infoEvent(21, true);

        {
            var rhs1 = ranges.aut1_x2_;
            var index2 = 2;
            if ((rhs1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][b]", rangesUtils.valueToStr(rhs1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field1 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(21, false);
        if (this.doInfoPrintOutput) this.printOutput(21, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 22 and event "e37".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge22() {

        if (this.doInfoPrintOutput) this.printOutput(22, true);
        if (this.doInfoEvent) this.infoEvent(22, true);

        {
            var rhs1 = ranges.aut1_x3_;
            var index2 = 2;
            if ((rhs1) < 0) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr(rhs1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(22, false);
        if (this.doInfoPrintOutput) this.printOutput(22, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 23 and event "e38".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge23() {

        if (this.doInfoPrintOutput) this.printOutput(23, true);
        if (this.doInfoEvent) this.infoEvent(23, true);

        {
            var rhs1 = ranges.aut1_x4_;
            var index2 = 2;
            if ((rhs1) < 0 || (rhs1) > 7) {
                rangesUtils.rangeErrInt("aut1.v1" + "[" + (index2).toString() + "][a]", rangesUtils.valueToStr(rhs1), "list[3] tuple(int[0..7] a; int[0..7] b)");
            }
            var part3 = rangesUtils.projectList(ranges.aut1_v1_, index2);
            part3 = part3.copy();
            part3._field0 = rhs1;
            ranges.aut1_v1_ = rangesUtils.modify(ranges.aut1_v1_, index2, part3);
        }

        if (this.doInfoEvent) this.infoEvent(23, false);
        if (this.doInfoPrintOutput) this.printOutput(23, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 24 and event "e41".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge24() {

        if (this.doInfoPrintOutput) this.printOutput(24, true);
        if (this.doInfoEvent) this.infoEvent(24, true);

        {
            var rhs1 = ranges.aut1_x4_;
            if ((rhs1) > 4) {
                rangesUtils.rangeErrInt("aut1.x3", rangesUtils.valueToStr(rhs1), "int[-1..4]");
            }
            ranges.aut1_x3_ = rhs1;
        }

        if (this.doInfoEvent) this.infoEvent(24, false);
        if (this.doInfoPrintOutput) this.printOutput(24, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /** Initializes the state. */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;

        // CIF model state variables.
        ranges.aut1_v1_ = [new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0)];
        ranges.aut1_v2_ = [new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0)];
        ranges.aut1_w1_ = [new CifTuple_T2II(1, 1), new CifTuple_T2II(1, 1), new CifTuple_T2II(1, 1)];
        ranges.aut1_x1_ = 0;
        ranges.aut1_x2_ = 0;
        ranges.aut1_x3_ = 0;
        ranges.aut1_x4_ = 0;
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) ranges.log(rangesUtils.fmt('Transition: event %s', ranges.getEventName(idx)));
        } else {
            if (this.doStateOutput) ranges.log('State: ' + ranges.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = rangesUtils.fmt('time=%s', ranges.time);
        state += rangesUtils.fmt(', aut1.v1=%s', rangesUtils.valueToStr(ranges.aut1_v1_));
        state += rangesUtils.fmt(', aut1.v2=%s', rangesUtils.valueToStr(ranges.aut1_v2_));
        state += rangesUtils.fmt(', aut1.w1=%s', rangesUtils.valueToStr(ranges.aut1_w1_));
        state += rangesUtils.fmt(', aut1.x1=%s', rangesUtils.valueToStr(ranges.aut1_x1_));
        state += rangesUtils.fmt(', aut1.x2=%s', rangesUtils.valueToStr(ranges.aut1_x2_));
        state += rangesUtils.fmt(', aut1.x3=%s', rangesUtils.valueToStr(ranges.aut1_x3_));
        state += rangesUtils.fmt(', aut1.x4=%s', rangesUtils.valueToStr(ranges.aut1_x4_));
        return state;
    }





    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link rangesUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            ranges.log(text);
        } else if (target == ':stderr') {
            ranges.error(text);
        } else {
            var path = rangesUtils.normalizePrintTarget(target);
            ranges.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
