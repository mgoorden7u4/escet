package ctrl_chk_exec_scheme_java;

import static ctrl_chk_exec_scheme_java.ctrl_chk_exec_scheme.ctrl_chk_exec_schemeUtils.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/** Test class for the {@link ctrl_chk_exec_scheme} class. */
@SuppressWarnings("unused")
public class ctrl_chk_exec_schemeTest extends ctrl_chk_exec_scheme {
    /**
     * Main method for the {@link ctrl_chk_exec_schemeTest} class.
     *
     * @param args The command line arguments. Ignored.
     */
    public static void main(String[] args) {
        ctrl_chk_exec_scheme code = new ctrl_chk_exec_schemeTest();
        code.exec(100);
    }

    @Override
    protected void updateInputs() {
        // No input variables.
    }

    @Override
    protected void infoExec(long duration, long cycleTime) {
        if (duration > cycleTime) {
            String text = fmt("Cycle time exceeded: %,d ns > %,d ns.", duration, cycleTime);
            System.err.println(text);
        }
    }

    @Override
    protected void infoEvent(int idx, boolean pre) {
        // Print the executed event, for testing.
        if (pre) {
            System.out.println("EVENT pre: " + getEventName(idx));
        } else {
            System.out.println("EVENT post: " + getEventName(idx));
        }
    }

    @Override
    protected void infoPrintOutput(String text, String target) {
        if (target.equals(":stdout")) {
            System.out.println(text);
            System.out.flush();

        } else if (target.equals(":stderr")) {
            System.err.println(text);
            System.err.flush();

        } else {
            System.out.println(normalizePrintTarget(target) + ": " + text);
            System.out.flush();
        }
    }

    @Override
    protected void preExec() {
        // Ignore.
    }

    @Override
    protected void postExec() {
        // Could for instance write outputs to the environment.
    }
}
