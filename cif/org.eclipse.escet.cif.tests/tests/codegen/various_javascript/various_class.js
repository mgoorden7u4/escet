/** Tuples. */


/** various code generated from a CIF specification. */
class various_class {
    /** variousEnum declaration. It contains the single merged enum from the CIF model. */
    variousEnum = Object.freeze({
        /** Literal "l1". */
        _l1: Symbol("l1"),

        /** Literal "l2". */
        _l2: Symbol("l2")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "a.e",
        "e1",
        "g.h1"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "a.li". */
    a_li_;

    /** Discrete variable "a.x". */
    a_x_;

    /** Discrete variable "g.rcv.v". */
    g_rcv_v_;

    /** Discrete variable "g.rcv.v2". */
    g_rcv_v2_;

    /** Discrete variable "g.snd.a". */
    g_snd_a_;

    /** Continuous variable "g.sync.c". */
    g_sync_c_;

    /** Discrete variable "g.sync". */
    g_sync_;

    /** Input variable "x". */
    x_;

    /** Input variable "y". */
    y_;

    /** Input variable "input_li". */
    input_li_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws variousException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        var deriv0 = various.g_sync_c_deriv();

                        various.g_sync_c_ = various.g_sync_c_ + delta * deriv0;
                        variousUtils.checkReal(various.g_sync_c_, "g.sync.c");
                        if (various.g_sync_c_ == -0.0) various.g_sync_c_ = 0.0;
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) various.log('Initial state: ' + various.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute SVG input edges as long as they are possible.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "a.e".
            edgeExecuted |= this.execEdge0();

            // Event "e1".
            edgeExecuted |= this.execEdge1();

            // Event "g.h1".
            edgeExecuted |= this.execEdge2();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws variousException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws variousException In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (various.first) {
                    various.first = false;
                    various.startMilli = now;
                    various.targetMilli = various.startMilli;
                    preMilli = various.startMilli;
                }

                // Handle pausing/playing.
                if (!various.playing) {
                    various.timePaused = now;
                    return;
                }

                if (various.timePaused) {
                    various.startMilli += (now - various.timePaused);
                    various.targetMilli += (now - various.timePaused);
                    various.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = various.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - various.startMilli;

                // Execute once.
                various.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (various.doInfoExec) {
                    various.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    various.targetMilli += cycleMilli;
                    remainderMilli = various.targetMilli - postMilli;
                }

                // Execute again.
                various.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "a.e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge0() {
        var guard = false;

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(0, true);
        if (this.doInfoEvent) this.infoEvent(0, true);

        {
            var rhs1 = various.a_x_;
            var index2 = 0;
            if ((rhs1) > 3) {
                variousUtils.rangeErrInt("a.li" + "[" + (index2).toString() + "]", variousUtils.valueToStr(rhs1), "list[2] int[0..3]");
            }
            various.a_li_ = variousUtils.modify(various.a_li_, index2, rhs1);
        }

        if (this.doInfoEvent) this.infoEvent(0, false);
        if (this.doInfoPrintOutput) this.printOutput(0, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 1 and event "e1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge1() {
        var guard = (various.g_sync_) == (various.variousEnum._l1);

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(1, true);
        if (this.doInfoEvent) this.infoEvent(1, true);

        various.g_sync_ = various.variousEnum._l2;

        if (this.doInfoEvent) this.infoEvent(1, false);
        if (this.doInfoPrintOutput) this.printOutput(1, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /**
     * Execute code for edge with index 2 and event "g.h1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     */
    execEdge2() {
        var guard = ((various.g_sync_) == (various.variousEnum._l2)) && ((various.g_sync_c_) >= (2));

        if (!guard) {
            return false;
        }

        if (this.doInfoPrintOutput) this.printOutput(2, true);
        if (this.doInfoEvent) this.infoEvent(2, true);

        various.g_rcv_v_ = variousUtils.multiplyInt(various.g_rcv_v2_, variousUtils.addInt(various.z_(), various.g_snd_a_));
        various.g_snd_a_ = variousUtils.addInt(various.g_snd_a_, 1);
        various.g_sync_c_ = 0.0;
        various.g_sync_ = various.variousEnum._l1;

        if (this.doInfoEvent) this.infoEvent(2, false);
        if (this.doInfoPrintOutput) this.printOutput(2, false);
        if (this.doStateOutput || this.doTransitionOutput) this.log('');
        return true;
    }

    /** Initializes the state. */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;

        // CIF model state variables.
        various.a_li_ = [0, 0];
        various.a_x_ = 2;
        various.g_rcv_v_ = 0;
        various.g_rcv_v2_ = various.g_rcv_v_;
        various.g_snd_a_ = variousUtils.addInt((5) + (variousUtils.sizeList(various.input_li_)), various.inc_(various.g_rcv_v_));
        various.g_sync_c_ = 0.0;
        various.g_sync_ = various.variousEnum._l1;
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) various.log(variousUtils.fmt('Transition: event %s', various.getEventName(idx)));
        } else {
            if (this.doStateOutput) various.log('State: ' + various.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = variousUtils.fmt('time=%s', various.time);
        state += variousUtils.fmt(', a.li=%s', variousUtils.valueToStr(various.a_li_));
        state += variousUtils.fmt(', a.x=%s', variousUtils.valueToStr(various.a_x_));
        state += variousUtils.fmt(', g.rcv.v=%s', variousUtils.valueToStr(various.g_rcv_v_));
        state += variousUtils.fmt(', g.rcv.v2=%s', variousUtils.valueToStr(various.g_rcv_v2_));
        state += variousUtils.fmt(', g.snd.a=%s', variousUtils.valueToStr(various.g_snd_a_));
        state += variousUtils.fmt(', g.sync=%s', variousUtils.valueToStr(various.g_sync_));
        state += variousUtils.fmt(', g.sync.c=%s', variousUtils.valueToStr(various.g_sync_c_));
        state += variousUtils.fmt(', g.sync.c\'=%s', variousUtils.valueToStr(various.g_sync_c_deriv()));
        return state;
    }


    /**
     * Evaluates algebraic variable "z".
     *
     * @return The evaluation result.
     */
    z_() {
        return variousUtils.addInt(various.x_, various.y_);
    }

    /**
     * Evaluates derivative of continuous variable "g.sync.c".
     *
     * @return The evaluation result.
     */
    g_sync_c_deriv() {
        return 1.0;
    }

    /**
     * Function "inc".
     *
     * @param inc_x_ Function parameter "inc.x".
     * @return The return value of the function.
     */
    inc_(inc_x_) {
        // Execute statements in the function body.
        return variousUtils.addInt(inc_x_, 1);
        throw new Error('No return statement at end of function.');
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     */
    printOutput(idx, pre) {
        if (true) {
            if (pre) {
                var value = various.time;
                var text = variousUtils.valueToStr(value);
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var value = various.g_sync_c_;
                var text = variousUtils.valueToStr(value);
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == 2) {
            if (!pre) {
                var value = various.time;
                var text = variousUtils.valueToStr(value);
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == 2) {
            if (!pre) {
                var value = various.g_rcv_v_;
                var text = variousUtils.valueToStr(value);
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text = "out";
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text = "err";
                this.infoPrintOutput(text, ":stderr");
            }
        }
        if (true) {
            if (!pre) {
                var text = "file";
                this.infoPrintOutput(text, ".\\some_file.txt");
            }
        }
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link variousUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            various.log(text);
        } else if (target == ':stderr') {
            various.error(text);
        } else {
            var path = variousUtils.normalizePrintTarget(target);
            various.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
