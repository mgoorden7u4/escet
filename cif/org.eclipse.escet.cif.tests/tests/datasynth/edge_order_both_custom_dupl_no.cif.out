Reading CIF file "datasynth/edge_order_both_custom_dupl_no.cif".

Preprocessing CIF specification (includes checking that the specification is supported).

Converting CIF specification to internal format (BDDs):
    CIF variables and location pointers:
        Nr     Kind              Type  Name      Group  BDD vars  CIF values  BDD values  Values used
        -----  ----------------  ----  --------  -----  --------  ----------  ----------  -----------
        1      location pointer  n/a   Counter   0      3 * 2     6 * 2       8 * 2       75%
        2      location pointer  n/a   Actuator  1      1 * 2     2 * 2       2 * 2       100%
        -----  ----------------  ----  --------  -----  --------  ----------  ----------  -----------
        Total                                    2      8         16          20          80%

    Applying variable ordering:
        Applying 4 orderers, sequentially:
            Applying model variable order:
                Effect: both

            Applying DCSH algorithm:
                Metric: wes
                Relations: legacy
                Effect: var-order
                Number of hyper-edges: 6

                Applying Weighted Cuthill-McKee algorithm:
                    Node finder: george-liu
                    Relations: legacy
                    Effect: var-order
                    Number of graph edges: 0

                    Skipping algorithm: no graph edges.

                Found new best variable order.

                Applying Sloan algorithm:
                    Relations: legacy
                    Effect: var-order
                    Number of graph edges: 0

                    Skipping algorithm: no graph edges.

                Applying 2 orderers, sequentially:
                    Applying Weighted Cuthill-McKee algorithm:
                        Node finder: george-liu
                        Relations: legacy
                        Effect: var-order
                        Number of graph edges: 0

                        Skipping algorithm: no graph edges.

                    Reversing the variable order:
                        Relations: legacy
                        Effect: var-order
                        Number of hyper-edges: 6

                        Total span:   0 (total)   0.00 (avg/edge) / WES:   0.166667 (total)   0.027778 (avg/edge) [before]
                        Total span:   0 (total)   0.00 (avg/edge) / WES:   0.166667 (total)   0.027778 (avg/edge) [reversed]

                Applying 2 orderers, sequentially:
                    Applying Sloan algorithm:
                        Relations: legacy
                        Effect: var-order
                        Number of graph edges: 0

                        Skipping algorithm: no graph edges.

                    Reversing the variable order:
                        Relations: legacy
                        Effect: var-order
                        Number of hyper-edges: 6

                        Total span:   0 (total)   0.00 (avg/edge) / WES:   0.166667 (total)   0.027778 (avg/edge) [before]
                        Total span:   0 (total)   0.00 (avg/edge) / WES:   0.166667 (total)   0.027778 (avg/edge) [reversed]

            Applying FORCE algorithm:
                Metric: total-span
                Relations: linearized
                Effect: var-order
                Number of hyper-edges: 12
                Maximum number of iterations: 10

                Total span:   2 (total)   0.17 (avg/edge) / WES:   0.166667 (total)   0.013889 (avg/edge) [before]
                Total span:   2 (total)   0.17 (avg/edge) / WES:   0.166667 (total)   0.013889 (avg/edge) [iteration 1]
                Total span:   2 (total)   0.17 (avg/edge) / WES:   0.166667 (total)   0.013889 (avg/edge) [after]

            Applying sliding window algorithm:
                Size: 4
                Metric: total-span
                Relations: linearized
                Effect: var-order
                Number of hyper-edges: 12
                Window length: 2

                Total span:   2 (total)   0.17 (avg/edge) / WES:   0.166667 (total)   0.013889 (avg/edge) [before]
                Total span:   2 (total)   0.17 (avg/edge) / WES:   0.166667 (total)   0.013889 (avg/edge) [after]

    Variable order unchanged.

Starting data-based synthesis.

Synthesis input:
    Invariant (components state plant inv):      true
    Invariant (locations state plant invariant): true
    Invariant (system state plant invariant):    true

    Invariant (components state req invariant):  true
    Invariant (locations state req invariant):   true
    Invariant (system state req invariant):      true

    Initial   (discrete variables):              true
    Initial   (components init predicate):       true
    Initial   (aut/locs init predicate):         Counter.zero
    Initial   (aut/locs init predicate):         Actuator.Off
    Initial   (auts/locs init predicate):        Counter.zero and Actuator.Off
    Initial   (uncontrolled system):             Counter.zero and Actuator.Off
    Initial   (system, combined init/plant inv): Counter.zero and Actuator.Off
    Initial   (system, combined init/state inv): Counter.zero and Actuator.Off

    Marked    (components marker predicate):     true
    Marked    (aut/locs marker predicate):       Counter.zero
    Marked    (aut/locs marker predicate):       Actuator.Off
    Marked    (auts/locs marker predicate):      Counter.zero and Actuator.Off
    Marked    (uncontrolled system):             Counter.zero and Actuator.Off
    Marked    (system, combined mark/plant inv): Counter.zero and Actuator.Off
    Marked    (system, combined mark/state inv): Counter.zero and Actuator.Off

    State/event exclusion plants:
        None

    State/event exclusion requirements:
        Event "Actuator.on" needs:
            Counter.zero
        Event "Actuator.off" needs:
            Counter.five

    Uncontrolled system:
        State: (controlled-behavior: ?)
            Edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)
            Edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)
            Edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)
            Edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)
            Edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)
            Edge: (event: Counter.dec) (guard: Counter.one) (assignments: Counter := Counter.zero)
            Edge: (event: Counter.dec) (guard: Counter.two) (assignments: Counter := Counter.one)
            Edge: (event: Counter.dec) (guard: Counter.three) (assignments: Counter := Counter.two)
            Edge: (event: Counter.dec) (guard: Counter.four) (assignments: Counter := Counter.three)
            Edge: (event: Counter.dec) (guard: Counter.five) (assignments: Counter := Counter.four)
            Edge: (event: Actuator.on) (guard: Actuator.Off) (assignments: Actuator := Actuator.On)
            Edge: (event: Actuator.off) (guard: Actuator.On) (assignments: Actuator := Actuator.Off)

Checking input for potential problems.

Restricting edge guards to prevent runtime errors:
    No guards changed.

Restricting uncontrolled system behavior using state/event exclusion plant invariants:
    No guards changed.

Initializing edges for being applied.

Restricting uncontrolled system behavior using state plant invariants:
    No restrictions needed.

Initializing controlled behavior:
    Controlled-behavior predicate: true.
    Controlled-initialization predicate: Counter.zero and Actuator.Off.

Restricting behavior using state requirements:
    Controlled behavior not changed.

Extending controlled-behavior predicate using variable ranges:
    Controlled behavior: true -> true [range: true, variable: location pointer for automaton "Counter" (group: 0, domain: 0+1, BDD variables: 3, CIF/BDD values: 6/8)].

    Extended controlled-behavior predicate using variable ranges: true.

Restricting behavior using state/event exclusion requirements:
    Edge (event: Actuator.on) (guard: Actuator.Off) (assignments: Actuator := Actuator.On): guard: Actuator.Off -> Counter.zero and Actuator.Off [state/event exclusion requirement: Counter.zero].
    Edge (event: Actuator.off) (guard: Actuator.On) (assignments: Actuator := Actuator.Off): guard: Actuator.On -> Counter.five and Actuator.On [state/event exclusion requirement: Counter.five].

    Restricted behavior using state/event exclusion requirements:
        State: (controlled-behavior: true)
            Edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)
            Edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)
            Edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)
            Edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)
            Edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)
            Edge: (event: Counter.dec) (guard: Counter.one) (assignments: Counter := Counter.zero)
            Edge: (event: Counter.dec) (guard: Counter.two) (assignments: Counter := Counter.one)
            Edge: (event: Counter.dec) (guard: Counter.three) (assignments: Counter := Counter.two)
            Edge: (event: Counter.dec) (guard: Counter.four) (assignments: Counter := Counter.three)
            Edge: (event: Counter.dec) (guard: Counter.five) (assignments: Counter := Counter.four)
            Edge: (event: Actuator.on) (guard: Actuator.Off -> Counter.zero and Actuator.Off) (assignments: Actuator := Actuator.On)
            Edge: (event: Actuator.off) (guard: Actuator.On -> Counter.five and Actuator.On) (assignments: Actuator := Actuator.Off)

Restricting behavior using implicit runtime error requirements:
    Controlled behavior not changed.

Re-initializing edges for being applied.

Checking pre-synthesis for events that are never enabled.

Synthesis round 1:
    Computing backward controlled-behavior predicate:
        Saturation matrix:
                            /-------- 1 Counter#0
                            |/------- 2 Counter#0+
                            ||/------ 3 Counter#1
                            |||/----- 4 Counter#1+
                            ||||/---- 5 Counter#2
                            |||||/--- 6 Counter#2+
                            ||||||/-- 7 Actuator#0
                            |||||||/- 8 Actuator#0+
            1  Counter.dec  rwrwrw
            2  Counter.dec  rwrwrw
            3  Counter.dec  rwrwrw
            4  Counter.dec  rwrwrw
            5  Counter.dec  rwrwrw
            6  Actuator.off r r r rw
            7  Counter.inc  rwrwrw
            8  Counter.inc  rwrwrw
            9  Counter.inc  rwrwrw
            10 Counter.inc  rwrwrw
            11 Counter.inc  rwrwrw
            12 Actuator.on  r r r rw

        Backward controlled-behavior: Counter.zero and Actuator.Off [marker predicate]
        Backward controlled-behavior: Counter.zero and Actuator.Off -> (Counter.zero or Counter.one) and Actuator.Off [backward reach using bounded saturation (transition: 1 of 12) (level: 1 of 8) with edge: (event: Counter.dec) (guard: Counter.one) (assignments: Counter := Counter.zero)]
        Backward controlled-behavior: (Counter.zero or Counter.one) and Actuator.Off -> (Counter.zero or Counter.two) and Actuator.Off or Counter.one and Actuator.Off [backward reach using bounded saturation (transition: 2 of 12) (level: 1 of 8) with edge: (event: Counter.dec) (guard: Counter.two) (assignments: Counter := Counter.one)]
        Backward controlled-behavior: (Counter.zero or Counter.two) and Actuator.Off or Counter.one and Actuator.Off -> not Counter.four and (not Counter.five and Actuator.Off) [backward reach using bounded saturation (transition: 3 of 12) (level: 1 of 8) with edge: (event: Counter.dec) (guard: Counter.three) (assignments: Counter := Counter.two)]
        Backward controlled-behavior: not Counter.four and (not Counter.five and Actuator.Off) -> (Counter.zero or Counter.four) and Actuator.Off or (Counter.two and Actuator.Off or (Counter.one or Counter.three) and Actuator.Off) [backward reach using bounded saturation (transition: 4 of 12) (level: 1 of 8) with edge: (event: Counter.dec) (guard: Counter.four) (assignments: Counter := Counter.three)]
        Backward controlled-behavior: (Counter.zero or Counter.four) and Actuator.Off or (Counter.two and Actuator.Off or (Counter.one or Counter.three) and Actuator.Off) -> (Counter.two or (Counter.three or Actuator.Off)) and (not Counter.two and not Counter.three or Actuator.Off) [backward reach using bounded saturation (transition: 5 of 12) (level: 1 of 8) with edge: (event: Counter.dec) (guard: Counter.five) (assignments: Counter := Counter.four)]
        Backward controlled-behavior: (Counter.two or (Counter.three or Actuator.Off)) and (not Counter.two and not Counter.three or Actuator.Off) -> (Counter.zero or Counter.four) and Actuator.Off or Counter.two and Actuator.Off or (Counter.one and Actuator.Off or (Counter.five or Counter.three and Actuator.Off)) [backward reach using bounded saturation (transition: 6 of 12) (level: 1 of 8) with edge: (event: Actuator.off) (guard: Actuator.On -> Counter.five and Actuator.On) (assignments: Actuator := Actuator.Off)]
        Backward controlled-behavior: (Counter.zero or Counter.four) and Actuator.Off or Counter.two and Actuator.Off or (Counter.one and Actuator.Off or (Counter.five or Counter.three and Actuator.Off)) -> (not Counter.zero and not Counter.one or Actuator.Off) and (not Counter.two and not Counter.three or Actuator.Off) [backward reach using bounded saturation (transition: 11 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)]
        Backward controlled-behavior: (not Counter.zero and not Counter.one or Actuator.Off) and (not Counter.two and not Counter.three or Actuator.Off) -> (not Counter.zero or Actuator.Off) and ((not Counter.two or Actuator.Off) and (not Counter.one or Actuator.Off)) [backward reach using bounded saturation (transition: 10 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)]
        Backward controlled-behavior: (not Counter.zero or Actuator.Off) and ((not Counter.two or Actuator.Off) and (not Counter.one or Actuator.Off)) -> not Counter.zero and not Counter.one or Actuator.Off [backward reach using bounded saturation (transition: 9 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)]
        Backward controlled-behavior: not Counter.zero and not Counter.one or Actuator.Off -> not Counter.zero or Actuator.Off [backward reach using bounded saturation (transition: 8 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)]
        Backward controlled-behavior: not Counter.zero or Actuator.Off -> true [backward reach using bounded saturation (transition: 7 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)]

        Backward controlled-behavior: true [fixed point].

        Controlled behavior not changed.

    Computing backward uncontrolled bad-state predicate:
        Saturation matrix:
            No edges.

        Backward uncontrolled bad-state: false [current/previous controlled behavior predicate]

        Controlled behavior not changed.

    Computing forward controlled-behavior predicate:
        Saturation matrix:
                            /-------- 1 Counter#0
                            |/------- 2 Counter#0+
                            ||/------ 3 Counter#1
                            |||/----- 4 Counter#1+
                            ||||/---- 5 Counter#2
                            |||||/--- 6 Counter#2+
                            ||||||/-- 7 Actuator#0
                            |||||||/- 8 Actuator#0+
            1  Counter.dec  rwrwrw
            2  Counter.dec  rwrwrw
            3  Counter.dec  rwrwrw
            4  Counter.dec  rwrwrw
            5  Counter.dec  rwrwrw
            6  Actuator.off r r r rw
            7  Counter.inc  rwrwrw
            8  Counter.inc  rwrwrw
            9  Counter.inc  rwrwrw
            10 Counter.inc  rwrwrw
            11 Counter.inc  rwrwrw
            12 Actuator.on  r r r rw

        Forward controlled-behavior: Counter.zero and Actuator.Off [initialization predicate]
        Forward controlled-behavior: Counter.zero and Actuator.Off -> (Counter.zero or Counter.one) and Actuator.Off [forward reach using bounded saturation (transition: 7 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)]
        Forward controlled-behavior: (Counter.zero or Counter.one) and Actuator.Off -> (Counter.zero or Counter.two) and Actuator.Off or Counter.one and Actuator.Off [forward reach using bounded saturation (transition: 8 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)]
        Forward controlled-behavior: (Counter.zero or Counter.two) and Actuator.Off or Counter.one and Actuator.Off -> not Counter.four and (not Counter.five and Actuator.Off) [forward reach using bounded saturation (transition: 9 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)]
        Forward controlled-behavior: not Counter.four and (not Counter.five and Actuator.Off) -> (Counter.zero or Counter.four) and Actuator.Off or (Counter.two and Actuator.Off or (Counter.one or Counter.three) and Actuator.Off) [forward reach using bounded saturation (transition: 10 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)]
        Forward controlled-behavior: (Counter.zero or Counter.four) and Actuator.Off or (Counter.two and Actuator.Off or (Counter.one or Counter.three) and Actuator.Off) -> (Counter.two or (Counter.three or Actuator.Off)) and (not Counter.two and not Counter.three or Actuator.Off) [forward reach using bounded saturation (transition: 11 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)]
        Forward controlled-behavior: (Counter.two or (Counter.three or Actuator.Off)) and (not Counter.two and not Counter.three or Actuator.Off) -> Counter.zero or Counter.four and Actuator.Off or (Counter.two and Actuator.Off or ((Counter.one or Counter.five) and Actuator.Off or Counter.three and Actuator.Off)) [forward reach using bounded saturation (transition: 12 of 12) (level: 1 of 8) with edge: (event: Actuator.on) (guard: Actuator.Off -> Counter.zero and Actuator.Off) (assignments: Actuator := Actuator.On)]
        Forward controlled-behavior: Counter.zero or Counter.four and Actuator.Off or (Counter.two and Actuator.Off or ((Counter.one or Counter.five) and Actuator.Off or Counter.three and Actuator.Off)) -> (not Counter.four and not Counter.five or Actuator.Off) and (not Counter.two and not Counter.three or Actuator.Off) [forward reach using bounded saturation (transition: 7 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)]
        Forward controlled-behavior: (not Counter.four and not Counter.five or Actuator.Off) and (not Counter.two and not Counter.three or Actuator.Off) -> (not Counter.four or Actuator.Off) and ((not Counter.five or Actuator.Off) and (not Counter.three or Actuator.Off)) [forward reach using bounded saturation (transition: 8 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)]
        Forward controlled-behavior: (not Counter.four or Actuator.Off) and ((not Counter.five or Actuator.Off) and (not Counter.three or Actuator.Off)) -> not Counter.four and not Counter.five or Actuator.Off [forward reach using bounded saturation (transition: 9 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)]
        Forward controlled-behavior: not Counter.four and not Counter.five or Actuator.Off -> not Counter.five or Actuator.Off [forward reach using bounded saturation (transition: 10 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)]
        Forward controlled-behavior: not Counter.five or Actuator.Off -> true [forward reach using bounded saturation (transition: 11 of 12) (level: 1 of 8) with edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)]

        Forward controlled-behavior: true [fixed point].

        Controlled behavior not changed.

    Finished: controlled behavior is stable.

Computing final controlled system guards:
    No guards changed.

Cleaning up cached predicate of edges that were used when applying edges.

Final synthesis result:
    State: (controlled-behavior: true)
        Edge: (event: Counter.inc) (guard: Counter.zero) (assignments: Counter := Counter.one)
        Edge: (event: Counter.inc) (guard: Counter.one) (assignments: Counter := Counter.two)
        Edge: (event: Counter.inc) (guard: Counter.two) (assignments: Counter := Counter.three)
        Edge: (event: Counter.inc) (guard: Counter.three) (assignments: Counter := Counter.four)
        Edge: (event: Counter.inc) (guard: Counter.four) (assignments: Counter := Counter.five)
        Edge: (event: Counter.dec) (guard: Counter.one) (assignments: Counter := Counter.zero)
        Edge: (event: Counter.dec) (guard: Counter.two) (assignments: Counter := Counter.one)
        Edge: (event: Counter.dec) (guard: Counter.three) (assignments: Counter := Counter.two)
        Edge: (event: Counter.dec) (guard: Counter.four) (assignments: Counter := Counter.three)
        Edge: (event: Counter.dec) (guard: Counter.five) (assignments: Counter := Counter.four)
        Edge: (event: Actuator.on) (guard: Actuator.Off -> Counter.zero and Actuator.Off) (assignments: Actuator := Actuator.On)
        Edge: (event: Actuator.off) (guard: Actuator.On -> Counter.five and Actuator.On) (assignments: Actuator := Actuator.Off)

Computing initialization predicate of the controlled system.

Controlled system: exactly 12 states.

Determining initialization predicate for output model:
    Initial (synthesis result):            true
    Initial (uncontrolled system):         Counter.zero and Actuator.Off
    Initial (controlled system):           Counter.zero and Actuator.Off
    Initial (removed by supervisor):       false
    Initial (added by supervisor):         true

    Initial (output model):                n/a

Determining supervisor guards for output model:
    Event Counter.inc: guard: not Counter.five.
    Event Counter.dec: guard: not Counter.zero.
    Event Actuator.on: guard: Counter.zero and Actuator.Off.
    Event Actuator.off: guard: Counter.five and Actuator.On.

Checking post-synthesis for events that are never enabled.

Simplifying supervisor guards for output model:
    Simplification under the assumption of the plants.

    Event Counter.inc: guard: not Counter.five -> true [assume not Counter.five].
    Event Counter.dec: guard: not Counter.zero -> true [assume not Counter.zero].
    Event Actuator.on: guard: Counter.zero and Actuator.Off -> Counter.zero [assume Actuator.Off].
    Event Actuator.off: guard: Counter.five and Actuator.On -> Counter.five [assume Actuator.On].

Constructing output CIF specification.

BDD cache statistics:
    Node creation requests:       691
    Node creation chain accesses: 0
    Node creation cache hits:     462
    Node creation cache misses:   146
    Operation count:              2565
    Operation cache hits:         280
    Operation cache misses:       543

Maximum used BDD nodes: 102.

Checking output CIF specification.

Writing output CIF file "datasynth/edge_order_both_custom_dupl_no.ctrlsys.real.cif".
