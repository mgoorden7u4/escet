PROGRAM MAIN
VAR
    aut_r: REAL;
    aut_b: BOOL;
    aut_i: DINT;
    aut_tii: TupleStruct2;
    aut: BYTE;
    inp: REAL;
    c: REAL;
    preset_c: TIME;
    firstRun: BOOL := TRUE;
END_VAR
VAR_TEMP
    b: BOOL;
    curValue: TIME;
    current_aut: BYTE;
    current_aut_1: BYTE;
    current_aut_b: BOOL;
    current_aut_i: DINT;
    current_aut_r: REAL;
    current_aut_tii: TupleStruct2;
    edge_aut: BYTE;
    eventEnabled: BOOL;
    ifResult: BOOL;
    isProgress: BOOL;
    litStruct: TupleStruct2;
    timeOut: BOOL;
END_VAR

(* Header text file for:
 *  -> (-*-) CIF PLC code generator.
 *)

(*------------------------------------------------------
 * Model overview:
 *
 * ----
 * Specification (top-level group):
 *
 * - Continuous variable "c".
 * - Input variable "inp".
 *
 * ----
 * Automaton "aut":
 *
 * - Discrete variable "aut.b".
 * - Discrete variable "aut.i".
 * - Discrete variable "aut.r".
 * - Discrete variable "aut.tii".
 *
 * - PLC current-location variable for automaton "aut".
 *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
 *
 * - PLC edge selection variable "edge_aut".
 *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
 *
 * - Uncontrollable event "aut.f".
 *
 * - Controllable event "aut.e".
 *------------------------------------------------------ *)

(* --- Initialize state or update continuous variables. -------------------- *)
IF firstRun THEN
    firstRun := FALSE;

    (* Initialize the state variables. *)
    (* Initialize discrete variable "aut.r". *)
    aut_r := 0.0;
    (* Initialize discrete variable "aut.b". *)
    aut_b := FALSE;
    (* Initialize discrete variable "aut.i". *)
    aut_i := 0;
    (* Initialize discrete variable "aut.tii". *)
    litStruct.field1 := 0;
    litStruct.field2 := 0;
    aut_tii := litStruct;
    (* Initialize current-location variable for automaton "aut". *)
    aut := 0;
    (* Initialize continuous variable "c". *)
    c := 0.0;
    (* Reset timer of "c". *)
    preset_c := DINT_TO_TIME(REAL_TO_DINT(c * 1000.0));
    ton_c(IN := FALSE, PT := preset_c);
    ton_c(IN := TRUE, PT := preset_c);
ELSE
    (* Update remaining time of continuous variable "c". *)
    ton_c(IN := TRUE, PT := preset_c, Q => timeOut, ET => curValue);
    c := SEL(timeOut, MAX(DINT_TO_REAL(TIME_TO_DINT(preset_c - curValue)) / 1000.0, 0.0), 0.0);
END_IF;

(* --- Process uncontrollable events. -------------------------------------- *)
isProgress := TRUE;
(* Perform events until none can be done anymore. *)
WHILE isProgress DO
    isProgress := FALSE;

    (*************************************************************
     * Try to perform uncontrollable event "aut.f".
     *
     * - Automaton "aut" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edges of automaton "aut" to synchronize for event "aut.f".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "l30":
     *   - 1st edge in the location
     * - Location "l31":
     *   - 1st edge in the location
     ***********)
    b := aut_b;
    IF (NOT b) THEN
        b := aut_b;
    END_IF;
    IF aut = 29 AND b THEN
        edge_aut := 0;
    ELSIF aut = 30 AND aut_b THEN
        edge_aut := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "aut.f" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_aut := aut;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "aut". *)
        IF edge_aut = 0 THEN
            (* Perform assignments of the 1st edge in location "aut.l30". *)
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 30;
        ELSIF edge_aut = 1 THEN
            (* Perform assignments of the 1st edge in location "aut.l31". *)
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 31;
        END_IF;
    END_IF;
END_WHILE;

(* --- Process controllable events. ---------------------------------------- *)
isProgress := TRUE;
(* Perform events until none can be done anymore. *)
WHILE isProgress DO
    isProgress := FALSE;

    (*************************************************************
     * Try to perform controllable event "aut.e".
     *
     * - Automaton "aut" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edges of automaton "aut" to synchronize for event "aut.e".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "l1":
     *   - 1st edge in the location
     * - Location "l2":
     *   - 1st edge in the location
     * - Location "l3":
     *   - 1st edge in the location
     * - Location "l4":
     *   - 1st edge in the location
     * - Location "l5":
     *   - 1st edge in the location
     * - Location "l6":
     *   - 1st edge in the location
     * - Location "l7":
     *   - 1st edge in the location
     * - Location "l8":
     *   - 1st edge in the location
     * - Location "l9":
     *   - 1st edge in the location
     * - Location "l10":
     *   - 1st edge in the location
     * - Location "l11":
     *   - 1st edge in the location
     * - Location "l12":
     *   - 1st edge in the location
     * - Location "l13":
     *   - 1st edge in the location
     * - Location "l14":
     *   - 1st edge in the location
     * - Location "l15":
     *   - 1st edge in the location
     * - Location "l16":
     *   - 1st edge in the location
     * - Location "l17":
     *   - 1st edge in the location
     * - Location "l18":
     *   - 1st edge in the location
     * - Location "l19":
     *   - 1st edge in the location
     * - Location "l20":
     *   - 1st edge in the location
     * - Location "l21":
     *   - 1st edge in the location
     * - Location "l22":
     *   - 1st edge in the location
     * - Location "l23":
     *   - 1st edge in the location
     * - Location "l24":
     *   - 1st edge in the location
     * - Location "l25":
     *   - 1st edge in the location
     * - Location "l26":
     *   - 1st edge in the location
     * - Location "l27":
     *   - 1st edge in the location
     * - Location "l28":
     *   - 1st edge in the location
     * - Location "l29":
     *   - 1st edge in the location
     ***********)
    IF aut = 0 THEN
        edge_aut := 0;
    ELSIF aut = 1 THEN
        edge_aut := 1;
    ELSIF aut = 2 THEN
        edge_aut := 2;
    ELSIF aut = 3 THEN
        edge_aut := 3;
    ELSIF aut = 4 THEN
        edge_aut := 4;
    ELSIF aut = 5 THEN
        edge_aut := 5;
    ELSIF aut = 6 THEN
        edge_aut := 6;
    ELSIF aut = 7 THEN
        edge_aut := 7;
    ELSIF aut = 8 THEN
        edge_aut := 8;
    ELSIF aut = 9 THEN
        edge_aut := 9;
    ELSIF aut = 10 THEN
        edge_aut := 10;
    ELSIF aut = 11 THEN
        edge_aut := 11;
    ELSIF aut = 12 THEN
        edge_aut := 12;
    ELSIF aut = 13 THEN
        edge_aut := 13;
    ELSIF aut = 14 THEN
        edge_aut := 14;
    ELSIF aut = 15 THEN
        edge_aut := 15;
    ELSIF aut = 16 THEN
        edge_aut := 16;
    ELSIF aut = 17 THEN
        edge_aut := 17;
    ELSIF aut = 18 THEN
        edge_aut := 18;
    ELSIF aut = 19 THEN
        edge_aut := 19;
    ELSIF aut = 20 THEN
        edge_aut := 20;
    ELSIF aut = 21 THEN
        edge_aut := 21;
    ELSIF aut = 22 THEN
        edge_aut := 22;
    ELSIF aut = 23 THEN
        edge_aut := 23;
    ELSIF aut = 24 THEN
        edge_aut := 24;
    ELSIF aut = 25 THEN
        edge_aut := 25;
    ELSIF aut = 26 THEN
        edge_aut := 26;
    ELSIF aut = 27 THEN
        edge_aut := 27;
    ELSIF aut = 28 THEN
        edge_aut := 28;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "aut.e" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_aut_1 := aut;
        current_aut_b := aut_b;
        current_aut_i := aut_i;
        current_aut_r := aut_r;
        current_aut_tii := aut_tii;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "aut". *)
        IF edge_aut = 0 THEN
            (* Perform assignments of the 1st edge in location "aut.l1". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := inp;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 1;
        ELSIF edge_aut = 1 THEN
            (* Perform assignments of the 1st edge in location "aut.l2". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := inp + (-1.0);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 2;
        ELSIF edge_aut = 2 THEN
            (* Perform assignments of the 1st edge in location "aut.l3". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := DINT_TO_REAL(current_aut_i);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 3;
        ELSIF edge_aut = 3 THEN
            (* Perform assignments of the 1st edge in location "aut.l4". *)
            (* Perform update of discrete variable "aut.b". *)
            aut_b := (NOT current_aut_b);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 4;
        ELSIF edge_aut = 4 THEN
            (* Perform assignments of the 1st edge in location "aut.l5". *)
            (* Perform update of discrete variable "aut.b". *)
            b := (NOT current_aut_b);
            IF (NOT b) THEN
                b := current_aut_b;
            END_IF;
            aut_b := b;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 5;
        ELSIF edge_aut = 5 THEN
            (* Perform assignments of the 1st edge in location "aut.l6". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := DINT_TO_REAL(current_aut_i) / DINT_TO_REAL(current_aut_i);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 6;
        ELSIF edge_aut = 6 THEN
            (* Perform assignments of the 1st edge in location "aut.l7". *)
            (* Perform update of discrete variable "aut.b". *)
            IF current_aut_b THEN
                ifResult := current_aut_b;
            ELSE
                ifResult := current_aut_b;
            END_IF;
            aut_b := ifResult;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 7;
        ELSIF edge_aut = 7 THEN
            (* Perform assignments of the 1st edge in location "aut.l8". *)
            (* Perform update of discrete variable "aut.i". *)
            aut_i := current_aut_tii.field1;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 8;
        ELSIF edge_aut = 8 THEN
            (* Perform assignments of the 1st edge in location "aut.l9". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := ABS(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 9;
        ELSIF edge_aut = 9 THEN
            (* Perform assignments of the 1st edge in location "aut.l10". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := EXP(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 10;
        ELSIF edge_aut = 10 THEN
            (* Perform assignments of the 1st edge in location "aut.l11". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := LN(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 11;
        ELSIF edge_aut = 11 THEN
            (* Perform assignments of the 1st edge in location "aut.l12". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := LOG(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 12;
        ELSIF edge_aut = 12 THEN
            (* Perform assignments of the 1st edge in location "aut.l13". *)
            (* Perform update of discrete variable "aut.i". *)
            aut_i := MIN(current_aut_i, current_aut_i);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 13;
        ELSIF edge_aut = 13 THEN
            (* Perform assignments of the 1st edge in location "aut.l14". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MIN(DINT_TO_REAL(current_aut_i), current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 14;
        ELSIF edge_aut = 14 THEN
            (* Perform assignments of the 1st edge in location "aut.l15". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MIN(current_aut_r, DINT_TO_REAL(current_aut_i));
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 15;
        ELSIF edge_aut = 15 THEN
            (* Perform assignments of the 1st edge in location "aut.l16". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MIN(current_aut_r, current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 16;
        ELSIF edge_aut = 16 THEN
            (* Perform assignments of the 1st edge in location "aut.l17". *)
            (* Perform update of discrete variable "aut.i". *)
            aut_i := MAX(current_aut_i, current_aut_i);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 17;
        ELSIF edge_aut = 17 THEN
            (* Perform assignments of the 1st edge in location "aut.l18". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MAX(DINT_TO_REAL(current_aut_i), current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 18;
        ELSIF edge_aut = 18 THEN
            (* Perform assignments of the 1st edge in location "aut.l19". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MAX(current_aut_r, DINT_TO_REAL(current_aut_i));
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 19;
        ELSIF edge_aut = 19 THEN
            (* Perform assignments of the 1st edge in location "aut.l20". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := MAX(current_aut_r, current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 20;
        ELSIF edge_aut = 20 THEN
            (* Perform assignments of the 1st edge in location "aut.l21". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := SQRT(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 21;
        ELSIF edge_aut = 21 THEN
            (* Perform assignments of the 1st edge in location "aut.l22". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := ASIN(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 22;
        ELSIF edge_aut = 22 THEN
            (* Perform assignments of the 1st edge in location "aut.l23". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := ACOS(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 23;
        ELSIF edge_aut = 23 THEN
            (* Perform assignments of the 1st edge in location "aut.l24". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := ATAN(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 24;
        ELSIF edge_aut = 24 THEN
            (* Perform assignments of the 1st edge in location "aut.l25". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := SIN(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 25;
        ELSIF edge_aut = 25 THEN
            (* Perform assignments of the 1st edge in location "aut.l26". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := COS(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 26;
        ELSIF edge_aut = 26 THEN
            (* Perform assignments of the 1st edge in location "aut.l27". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := TAN(current_aut_r);
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 27;
        ELSIF edge_aut = 27 THEN
            (* Perform assignments of the 1st edge in location "aut.l28". *)
            (* Perform update of discrete variable "aut.tii". *)
            litStruct.field1 := current_aut_i;
            litStruct.field2 := current_aut_i + 1;
            aut_tii := litStruct;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 28;
        ELSIF edge_aut = 28 THEN
            (* Perform assignments of the 1st edge in location "aut.l29". *)
            (* Perform update of discrete variable "aut.r". *)
            aut_r := current_aut_r * 2.0;
            (* Perform update of discrete variable "aut.i". *)
            aut_i := current_aut_i + 1;
            (* Perform update of current-location variable for automaton "aut". *)
            aut := 29;
        END_IF;
    END_IF;
END_WHILE;
END_PROGRAM
