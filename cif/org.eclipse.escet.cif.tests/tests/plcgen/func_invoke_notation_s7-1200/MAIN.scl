ORGANIZATION_BLOCK MAIN
{ S7_Optimized_Access := 'true' }
    VAR_TEMP
        curValue: TIME;
        current_aut: BYTE;
        current_aut_1: BYTE;
        current_aut_b: BOOL;
        current_aut_i: DINT;
        current_aut_r: LREAL;
        current_aut_tii: TupleStruct2;
        edge_aut: BYTE;
        eventEnabled: BOOL;
        ifResult: BOOL;
        isProgress: BOOL;
        litStruct: TupleStruct2;
        timeOut: BOOL;
    END_VAR

BEGIN
    (* Header text file for:
     *  -> (-*-) CIF PLC code generator.
     *)

    (*------------------------------------------------------
     * Model overview:
     *
     * ----
     * Specification (top-level group):
     *
     * - Continuous variable "c".
     * - Input variable "inp".
     *
     * ----
     * Automaton "aut":
     *
     * - Discrete variable "aut.b".
     * - Discrete variable "aut.i".
     * - Discrete variable "aut.r".
     * - Discrete variable "aut.tii".
     *
     * - PLC current-location variable for automaton "aut".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_aut".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - Uncontrollable event "aut.f".
     *
     * - Controllable event "aut.e".
     *------------------------------------------------------ *)

    (* --- Initialize state or update continuous variables. -------------------- *)
    IF "DB".firstRun THEN
        "DB".firstRun := FALSE;

        (* Initialize the state variables. *)
        (* Initialize discrete variable "aut.r". *)
        "DB".aut_r := 0.0;
        (* Initialize discrete variable "aut.b". *)
        "DB".aut_b := FALSE;
        (* Initialize discrete variable "aut.i". *)
        "DB".aut_i := 0;
        (* Initialize discrete variable "aut.tii". *)
        litStruct.field1 := 0;
        litStruct.field2 := 0;
        "DB".aut_tii := litStruct;
        (* Initialize current-location variable for automaton "aut". *)
        "DB".aut := aut_l1;
        (* Initialize continuous variable "c". *)
        "DB".c := 0.0;
        (* Reset timer of "c". *)
        "DB".preset_c := DINT_TO_TIME(LREAL_TO_DINT("DB".c * 1000.0));
        ton_c.TON(IN := FALSE, PT := "DB".preset_c);
        ton_c.TON(IN := TRUE, PT := "DB".preset_c);
    ELSE
        (* Update remaining time of continuous variable "c". *)
        ton_c.TON(IN := TRUE, PT := "DB".preset_c, Q => timeOut, ET => curValue);
        "DB".c := SEL_LREAL(G := timeOut, IN0 := MAX(IN1 := DINT_TO_LREAL(TIME_TO_DINT("DB".preset_c - curValue)) / 1000.0, IN2 := 0.0), IN1 := 0.0);
    END_IF;

    (* --- Process uncontrollable events. -------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        (*************************************************************
         * Try to perform uncontrollable event "aut.f".
         *
         * - Automaton "aut" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Test edges of automaton "aut" to synchronize for event "aut.f".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "l30":
         *   - 1st edge in the location
         * - Location "l31":
         *   - 1st edge in the location
         ***********)
        IF "DB".aut = aut_l30 AND ("DB".aut_b OR "DB".aut_b) THEN
            edge_aut := 0;
        ELSIF "DB".aut = aut_l31 AND "DB".aut_b THEN
            edge_aut := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "aut.f" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_aut := "DB".aut;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "aut". *)
            IF edge_aut = 0 THEN
                (* Perform assignments of the 1st edge in location "aut.l30". *)
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l31;
            ELSIF edge_aut = 1 THEN
                (* Perform assignments of the 1st edge in location "aut.l31". *)
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l32;
            END_IF;
        END_IF;
    END_WHILE;

    (* --- Process controllable events. ---------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        (*************************************************************
         * Try to perform controllable event "aut.e".
         *
         * - Automaton "aut" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Test edges of automaton "aut" to synchronize for event "aut.e".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "l1":
         *   - 1st edge in the location
         * - Location "l2":
         *   - 1st edge in the location
         * - Location "l3":
         *   - 1st edge in the location
         * - Location "l4":
         *   - 1st edge in the location
         * - Location "l5":
         *   - 1st edge in the location
         * - Location "l6":
         *   - 1st edge in the location
         * - Location "l7":
         *   - 1st edge in the location
         * - Location "l8":
         *   - 1st edge in the location
         * - Location "l9":
         *   - 1st edge in the location
         * - Location "l10":
         *   - 1st edge in the location
         * - Location "l11":
         *   - 1st edge in the location
         * - Location "l12":
         *   - 1st edge in the location
         * - Location "l13":
         *   - 1st edge in the location
         * - Location "l14":
         *   - 1st edge in the location
         * - Location "l15":
         *   - 1st edge in the location
         * - Location "l16":
         *   - 1st edge in the location
         * - Location "l17":
         *   - 1st edge in the location
         * - Location "l18":
         *   - 1st edge in the location
         * - Location "l19":
         *   - 1st edge in the location
         * - Location "l20":
         *   - 1st edge in the location
         * - Location "l21":
         *   - 1st edge in the location
         * - Location "l22":
         *   - 1st edge in the location
         * - Location "l23":
         *   - 1st edge in the location
         * - Location "l24":
         *   - 1st edge in the location
         * - Location "l25":
         *   - 1st edge in the location
         * - Location "l26":
         *   - 1st edge in the location
         * - Location "l27":
         *   - 1st edge in the location
         * - Location "l28":
         *   - 1st edge in the location
         * - Location "l29":
         *   - 1st edge in the location
         ***********)
        IF "DB".aut = aut_l1 THEN
            edge_aut := 0;
        ELSIF "DB".aut = aut_l2 THEN
            edge_aut := 1;
        ELSIF "DB".aut = aut_l3 THEN
            edge_aut := 2;
        ELSIF "DB".aut = aut_l4 THEN
            edge_aut := 3;
        ELSIF "DB".aut = aut_l5 THEN
            edge_aut := 4;
        ELSIF "DB".aut = aut_l6 THEN
            edge_aut := 5;
        ELSIF "DB".aut = aut_l7 THEN
            edge_aut := 6;
        ELSIF "DB".aut = aut_l8 THEN
            edge_aut := 7;
        ELSIF "DB".aut = aut_l9 THEN
            edge_aut := 8;
        ELSIF "DB".aut = aut_l10 THEN
            edge_aut := 9;
        ELSIF "DB".aut = aut_l11 THEN
            edge_aut := 10;
        ELSIF "DB".aut = aut_l12 THEN
            edge_aut := 11;
        ELSIF "DB".aut = aut_l13 THEN
            edge_aut := 12;
        ELSIF "DB".aut = aut_l14 THEN
            edge_aut := 13;
        ELSIF "DB".aut = aut_l15 THEN
            edge_aut := 14;
        ELSIF "DB".aut = aut_l16 THEN
            edge_aut := 15;
        ELSIF "DB".aut = aut_l17 THEN
            edge_aut := 16;
        ELSIF "DB".aut = aut_l18 THEN
            edge_aut := 17;
        ELSIF "DB".aut = aut_l19 THEN
            edge_aut := 18;
        ELSIF "DB".aut = aut_l20 THEN
            edge_aut := 19;
        ELSIF "DB".aut = aut_l21 THEN
            edge_aut := 20;
        ELSIF "DB".aut = aut_l22 THEN
            edge_aut := 21;
        ELSIF "DB".aut = aut_l23 THEN
            edge_aut := 22;
        ELSIF "DB".aut = aut_l24 THEN
            edge_aut := 23;
        ELSIF "DB".aut = aut_l25 THEN
            edge_aut := 24;
        ELSIF "DB".aut = aut_l26 THEN
            edge_aut := 25;
        ELSIF "DB".aut = aut_l27 THEN
            edge_aut := 26;
        ELSIF "DB".aut = aut_l28 THEN
            edge_aut := 27;
        ELSIF "DB".aut = aut_l29 THEN
            edge_aut := 28;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "aut.e" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_aut_1 := "DB".aut;
            current_aut_b := "DB".aut_b;
            current_aut_i := "DB".aut_i;
            current_aut_r := "DB".aut_r;
            current_aut_tii := "DB".aut_tii;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "aut". *)
            IF edge_aut = 0 THEN
                (* Perform assignments of the 1st edge in location "aut.l1". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := "DB".inp;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l2;
            ELSIF edge_aut = 1 THEN
                (* Perform assignments of the 1st edge in location "aut.l2". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := "DB".inp + (-1.0);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l3;
            ELSIF edge_aut = 2 THEN
                (* Perform assignments of the 1st edge in location "aut.l3". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := DINT_TO_LREAL(current_aut_i);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l4;
            ELSIF edge_aut = 3 THEN
                (* Perform assignments of the 1st edge in location "aut.l4". *)
                (* Perform update of discrete variable "aut.b". *)
                "DB".aut_b := (NOT current_aut_b);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l5;
            ELSIF edge_aut = 4 THEN
                (* Perform assignments of the 1st edge in location "aut.l5". *)
                (* Perform update of discrete variable "aut.b". *)
                "DB".aut_b := (NOT current_aut_b) OR current_aut_b;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l6;
            ELSIF edge_aut = 5 THEN
                (* Perform assignments of the 1st edge in location "aut.l6". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := DINT_TO_LREAL(current_aut_i) / DINT_TO_LREAL(current_aut_i);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l7;
            ELSIF edge_aut = 6 THEN
                (* Perform assignments of the 1st edge in location "aut.l7". *)
                (* Perform update of discrete variable "aut.b". *)
                IF current_aut_b THEN
                    ifResult := current_aut_b;
                ELSE
                    ifResult := current_aut_b;
                END_IF;
                "DB".aut_b := ifResult;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l8;
            ELSIF edge_aut = 7 THEN
                (* Perform assignments of the 1st edge in location "aut.l8". *)
                (* Perform update of discrete variable "aut.i". *)
                "DB".aut_i := current_aut_tii.field1;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l9;
            ELSIF edge_aut = 8 THEN
                (* Perform assignments of the 1st edge in location "aut.l9". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := ABS(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l10;
            ELSIF edge_aut = 9 THEN
                (* Perform assignments of the 1st edge in location "aut.l10". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := EXP(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l11;
            ELSIF edge_aut = 10 THEN
                (* Perform assignments of the 1st edge in location "aut.l11". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := LN(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l12;
            ELSIF edge_aut = 11 THEN
                (* Perform assignments of the 1st edge in location "aut.l12". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := LN(current_aut_r) / LN(10.0);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l13;
            ELSIF edge_aut = 12 THEN
                (* Perform assignments of the 1st edge in location "aut.l13". *)
                (* Perform update of discrete variable "aut.i". *)
                "DB".aut_i := MIN(IN1 := current_aut_i, IN2 := current_aut_i);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l14;
            ELSIF edge_aut = 13 THEN
                (* Perform assignments of the 1st edge in location "aut.l14". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MIN(IN1 := DINT_TO_LREAL(current_aut_i), IN2 := current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l15;
            ELSIF edge_aut = 14 THEN
                (* Perform assignments of the 1st edge in location "aut.l15". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MIN(IN1 := current_aut_r, IN2 := DINT_TO_LREAL(current_aut_i));
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l16;
            ELSIF edge_aut = 15 THEN
                (* Perform assignments of the 1st edge in location "aut.l16". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MIN(IN1 := current_aut_r, IN2 := current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l17;
            ELSIF edge_aut = 16 THEN
                (* Perform assignments of the 1st edge in location "aut.l17". *)
                (* Perform update of discrete variable "aut.i". *)
                "DB".aut_i := MAX(IN1 := current_aut_i, IN2 := current_aut_i);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l18;
            ELSIF edge_aut = 17 THEN
                (* Perform assignments of the 1st edge in location "aut.l18". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MAX(IN1 := DINT_TO_LREAL(current_aut_i), IN2 := current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l19;
            ELSIF edge_aut = 18 THEN
                (* Perform assignments of the 1st edge in location "aut.l19". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MAX(IN1 := current_aut_r, IN2 := DINT_TO_LREAL(current_aut_i));
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l20;
            ELSIF edge_aut = 19 THEN
                (* Perform assignments of the 1st edge in location "aut.l20". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := MAX(IN1 := current_aut_r, IN2 := current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l21;
            ELSIF edge_aut = 20 THEN
                (* Perform assignments of the 1st edge in location "aut.l21". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := SQRT(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l22;
            ELSIF edge_aut = 21 THEN
                (* Perform assignments of the 1st edge in location "aut.l22". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := ASIN(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l23;
            ELSIF edge_aut = 22 THEN
                (* Perform assignments of the 1st edge in location "aut.l23". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := ACOS(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l24;
            ELSIF edge_aut = 23 THEN
                (* Perform assignments of the 1st edge in location "aut.l24". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := ATAN(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l25;
            ELSIF edge_aut = 24 THEN
                (* Perform assignments of the 1st edge in location "aut.l25". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := SIN(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l26;
            ELSIF edge_aut = 25 THEN
                (* Perform assignments of the 1st edge in location "aut.l26". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := COS(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l27;
            ELSIF edge_aut = 26 THEN
                (* Perform assignments of the 1st edge in location "aut.l27". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := TAN(current_aut_r);
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l28;
            ELSIF edge_aut = 27 THEN
                (* Perform assignments of the 1st edge in location "aut.l28". *)
                (* Perform update of discrete variable "aut.tii". *)
                litStruct.field1 := current_aut_i;
                litStruct.field2 := current_aut_i + 1;
                "DB".aut_tii := litStruct;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l29;
            ELSIF edge_aut = 28 THEN
                (* Perform assignments of the 1st edge in location "aut.l29". *)
                (* Perform update of discrete variable "aut.r". *)
                "DB".aut_r := current_aut_r * 2.0;
                (* Perform update of discrete variable "aut.i". *)
                "DB".aut_i := current_aut_i + 1;
                (* Perform update of current-location variable for automaton "aut". *)
                "DB".aut := aut_l30;
            END_IF;
        END_IF;
    END_WHILE;
END_ORGANIZATION_BLOCK
