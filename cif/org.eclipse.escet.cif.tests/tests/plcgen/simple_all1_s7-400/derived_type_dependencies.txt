Direct dependencies:
Each line has the form 'TYPE <- DEPENDENCY_1 DEPENDENCY_2 ...'.
To be able to use type 'TYPE', all 'DEPENDENCY_*' types must be available already.

    TupleStruct2 <-
    g_trr <-
    g_ttrrr <- g_trr

Groups of dependent derived types:
A derived type in a non-first group depends on at least one type from the previous group,
and possibly also groups before that.

    Group 1:
        TupleStruct2
        g_trr

    Group 2:
        g_ttrrr
