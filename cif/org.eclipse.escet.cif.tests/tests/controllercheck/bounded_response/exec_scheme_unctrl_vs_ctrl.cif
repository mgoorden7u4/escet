//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollables bound: 3.
// Controllables bound: 546.

plant unctrl_vs_ctrl:
  uncontrollable u1, u2;
  controllable c1, c2;
  controllable z;
  disc int[0..2000] v, w;

  location:
    initial;
    marked;
    edge u1 when v < 500 and v mod 2 = 0 do v := v + v + 1;
    edge c1 when v < 500 and v mod 2 = 1 do v := v + v + 4;
    edge u2 when v < 10                  do v := v + v + 2;
    edge c2 when v < 100                 do v := v + v + 8;

    // Starting from the initial state, considering the execution scheme:
    //
    // Uncontrollables iteration 1.1:
    // - u1, v: 0 -> 1
    // - u2, v: 1 -> 4
    // Uncontrollables iteration 1.2:
    // - u1, v: 4 -> 9
    // - u2, v: 9 -> 20
    // Uncontrollables iteration 1.3: -> 3 times for uncontrollables is the maximum.
    // - u1, v: 20 -> 41
    // Controllables iteration 1.1:
    // - c1, v: 41 -> 86
    // - c2, v: 86 -> 180
    //
    // Uncontrollables iteration 2.1:
    // - u1, v: 180 -> 361
    // Controllables iteration 2.1:
    // - c1, v: 361 -> 726

    edge z when w < v do w := w + 1; // 180 times for 1st execution, 726 - 180 = 546 times for 2nd one -> bound 546.
end
