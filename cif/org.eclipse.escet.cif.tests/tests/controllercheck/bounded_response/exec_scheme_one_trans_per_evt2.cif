//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollables bound: 18.
// Controllables bound: 20.

group one_trans_per_evt2:
  uncontrollable e;

  // Starting from the initial state, considering the execution scheme:
  //
  // We get 4 transitions for automata 'one_trans_per_evt2a' and 'one_trans_per_evt2b':
  // 1) 2a 1st edge + 2b 1st edge, v: 0 -> 1, 2a w: 0 -> 1,  2b w: 0 -> 1
  // 2) 2a 1st edge + 2b 2nd edge, v: 1 -> 2, 2a w: 1 -> 3,  2b w: 1 -> 4
  // 3) 2a 2nd edge + 2b 1st edge, v: 2 -> 3, 2a w: 3 -> 8,  2b w: 4 -> 9
  // 4) 2a 2nd edge + 2b 2nd edge, v: 3 -> 4, 2a w: 8 -> 18, 2b w: 9 -> 20
end

plant one_trans_per_evt2a:
  disc int[0..5] v;
  disc int[0..50] w;

  location:
    initial;
    marked;
    edge one_trans_per_evt2.e when v <= 1 do v := v + 1, w := w + w + 1;
    edge one_trans_per_evt2.e when v <= 3 do v := v + 1, w := w + w + 2;
end

plant one_trans_per_evt2b:
  disc int[0..50] w;

  location:
    initial;
    marked;
    edge one_trans_per_evt2.e when one_trans_per_evt2a.v mod 2 = 0 do w := w + w + 1;
    edge one_trans_per_evt2.e when one_trans_per_evt2a.v mod 2 = 1 do w := w + w + 2;
end

plant z:
  uncontrollable ua;
  controllable cb;
  disc int[0..50] va, vb;

  location:
    initial;
    marked;
    edge ua when va < one_trans_per_evt2a.w do va := va + 1; // 18 times -> bound.
    edge cb when vb < one_trans_per_evt2b.w do vb := vb + 1; // 20 times -> bound.
end
