//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.tests.checkers.checks;

import org.eclipse.escet.cif.checkers.checks.TypeNoSpecificNestedTypesCheck;

/**
 * Check that only allows the following directly nested container types:
 * <ul>
 * <li>A tuple type directly nested in an array type.</li>
 * <li>A set type directly nested in a set, dictionary, or tuple type.</li>
 * </ul>
 */
public class TypeNoSpecificNestedTypesCheckAll extends TypeNoSpecificNestedTypesCheck {
    /** Constructor of the {@link TypeNoSpecificNestedTypesCheckAll} class. */
    public TypeNoSpecificNestedTypesCheckAll() {
        super(TypeNoSpecificNestedTypesCheck.FORBID_ALL); // Allow nothing.
        allow(ContainerType.ARRAY, ContainerType.TUPLE); // Add allowing a tuple type directly in an array type.
        allow(ALL_CONTAINERS, ContainerType.SET) // Add allowing a set type is allowed in any container type,
                .forbid(ALL_LISTS, ContainerType.SET); // except set type in list containers.
    }
}
