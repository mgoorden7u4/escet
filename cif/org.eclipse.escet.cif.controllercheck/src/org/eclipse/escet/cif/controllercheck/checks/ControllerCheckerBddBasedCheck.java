//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks;

import java.util.Collections;
import java.util.EnumSet;

import org.eclipse.escet.cif.bdd.conversion.CifToBddConverter;
import org.eclipse.escet.cif.bdd.settings.AllowNonDeterminism;
import org.eclipse.escet.cif.bdd.settings.CifBddSettings;
import org.eclipse.escet.cif.bdd.settings.CifBddStatistics;
import org.eclipse.escet.cif.bdd.spec.CifBddEdge;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.utils.CifBddApplyPlantInvariants;
import org.eclipse.escet.cif.cif2cif.RelabelSupervisorsAsPlants;
import org.eclipse.escet.cif.controllercheck.ControllerCheckerSettings;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.common.emf.EMFHelper;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.output.DebugNormalOutput;

import com.github.javabdd.BDDFactory;

/**
 * A BDD-based check that can be performed by the controller properties checker.
 *
 * @param <T> The type of the conclusion of the check.
 */
public abstract class ControllerCheckerBddBasedCheck<T extends CheckConclusion> implements ControllerCheckerCheck<T> {
    /** The saturation instance number for the backward 'cpp' search by the non-blocking under control check. */
    public static final int SATURATION_INSTANCE_NONBLOCKING_CCP = 1;

    /** The saturation instance number for the backward 'bad' search by the non-blocking under control check. */
    public static final int SATURATION_INSTANCE_NONBLOCKING_BAD = 2;

    @Override
    public T performCheck(Specification spec, String specAbsPath, ControllerCheckerSettings checkerSettings) {
        DebugNormalOutput debugOutput = checkerSettings.getDebugOutput();

        // Convert specification to a BDD representation.
        debugOutput.line("Converting CIF specification to a BDD representation:");
        debugOutput.inc();

        CifBddSpec cifBddSpec = convertToBdd(spec, specAbsPath, checkerSettings);

        debugOutput.dec();
        debugOutput.line();

        if (cifBddSpec == null) {
            return null;
        }

        // Perform the check.
        return performCheck(cifBddSpec);
    }

    /**
     * Performs the check.
     *
     * @param cifBddSpec The BDD representation of the specification to check.
     * @return The check result, or {@code null} if termination was requested.
     */
    protected abstract T performCheck(CifBddSpec cifBddSpec);

    /**
     * Convert a CIF specification to a BDD representation. Also performs some BDD-specific checks on the input
     * specification.
     *
     * @param spec The specification to convert. Must not be modified.
     * @param specAbsPath The absolute local file system path to the CIF file.
     * @param checkerSettings The controller properties checker settings.
     * @return The CIF/BDD specification, or {@code null} if termination was requested.
     */
    private CifBddSpec convertToBdd(Specification spec, String specAbsPath,
            ControllerCheckerSettings checkerSettings)
    {
        Termination termination = checkerSettings.getTermination();

        // Use a copy of the specification.
        spec = EMFHelper.deepclone(spec);
        if (termination.isRequested()) {
            return null;
        }

        // Relabel supervisors as plants, to deal with them in the same way.
        new RelabelSupervisorsAsPlants().transform(spec);

        // Get CIF/BDD settings.
        CifBddSettings cifBddSettings = createCifBddSettings(checkerSettings);
        cifBddSettings.setModificationAllowed(false);

        // Pre-process the CIF specification:
        // - Does not warn about CIF/SVG specifications, as they have been removed already.
        // - Does not warn about plants referring to requirement state, as we disabled that check.
        CifToBddConverter converter = new CifToBddConverter("CIF controller properties checker");
        converter.preprocess(spec, specAbsPath, cifBddSettings.getWarnOutput(),
                cifBddSettings.getDoPlantsRefReqsWarn(), cifBddSettings.getTermination());

        // Convert the CIF specification to its BDD representation. Also checks BDD-specific preconditions.
        BDDFactory factory = CifToBddConverter.createFactory(cifBddSettings, Collections.emptyList(),
                Collections.emptyList());
        CifBddSpec cifBddSpec = converter.convert(spec, cifBddSettings, factory);
        if (termination.isRequested()) {
            return null;
        }

        // Clean up no longer needed BDD predicates.
        cifBddSpec.freeIntermediateBDDs(true);
        if (termination.isRequested()) {
            return null;
        }

        // Apply the plant state/event exclusion invariants.
        CifBddApplyPlantInvariants.applyStateEvtExclPlantsInvs(cifBddSpec, "system", () -> null,
                cifBddSettings.getDebugOutput().isEnabled());
        if (termination.isRequested()) {
            return null;
        }

        // Initialize applying edges.
        for (CifBddEdge edge: cifBddSpec.edges) {
            edge.initApply();
            if (termination.isRequested()) {
                return null;
            }
        }

        // Return the CIF/BDD specification.
        return cifBddSpec;
    }

    /**
     * Create CIF/BDD settings for the check.
     *
     * <p>
     * Concrete checks may override this, call the super implementation, adapt some settings, and return those adapted
     * settings.
     * </p>
     *
     * @param checkerSettings The controller properties checker settings.
     * @return The newly created CIF/BDD settings for the check.
     */
    protected CifBddSettings createCifBddSettings(ControllerCheckerSettings checkerSettings) {
        // Create settings.
        CifBddSettings cifBddSettings = new CifBddSettings();

        // Setup interaction with environment.
        cifBddSettings.setTermination(checkerSettings.getTermination());
        cifBddSettings.setDebugOutput(checkerSettings.getDebugOutput());
        cifBddSettings.setNormalOutput(checkerSettings.getNormalOutput());
        cifBddSettings.setWarnOutput(checkerSettings.getWarnOutput());
        cifBddSettings.setIndentAmount(4);
        cifBddSettings.setCifBddStatistics(EnumSet.noneOf(CifBddStatistics.class));
        cifBddSettings.setDoPlantsRefReqsWarn(false);

        // We allow non-determinism for uncontrollables, like for synthesis. For instance, to allow uncontrollable
        // channels. We don't allow non-determinism for controllables, also like for synthesis. We disallow
        // non-determinism for controllable events also because the confluence check doesn't support it.
        cifBddSettings.setAllowNonDeterminism(AllowNonDeterminism.UNCONTROLLABLE);

        // Return the settings.
        return cifBddSettings;
    }
}
