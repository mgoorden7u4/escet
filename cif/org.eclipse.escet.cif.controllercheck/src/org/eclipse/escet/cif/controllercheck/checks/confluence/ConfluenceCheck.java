//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks.confluence;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.reverse;
import static org.eclipse.escet.common.java.Lists.set2list;
import static org.eclipse.escet.common.java.Strings.SORTER;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.escet.cif.bdd.settings.CifBddSettings;
import org.eclipse.escet.cif.bdd.spec.CifBddEdge;
import org.eclipse.escet.cif.bdd.spec.CifBddEdgeApplyDirection;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.spec.CifBddVariable;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.controllercheck.ControllerCheckerSettings;
import org.eclipse.escet.cif.controllercheck.checks.ControllerCheckerBddBasedCheck;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.common.java.Lists;
import org.eclipse.escet.common.java.Pair;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.output.DebugNormalOutput;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDVarSet;

/** Class to check confluence of the specification. */
public class ConfluenceCheck extends ControllerCheckerBddBasedCheck<ConfluenceCheckConclusion> {
    /** Debug global flow of the checks, which pairs are tested, where are they matched. */
    private static final boolean DEBUG_GLOBAL = false;

    /** Whether to enable debugging output for independence checking. */
    private static final boolean DEBUG_INDENPENCE = false;

    /** Whether to enable debugging output for reversible checking. */
    private static final boolean DEBUG_REVERSIBLE = false;

    /** Whether to enable debugging output for update equivalence. */
    private static final boolean DEBUG_UPDATE_EQUIVALENCE = false;

    /** The name of the property being checked. */
    public static final String PROPERTY_NAME = "confluence";

    @Override
    public String getPropertyName() {
        return PROPERTY_NAME;
    }

    @Override
    protected CifBddSettings createCifBddSettings(ControllerCheckerSettings checkerSettings) {
        // Create default settings.
        CifBddSettings settings = super.createCifBddSettings(checkerSettings);

        // We need one set of extra variables, the 'zero' variables. But we must add an even number of extra variables,
        // so we also get an extra unused/dummy set of variables.
        settings.setBddNumberOfExtraVarDomains(2);

        // Return settings for this check.
        return settings;
    }

    @Override
    public ConfluenceCheckConclusion performCheck(CifBddSpec cifBddSpec) {
        // Get information from BDD specification.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput out = cifBddSpec.settings.getNormalOutput();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();
        List<Event> controllableEvents = set2list(cifBddSpec.controllables);

        // Check for cases where confluence trivially holds.
        if (controllableEvents.isEmpty()) {
            dbg.line("No controllable events. Confluence trivially holds.");
            return new ConfluenceCheckConclusion(List.of());
        }
        if (controllableEvents.size() == 1) {
            dbg.line("One controllable event. Confluence trivially holds.");
            return new ConfluenceCheckConclusion(List.of());
        }

        // Create a 'zero' variables to 'old' variables identity relation: 'x0 = x and y0 = y and z0 = z and ...'.
        BDD zeroToOldVarRelations = createZeroToOldVarsRelations(cifBddSpec);

        if (termination.isRequested()) {
            return null;
        }

        // Get single edge per controllable event.
        Map<Event, CifBddEdge> eventToEdge = cifBddSpec.eventEdges.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> Lists.single(e.getValue())));

        // Storage of test results.
        List<Pair<String, String>> mutualExclusives = list(); // List with pairs that are mutual exclusive.
        List<Pair<String, String>> updateEquivalents = list(); // List with pairs that are update equivalent.
        List<Pair<String, String>> independents = list(); // List with pairs that are independent.
        List<Pair<String, String>> skippables = list(); // List with pairs that are skippable.
        List<Pair<String, String>> reversibles = list(); // List with pairs that are reversible.
        List<Pair<String, String>> cannotProves = list(); // List with pairs where confluence could not be proven.

        // For all pairs of events, perform the checks. But, avoid checking both both (A, B) and (B, A).
        for (int i = 0; i < controllableEvents.size(); i++) {
            Event event1 = controllableEvents.get(i);
            String evt1Name = CifTextUtils.getAbsName(event1);
            CifBddEdge edge1 = eventToEdge.get(event1);

            for (int j = i + 1; j < controllableEvents.size(); j++) {
                Event event2 = controllableEvents.get(j);
                String evt2Name = CifTextUtils.getAbsName(event2);
                CifBddEdge edge2 = eventToEdge.get(event2);

                EventPairData eventPairData = new EventPairData(cifBddSpec, event1, event2, edge1, edge2, evt1Name,
                        evt2Name, zeroToOldVarRelations);

                if (termination.isRequested()) {
                    return null;
                }

                if (DEBUG_GLOBAL) {
                    dbg.line("Trying event pair (%s, %s).", evt1Name, evt2Name);
                }

                // Check for mutual exclusiveness (never both guards are enabled at the same time).
                boolean isMutualExclusive = checkMutualExclusive(eventPairData, dbg, termination);
                if (isMutualExclusive) {
                    mutualExclusives.add(makeSortedPair(eventPairData.evt1Name, eventPairData.evt2Name));
                    eventPairData.free();
                    continue;
                }

                if (termination.isRequested()) {
                    return null;
                }

                // Check for update equivalence (both events make the same changes).
                boolean isUpdateEquivalent = checkUpdateEquivalent(eventPairData, dbg, termination);
                if (isUpdateEquivalent) {
                    updateEquivalents.add(makeSortedPair(eventPairData.evt1Name, eventPairData.evt2Name));
                    eventPairData.free();
                    continue;
                }

                if (termination.isRequested()) {
                    return null;
                }

                // Check for independence (diamond shape edges leading to the same changes).
                boolean isIndependent = checkIndependent(eventPairData, dbg, termination);
                if (isIndependent) {
                    independents.add(makeSortedPair(eventPairData.evt1Name, eventPairData.evt2Name));
                    eventPairData.free();
                    continue;
                }

                if (termination.isRequested()) {
                    return null;
                }

                // Check for skippable (may perform a second event, but its changes are undone).
                boolean isSkippable = checkSkippable(eventPairData, dbg, termination);
                if (isSkippable) {
                    skippables.add(makeSortedPair(eventPairData.evt1Name, eventPairData.evt2Name));
                    eventPairData.free();
                    continue;
                }

                if (termination.isRequested()) {
                    return null;
                }

                // Check reversible (if event 2 is performed before event 1 its effect is reverted by event 3 after
                // event 1).
                boolean isReversible = checkReversible(eventPairData, eventToEdge, dbg, termination);
                if (isReversible) {
                    reversibles.add(makeSortedPair(eventPairData.evt1Name, eventPairData.evt2Name));
                    eventPairData.free();
                    continue;
                }

                if (termination.isRequested()) {
                    return null;
                }

                // Cleanup.
                eventPairData.free();

                // None of the checks hold: failed to prove confluence.
                cannotProves.add(makeSortedPair(evt1Name, evt2Name));
                if (DEBUG_GLOBAL) {
                    dbg.line("  -> event pair (%s, %s) is CANNOT-PROVE.", evt1Name, evt2Name);
                }
            }
        }

        if (termination.isRequested()) {
            return null;
        }

        // Cleanup.
        zeroToOldVarRelations.free();

        // Dump results.
        boolean needEmptyLine = false;
        needEmptyLine = dumpMatches(mutualExclusives, "Mutual exclusive event pairs", out, needEmptyLine);
        needEmptyLine = dumpMatches(updateEquivalents, "Update equivalent event pairs", out, needEmptyLine);
        needEmptyLine = dumpMatches(independents, "Independent event pairs", out, needEmptyLine);
        needEmptyLine = dumpMatches(skippables, "Skippable event pairs", out, needEmptyLine);
        needEmptyLine = dumpMatches(reversibles, "Reversible event pairs", out, needEmptyLine);

        if (mutualExclusives.isEmpty() && updateEquivalents.isEmpty() && independents.isEmpty() && skippables.isEmpty()
                && reversibles.isEmpty())
        {
            dbg.line("No proven pairs.");
        }

        dbg.line();
        if (cannotProves.isEmpty()) {
            dbg.line("All pairs proven. Confluence holds.");
        } else {
            dbg.line("Some pairs unproven. Confluence may not hold.");
        }

        // Return check conclusion.
        return new ConfluenceCheckConclusion(cannotProves);
    }

    /**
     * Create a 'zero' variables to 'old' variables identity relation: 'x0 = x and y0 = y and z0 = z and ...'.
     *
     * <p>
     * The 'zero' variables hold the values of the variables before taking any transitions (zero transitions taken). The
     * reason for adding these 'zero' variables and identity relations is as follows. To prove confluence, we consider
     * pairs of events, and their global guards and updates. We start with states where the guards of both edges are
     * enabled. If there are any (they are not mutually exclusive), we connect the 'zero' variables to the 'old'
     * variables through an 'x0 = x and y0 = y and z0 = z and ...' identity relation, and add this to the combined guard
     * of the two edges. This allows us to keep track of what values the variables had before taking any edges (the
     * possible 'source states'). If we then apply edges to the combined guard predicate, the values of the 'x'
     * variables may get updated, but the 'x0' variables keep their values. This allows detection of different paths
     * through the state space that lead to the same set of states when considering all possible source states and
     * target states for certain events, while for specific source states the different paths lead to different target
     * states. For instance, consider the following example:
     * <ul>
     * <li>Event 'a' has guard 'true' and update 'x := not x'.</li>
     * <li>Event 'b' has guard 'true' and update 'x := x'.</li>
     * <li>Their combined guard is 'true'. If we don't consider the 'zero' variables, and apply the update of each edge
     * to predicate 'true', both edges result in set of target states 'true'. However, one edge inverts the value of
     * 'x', while the other keeps it the same. They thus don't have the same effect.</li>
     * <li>If we do consider 'zero' variables, we first change the combined guard 'true' to 'x0 = x'. Then we apply the
     * update of each edge, which gives us 'x0 = not x' and 'x0 = x'. In this case, we can detect that for different
     * source states the target states are different when applying these two edges.</li>
     * </ul>
     * </p>
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @return The 'zero' variables to old variables identity relations, or {@code null} if termination was requested.
     */
    private BDD createZeroToOldVarsRelations(CifBddSpec cifBddSpec) {
        Termination termination = cifBddSpec.settings.getTermination();

        // We use the reverse variable order when constructing the relations, for better performance.
        BDD relations = cifBddSpec.factory.one();
        for (CifBddVariable cifBddVar: reverse(Arrays.asList(cifBddSpec.variables))) {
            // Get relation for a single CIF/BDD variable.
            BDD relation = cifBddVar.domain.buildEquals(cifBddVar.domainsExtra[0]);

            if (termination.isRequested()) {
                return null;
            }

            // Add it to the relations.
            relations = relations.andWith(relation);

            if (termination.isRequested()) {
                return null;
            }
        }
        return relations;
    }

    /**
     * Check an event pair for being mutual exclusive.
     *
     * @param eventPairData The event pair data. Is modified in-place.
     * @param dbg The callback for debug output.
     * @param termination The cooperative termination query function.
     * @return {@code true} if the event pair is mutual exclusive, {@code false} if it is not mutual exclusive, or if
     *     termination was requested.
     */
    private boolean checkMutualExclusive(EventPairData eventPairData, DebugNormalOutput dbg, Termination termination) {
        // Get data.
        BDD commonEnabledGuards = eventPairData.getCommonEnabledGuardsNoZero();

        if (termination.isRequested()) {
            return false;
        }

        // Perform check.
        boolean isMutualExclusive = commonEnabledGuards.isZero();

        // Debug output.
        if (isMutualExclusive) {
            if (DEBUG_GLOBAL) {
                dbg.line("  -> event pair (%s, %s) is mutual exclusive.", eventPairData.evt1Name,
                        eventPairData.evt2Name);
            }
        }

        // Return the result.
        return isMutualExclusive;
    }

    /**
     * Check an event pair for being update equivalent.
     *
     * @param eventPairData The event pair data. Is modified in-place.
     * @param dbg The callback for debug output.
     * @param termination The cooperative termination query function.
     * @return {@code true} if the event pair is update equivalent, {@code false} if it is not update equivalent, or if
     *     termination was requested.
     */
    private boolean checkUpdateEquivalent(EventPairData eventPairData, DebugNormalOutput dbg, Termination termination) {
        // Debug output.
        if (DEBUG_UPDATE_EQUIVALENCE) {
            dbg.line("Update equivalence: edge1 guard: %s", eventPairData.edge1.guard.toString());
            dbg.line("Update equivalence: edge2 guard: %s", eventPairData.edge2.guard.toString());
            dbg.line("Update equivalence: edge1 update/guard: %s", eventPairData.edge1.updateGuard.toString());
            dbg.line("Update equivalence: edge2 update/guard: %s", eventPairData.edge1.updateGuard.toString());
        }

        // Get data.
        BDD commonEnabledZeroStates = eventPairData.getCommonEnabledZeroStates(termination);

        if (termination.isRequested()) {
            return false;
        }

        BDD event1Done = eventPairData.getEvent1Done(termination);

        if (termination.isRequested()) {
            return false;
        }

        BDD event2Done = eventPairData.getEvent2Done(termination);

        if (termination.isRequested()) {
            return false;
        }

        // Debug output.
        if (DEBUG_UPDATE_EQUIVALENCE) {
            dbg.line("Update equivalence: commonEnabledZeroStates: %s", commonEnabledZeroStates.toString());
            dbg.line("Update equivalence: event1 done: %s", event1Done.toString());
            dbg.line("Update equivalence: event2 done: %s", event2Done.toString());
        }

        // Perform check.
        boolean isUpdateEquivalent = !event1Done.isZero() && event1Done.equals(event2Done)
                && allStatesCovered(commonEnabledZeroStates, event1Done, eventPairData.varSetOld, termination);

        if (termination.isRequested()) {
            return false;
        }

        // Debug output.
        if (isUpdateEquivalent) {
            if (DEBUG_GLOBAL) {
                dbg.line("  -> event pair (%s, %s) is update equivalent.", eventPairData.evt1Name,
                        eventPairData.evt2Name);
            }
        }

        // Return the result.
        return isUpdateEquivalent;
    }

    /**
     * Check an event pair for being independent.
     *
     * @param eventPairData The event pair data. Is modified in-place.
     * @param dbg The callback for debug output.
     * @param termination The cooperative termination query function.
     * @return {@code true} if the event pair is independent, {@code false} if it is not independent, or if termination
     *     was requested.
     */
    private boolean checkIndependent(EventPairData eventPairData, DebugNormalOutput dbg, Termination termination) {
        // Debug output.
        if (DEBUG_INDENPENCE) {
            dbg.line("Independence: edge1 guard: %s", eventPairData.edge1.guard.toString());
            dbg.line("Independence: edge2 guard: %s", eventPairData.edge2.guard.toString());
            dbg.line("Independence: edge1 update/guard: %s", eventPairData.edge1.updateGuard.toString());
            dbg.line("Independence: edge2 update/guard: %s", eventPairData.edge1.updateGuard.toString());
        }

        // Get data.
        BDD commonEnabledZeroStates = eventPairData.getCommonEnabledZeroStates(termination);

        if (termination.isRequested()) {
            return false;
        }

        BDD event12Done = eventPairData.getEvent12Done(termination);

        if (termination.isRequested()) {
            return false;
        }

        BDD event21Done = eventPairData.getEvent21Done(termination);

        if (termination.isRequested()) {
            return false;
        }

        // Debug output.
        if (DEBUG_INDENPENCE) {
            dbg.line("Independence: event1+2 done: %s", event12Done.toString());
            dbg.line("Independence: event2+1 done: %s", event21Done.toString());
        }

        if (termination.isRequested()) {
            return false;
        }

        // Perform check.
        boolean isIndependent = !event12Done.isZero() && event12Done.equals(event21Done)
                && allStatesCovered(commonEnabledZeroStates, event12Done, eventPairData.varSetOld, termination);

        if (termination.isRequested()) {
            return false;
        }

        // Debug output.
        if (isIndependent) {
            if (DEBUG_GLOBAL) {
                dbg.line("  -> event pair (%s, %s) is independent.", eventPairData.evt1Name, eventPairData.evt2Name);
            }
        }

        // Return the result.
        return isIndependent;
    }

    /**
     * Check an event pair for being skippable.
     *
     * @param eventPairData The event pair data. Is modified in-place.
     * @param dbg The callback for debug output.
     * @param termination The cooperative termination query function.
     * @return {@code true} if the event pair is skippable, {@code false} if it is not skippable, or if termination was
     *     requested.
     */
    private boolean checkSkippable(EventPairData eventPairData, DebugNormalOutput dbg, Termination termination) {
        // Initialization.
        boolean isSkippable = false;

        // Get data.
        BDD commonEnabledZeroStates = eventPairData.getCommonEnabledZeroStates(termination);

        if (termination.isRequested()) {
            return false;
        }

        // Perform check for skippable event 2 (events 2, 1 versus event 1).
        if (!isSkippable) {
            // Get data.
            BDD event1Done = eventPairData.getEvent1Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            BDD event21Done = eventPairData.getEvent21Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            // Perform check.
            isSkippable = !event21Done.isZero() && event1Done.equals(event21Done)
                    && allStatesCovered(commonEnabledZeroStates, event1Done, eventPairData.varSetOld, termination);

            if (termination.isRequested()) {
                return false;
            }
        }

        // Perform check for skippable event 1 (events 1, 2 versus event 2).
        if (!isSkippable) {
            // Get data.
            BDD event2Done = eventPairData.getEvent2Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            BDD event12Done = eventPairData.getEvent12Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            // Perform check.
            isSkippable = !event12Done.isZero() && event2Done.equals(event12Done)
                    && allStatesCovered(commonEnabledZeroStates, event2Done, eventPairData.varSetOld, termination);

            if (termination.isRequested()) {
                return false;
            }
        }

        // Debug output.
        if (isSkippable) {
            if (DEBUG_GLOBAL) {
                dbg.line("  -> event pair (%s, %s) is skippable.", eventPairData.evt1Name, eventPairData.evt2Name);
            }
        }

        // Return the result.
        return isSkippable;
    }

    /**
     * Check an event pair for being reversible.
     *
     * @param eventPairData The event pair data. Is modified in-place.
     * @param eventToEdge Per event to consider for making triples out of pairs, its CIF/BDD edge.
     * @param dbg The callback for debug output.
     * @param termination The cooperative termination query function.
     * @return {@code true} if the event pair is reversible, {@code false} if it is not reversible, or if termination
     *     was requested.
     */
    private boolean checkReversible(EventPairData eventPairData, Map<Event, CifBddEdge> eventToEdge,
            DebugNormalOutput dbg, Termination termination)
    {
        // Debug output.
        if (DEBUG_REVERSIBLE) {
            dbg.line("Reversible: edge1 guard: %s", eventPairData.edge1.guard.toString());
            dbg.line("Reversible: edge2 guard: %s", eventPairData.edge2.guard.toString());
            dbg.line("Reversible: edge1 update/guard: %s", eventPairData.edge1.updateGuard.toString());
            dbg.line("Reversible: edge2 update/guard: %s", eventPairData.edge1.updateGuard.toString());
        }

        // Get data.
        BDD commonEnabledZeroStates = eventPairData.getCommonEnabledZeroStates(termination);

        if (termination.isRequested()) {
            return false;
        }

        // Check for all event triples, consisting of the pair (in any order) and a different third event.
        for (Entry<Event, CifBddEdge> entry: eventToEdge.entrySet()) {
            Event event3 = entry.getKey();
            if (event3 == eventPairData.event1 || event3 == eventPairData.event2) {
                continue;
            }

            if (termination.isRequested()) {
                return false;
            }

            String evt3Name = CifTextUtils.getAbsName(event3);
            CifBddEdge edge3 = entry.getValue();

            // Debug output.
            if (DEBUG_REVERSIBLE) {
                dbg.line("Reversible (%s, %s), trying event3 = %s", eventPairData.evt1Name, eventPairData.evt2Name,
                        evt3Name);
                dbg.line("Reversible: edge3 guard: %s", edge3.guard.toString());
                dbg.line("Reversible: edge3 update/guard: %s", edge3.updateGuard.toString());
            }

            // Get data.
            BDD event21Done = eventPairData.getEvent21Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            // Debug output.
            if (DEBUG_REVERSIBLE) {
                dbg.line("Reversible: event 2+1 done: %s", event21Done.toString());
            }

            // Check for reversible (events 2, 1, 3 versus event 1).
            if (!event21Done.isZero()) {
                // Get data.
                BDD event1Done = eventPairData.getEvent1Done(termination);

                if (termination.isRequested()) {
                    return false;
                }

                // Compute in which states event 3 is enabled after events 2 and 1.
                BDD event21Enabled3 = event21Done.and(edge3.guard);

                if (termination.isRequested()) {
                    return false;
                }

                // Compute states reachable from the common enabled states, by taking the second, first and third edges.
                BDD event213Done = (event21Enabled3.isZero()) ? eventPairData.factory.zero()
                        : edge3.apply(event21Enabled3.id(), CifBddEdgeApplyDirection.FORWARD, null);

                if (termination.isRequested()) {
                    return false;
                }

                // Debug output.
                if (DEBUG_REVERSIBLE) {
                    dbg.line("Reversible: event2+1 enabled3: %s", event21Enabled3.toString());
                    dbg.line("Reversible: event2+1+3 done: %s", event213Done.toString());
                    dbg.line("Reversible: event1 done: %s", event1Done.toString());
                }

                // Cleanup.
                event21Enabled3.free();

                // Perform the check.
                boolean isReversible = !event213Done.isZero() && event213Done.equals(event1Done)
                        && allStatesCovered(commonEnabledZeroStates, event1Done, eventPairData.varSetOld, termination);

                if (termination.isRequested()) {
                    return false;
                }

                // Debug output.
                if (isReversible) {
                    if (DEBUG_REVERSIBLE) {
                        dbg.line("reversible: event213Done != false && event213Done == event1Done -> "
                                + "Found a match.");
                    }
                }

                // Cleanup.
                event213Done.free();

                // Return the result, if we're done.
                if (isReversible) {
                    return true;
                }
            }

            // Get data.
            BDD event12Done = eventPairData.getEvent12Done(termination);

            if (termination.isRequested()) {
                return false;
            }

            // Debug output.
            if (DEBUG_REVERSIBLE) {
                dbg.line("Reversible: event 1+2 done: %s", event12Done.toString());
            }

            // Check for reversible (events 1, 2, 3 versus event 2).
            if (!event12Done.isZero()) {
                // Get data.
                BDD event2Done = eventPairData.getEvent2Done(termination);

                if (termination.isRequested()) {
                    return false;
                }

                // Compute in which states event 3 is enabled after events 1 and 2.
                BDD event12Enabled3 = event12Done.and(edge3.guard);

                if (termination.isRequested()) {
                    return false;
                }

                // Compute states reachable from the common enabled states, by taking the first, second and third edges.
                BDD event123Done = (event12Enabled3.isZero()) ? eventPairData.factory.zero()
                        : edge3.apply(event12Enabled3.id(), CifBddEdgeApplyDirection.FORWARD, null);

                if (termination.isRequested()) {
                    return false;
                }

                // Debug output.
                if (DEBUG_REVERSIBLE) {
                    dbg.line("Reversible: event1+2 enabled3: %s", event12Enabled3.toString());
                    dbg.line("Reversible: event1+2+3 done: %s", event123Done.toString());
                    dbg.line("Reversible: event2 done: %s", event2Done.toString());
                }

                // Cleanup.
                event12Enabled3.free();

                // Perform the check.
                boolean isReversible = !event123Done.isZero() && event123Done.equals(event2Done)
                        && allStatesCovered(commonEnabledZeroStates, event2Done, eventPairData.varSetOld, termination);

                if (termination.isRequested()) {
                    return false;
                }

                // Debug output.
                if (isReversible) {
                    if (DEBUG_REVERSIBLE) {
                        dbg.line("reversible: event123Done != false && event123Done == event2Done -> "
                                + "Found a match.");
                    }
                }

                // Cleanup.
                event123Done.free();

                // Return the result, if we're done.
                if (isReversible) {
                    return true;
                }
            }
        }

        // Return the result.
        return false;
    }

    /**
     * Check whether that the given 'result' states cover all given 'zero' states.
     *
     * <p>
     * This check is to make sure that no source states are lost along the way, by taking transitions. That is, if some
     * path of transitions can only be executed from certain states, while another path can be executed from different
     * states, then we may not correctly detect confluence for all source states. As an example of this, consider the
     * following:
     * </p>
     * <pre>
     * controllable a, b;
     *
     * supervisor A:
     *   location p:
     *     initial;
     *     edge a do z := true goto q;
     *     edge b do v := 2 goto q;
     *
     *   location q:
     *     edge a do z := true goto s;
     *     edge b do v := 2 goto s;
     *
     *   location s:
     *     marked;
     * </pre>
     * <p>
     * Then:
     * <ul>
     * <li>The updates are independent in relation to 'z' and 'v'.</li>
     * <li>If we first execute 'a' and then 'b', it must be the case that we started in 'p' and went to 'q' with 'a',
     * and then went to 's' with 'b'. We can compute the target states reached after taking 'a' and then 'b', but given
     * that we could only start in 'p', this doesn't give us the information for all possible source states.</li>
     * </ul>
     * This method detects such cases, such that we can prevent deciding confluence for them.
     * </p>
     *
     * @param commonEnabledZeroStates The 'zero' states, the source states where the combined guards holds, expressed in
     *     terms of 'zero' variables.
     * @param resultStates The 'result' states, the target states for one of the confluence pattern checks, as a
     *     relation between surviving 'zero' states and the target 'old' states.
     * @param varSetOld The BDD variable set containing all old variables.
     * @param termination The cooperative termination query function.
     * @return Whether the {@code resultStates} cover all {@code commonEnabledZeroStates}, and thus no 'zero' states are
     *     lost along the way. Always returns {@code false} if termination was requested.
     */
    private boolean allStatesCovered(BDD commonEnabledZeroStates, BDD resultStates, BDDVarSet varSetOld,
            Termination termination)
    {
        // Compute the set of 'zero' states that are associated with the target 'old' states.
        BDD zeroResultStates = resultStates.exist(varSetOld);

        if (termination.isRequested()) {
            return false;
        }

        // Check whether the target 'zero' states cover all source 'zero' states.
        boolean result = commonEnabledZeroStates.equals(zeroResultStates);
        zeroResultStates.free();
        return result;
    }

    /**
     * Construct an event pair where its members are alphabetically sorted.
     *
     * @param evt1Name First name to use.
     * @param evt2Name Second name to use.
     * @return Pair with the names in alphabetical order.
     */
    private Pair<String, String> makeSortedPair(String evt1Name, String evt2Name) {
        if (SORTER.compare(evt1Name, evt2Name) < 0) {
            return new Pair<>(evt1Name, evt2Name);
        } else {
            return new Pair<>(evt2Name, evt1Name);
        }
    }

    /**
     * Output the collection of event pairs with the stated reason.
     *
     * @param pairs Event pairs to output.
     * @param reasonText Description of the reason what the collection means.
     * @param out Callback to send normal output to the user.
     * @param needEmptyLine Whether an empty line is needed if matches are dumped by this method.
     * @return Whether an empty line is needed if matches are dumped after this method.
     */
    private boolean dumpMatches(List<Pair<String, String>> pairs, String reasonText, DebugNormalOutput out,
            boolean needEmptyLine)
    {
        // Nothing to do if no pairs. Preserves the need for an empty line for the next dump.
        if (pairs.isEmpty()) {
            return needEmptyLine;
        }

        // Sort the pairs for easier reading.
        pairs.sort(
                Comparator.comparing((Pair<String, String> p) -> p.left, SORTER).thenComparing(p -> p.right, SORTER));

        // Dump the pairs.
        if (needEmptyLine) {
            out.line();
        }
        out.line(reasonText + ":");
        out.inc();
        out.line(pairs.stream().map(Pair::toString).collect(Collectors.joining(", ")));
        out.dec();
        return true;
    }
}
