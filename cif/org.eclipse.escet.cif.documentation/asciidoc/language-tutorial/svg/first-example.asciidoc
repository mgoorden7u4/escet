//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::../_part_attributes.asciidoc[]
include::_local_attributes.asciidoc[]

[[lang-tut-svg-chapter-first-example]]
== First example

indexterm:[SVG visualization,first example]
Let's look at a first example of SVG visualization.
Here, we use a trivial example, which nonetheless gives a quick overview of what the idea of SVG visualization is all about, and what it looks like.

Note that it is not necessary to fully understand what exactly is going on, or how it works.
Those details should become clear after reading the remaining pages of the documentation.

=== The SVG image

We use an SVG image with a single circle:

image::{lang-tut-imgsdir}/svg/lamp/lamp_image.png[]

The element of the image that represents the circle is given id `lamp`, to be able to uniquely identify it.
The SVG image is saved in a file named `lamp.svg`:

[source, svg]
----
include::{incdir-lang-tut-svg}/lamp/lamp.svg[]
----

The SVG image has a size of 70 by 70 pixels if not scaled.
It contains a single circle, with id `lamp`, `black` `fill` color, its center at x-coordinate `35` and y-coordinate `35`, and a radius of `25` pixels.

=== The CIF model

We use the following CIF model:

[source, cif]
----
include::{incdir-lang-tut-svg}/lamp/lamp.cif[]
----

This model describes not only the behavior of the lamp using a CIF automaton, but also contains two CIF/SVG declarations.

The SVG file declaration at the start of the model connects the SVG image from the `lamp.svg` SVG file to the model.
For this to work, the `lamp.svg` file should be in the same folder as the CIF model file.

The CIF/SVG output mapping near the end of the model adapts a property of an element of the image, based on the state of the automaton.
It sets the `fill` attribute of the element with id `lamp`, the circle of the image, to either of two colors: `silver` or `yellow`.
If the automaton is in its `Off` location, the circle is silver.
If the automaton is in its `On` location, the circle is yellow.

=== Simulation

If we simulate the CIF model, the automaton starts in its `Off` location, and thus we see a silver circle:

image::{lang-tut-imgsdir}/svg/lamp/lamp_sim_off.png[]

After a bit of time and taking a `tau` transition, the automaton is in its `On` location.
We then get a yellow circle:

image::{lang-tut-imgsdir}/svg/lamp/lamp_sim_on.png[]

If we let the simulator run for a while, we see that the lamp is turned on and off repeatedly.
SVG visualization essentially turns into an SVG movie of a flashing lamp.
